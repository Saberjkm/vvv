#ifndef VVV_MOCK_APPLICATION_H_INCLUDED
#define VVV_MOCK_APPLICATION_H_INCLUDED

#include "gmock/gmock.h"
#include "application/application.h"
#include "test/mock_console.h"

namespace application {

    class MockApplication : public BaseApplication {
        protected:
        console::SystemConsole& mock_console_;
        public:
        MockApplication() = delete;
        MockApplication(console::SystemConsole& console) : mock_console_(console) { }
        console::SystemConsole& main_console() override { return mock_console_; }
        MOCK_METHOD(void, Run, (const application::LogicRegistrarSettings& logic_registrar_settings), (override));
        MOCK_METHOD(void, CleanUp, ());
        MOCK_METHOD(void, TerminateApplication, (), (override));
    };
}

#endif