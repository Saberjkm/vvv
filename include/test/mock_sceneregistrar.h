#ifndef VVV_MOCK_SCENEREGISTRAR_H_INCLUDED
#define VVV_MOCK_SCENEREGISTRAR_H_INCLUDED

#include "gmock/gmock.h"

#include "scenes/sceneregistrar.h"

namespace scenes {

class MockSceneRegistrar : public SceneRegistrar {
    public:
    MockSceneRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar) : SceneRegistrar(global_context, related_logic_registrar) {}

    bool AddSceneInfo(SceneInfo scene_info) {
        if (!scene_info_.contains(scene_info.scene_label)) {
            scene_info_[scene_info.scene_label] = scene_info;
        }
        return false;
    }
};

}
#endif
