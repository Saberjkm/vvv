#ifndef VVV_MOCK_ENTITYREGISTRAR_H_INCLUDED
#define VVV_MOCK_ENTITYREGISTRAR_H_INCLUDED

#include "gmock/gmock.h"

#include "entities/entityregistrar.h"
#include "entities/entitysearchtree.h"
#include "entities/entity.h"


namespace entities {

class MockEntityRegistrar : public EntityRegistrar {
    public:
    MockEntityRegistrar() = delete;
    MockEntityRegistrar(application::BaseApplication& globalC, logic::LogicRegistrar& rLR) : EntityRegistrar(globalC, rLR) {}
    MOCK_METHOD(bool, AdvanceFrame, (), (override));
    MOCK_METHOD(bool, HasEntity, (std::size_t entity_id), (override));
    MOCK_METHOD(std::size_t, CountEntities, (), (override));
    MOCK_METHOD(application::EntityData, CreateEmptyEntity, (), (override));
    MOCK_METHOD(EntitySearchTree*, InsertKey, ((std::size_t entity_key), (const std::map<std::string, std::pair<std::size_t, ComponentAccess>>* component_data)), (override));
    MOCK_METHOD(std::vector<std::shared_ptr<entities::Entity>>, GatherEntityPointers, (const std::set<std::string>& component_labels), (override));
    MOCK_METHOD(std::vector<std::shared_ptr<entities::Entity>>, GatherEntityPointers, ((const std::set<std::string>& component_labels), (std::function<bool(const std::shared_ptr<entities::Entity>)> gather_condition)), (override));
    MOCK_METHOD(std::weak_ptr<Entity>, GetWeakPointer, (std::size_t entity_key), (override));
    MOCK_METHOD(void, AddCreationCallback, ((std::pair<std::string, std::function<void(std::size_t)>> callback)), (override));
    MOCK_METHOD(std::vector<application::ComponentTypeRepresentation>, GetComponentTypesFromRegister, (std::string register_label, std::string scene_label, std::string entity_label), (override));
    MOCK_METHOD(application::EntityData, CreateEntity, (const application::EntityRepresentation& entity), (override));
    //virtual Entity* operator[] (std::size_t entityID) override { return nullptr; }
};

}
#endif