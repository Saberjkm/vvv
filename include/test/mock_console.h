#ifndef VVV_MOCK_CONSOLE_H_INCLUDED
#define VVV_MOCK_CONSOLE_H_INCLUDED

#include <string>

#include "gmock/gmock.h"

#include "application/console.h"

namespace console {
    class MockSystemConsole : public SystemConsole {
        public:
        MOCK_METHOD(void, ThrowError, (std::string source, std::string reason, std::string description), (override));
        MOCK_METHOD(void, IssueWarning, (std::string source, std::string reason, std::string description), (override));
    };
}

#endif