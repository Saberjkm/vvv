#ifndef VVV_MOCK_LOGICREGISTRAR_H_INCLUDED
#define VVV_MOCK_LOGICREGISTRAR_H_INCLUDED

#include "gmock/gmock.h"

#include "logic/logic.h"
#include "logic/logicregistrar.h"
#include "test/mock_componentregistrar.h"
#include "test/mock_entityregistrar.h"
#include "test/mock_systemregistrar.h"

namespace logic {

class MockLogicRegistrar : public LogicRegistrar { 
    entities::EntityRegistrar& related_entity_registrar_; 
    components::ComponentRegistrar& related_component_registrar_;
    systems::SystemRegistrar& related_system_registrar_;
    public:
    MockLogicRegistrar() = delete;
    MockLogicRegistrar(application::BaseApplication& globalC,
                       entities::EntityRegistrar& relatedER,
                       components::ComponentRegistrar& relatedCR,
                       systems::SystemRegistrar& relatedSR ) 
        : LogicRegistrar(globalC), 
          related_entity_registrar_(relatedER),
          related_component_registrar_(relatedCR),
          related_system_registrar_(relatedSR) 
    {}
    entities::EntityRegistrar& entity_registrar() override { return related_entity_registrar_; }
    components::ComponentRegistrar& component_registrar() override { return related_component_registrar_; }
    systems::SystemRegistrar& system_registrar() override { return related_system_registrar_; }
    

};

}

#endif