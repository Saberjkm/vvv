#ifndef VVV_MOCK_COMPONENTREGISTER_H_INCLUDED
#define VVV_MOCK_COMPONENTREGISTER_H_INCLUDED

#include "components/componentregister.h"

namespace components {
class MockComponentRegister : public ComponentRegister {
    protected:
    public:

    MockComponentRegister() = delete;
    MockComponentRegister(ComponentRegistrar& parent_registrar, std::string register_name) : ComponentRegister(parent_registrar, register_name) {
        component_types_["test1"] = std::make_any<int>();
        component_types_["test2"] = std::make_any<int>();
        component_types_["test3"] = std::make_any<std::string>();
    }
};

}
#endif