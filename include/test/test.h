#ifndef VVV_TEST_H_INCLUDED
#define VVV_TEST_H_INCLUDED

namespace test {
    const unsigned long kLowNumber = 10;
    const unsigned long kMedNumber = 1000;
    const unsigned long kHghNumber = 100000;

    const double kLowChance = 1.0f / 10.0f;
    const double kMedChance = 5.0f / 10.0f;
    const double kHghChance = 8.0f / 10.0f;

    // Use when a good random function isn't required
    float WeakRandomChance();

    // Gets a random low int [0, kLowNumber)
    unsigned long GetRandomLowInt();

    // Gets a random med int [0, kMedNumber)
    unsigned long GetRandomMedInt();

    // Gets a random hgh int [0, kHghNumber)
    unsigned long GetRandomHghInt();

}

#endif