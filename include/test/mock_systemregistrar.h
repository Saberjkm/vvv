#ifndef VVV_MOCK_SYSTEMREGISTRAR_H_INCLUDED
#define VVV_MOCK_SYSTEMREGISTRAR_H_INCLUDED

#include "gmock/gmock.h"

#include "systems/systemregistrar.h"

namespace systems {

class MockSystemRegistrar : public SystemRegistrar {
    public:
    MockSystemRegistrar() = delete;
    MockSystemRegistrar(application::BaseApplication& globalC, logic::LogicRegistrar& rLR) : SystemRegistrar(globalC, rLR) {}
    MOCK_METHOD(bool, RegisterSystem, (std::string register_label, std::string system_label), (override));
    MOCK_METHOD(std::size_t, CountSystems, (), (override));
    MOCK_METHOD(application::SystemData, LoadSystem, (const application::SystemRepresentation& system), (override));
    MOCK_METHOD(bool, LockSystem, (std::string system_label, std::string lock_name), (override));
};

}
#endif