#ifndef VVV_SVGLOADER_H_INCLUDED
#define VVV_SVGLOADER_H_INCLUDED

#include <vector>

#include "graphicsdata.h"
#include "tinyxml2/tinyxml2.h"
#include "application/application.h"

namespace graphics::svg {

class SVGLoader {
    private:
    
    tinyxml2::XMLDocument loaded_file_; // The loaded graphic (svg) resource

    bool Reset();
    
    public:
    
    bool Loadfile(std::string file_name);
};

}
#endif