#ifndef VVV_GRAPHICSOBJECTS_H_INCLUDED
#define VVV_GRAPHICSOBJECTS_H_INCLUDED

#include <array>
#include <map>
#include <string>
#include <optional>
#include <vector>

#include <vulkan/vulkan.h>

#include "glm/glm.hpp"

#include "application/application.h"

namespace graphics {

const glm::mat3 kIdentity(1.0f);

enum FillType { kTriangle = 0, kCubic = 1, kQuadratic = 2, kArc = 3};

struct ModelData2D {
    glm::mat3 model_matrix;
};

struct PushConstant {
    glm::mat3 model_matrix; 
};

struct UniformBufferObject {
    alignas(16) glm::mat4 view;
    alignas(16) glm::mat4 proj;
};

struct Vertex {
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec3 texCoord;
    float inside_or_outside = 1;
    unsigned int fill_type = 0;

    static VkVertexInputBindingDescription GetBindingDescription() {
        VkVertexInputBindingDescription bindingDescription{};
        bindingDescription.binding = 0;
        bindingDescription.stride = sizeof(Vertex);
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return bindingDescription;
    }

    static std::array<VkVertexInputAttributeDescription, 5> GetAttributeDescriptions() {
        std::array<VkVertexInputAttributeDescription, 5> attributeDescriptions{};

        attributeDescriptions[0].binding = 0;
        attributeDescriptions[0].location = 0;
        attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[0].offset = offsetof(Vertex, pos);

        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof(Vertex, color);

        attributeDescriptions[2].binding = 0;
        attributeDescriptions[2].location = 2;
        attributeDescriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[2].offset = offsetof(Vertex, texCoord);

        attributeDescriptions[3].binding = 0;
        attributeDescriptions[3].location = 3;
        attributeDescriptions[3].format = VK_FORMAT_R32_SFLOAT;
        attributeDescriptions[3].offset = offsetof(Vertex, inside_or_outside);

        attributeDescriptions[4].binding = 0;
        attributeDescriptions[4].location = 4;
        attributeDescriptions[4].format = VK_FORMAT_R32_UINT;
        attributeDescriptions[4].offset = offsetof(Vertex, fill_type);
        
        return attributeDescriptions;
    }

    bool operator==(const Vertex& other) {
        return (pos == other.pos) && (color == other.color) && (texCoord == other.texCoord) && (inside_or_outside == other.inside_or_outside) && (fill_type == other.fill_type);
    }

    bool operator<(const Vertex& other) {
        // Not the most robust comparison but simple enough to use in a set
        return (pos.x < other.pos.x);
    }
};

struct RenderObject {
    const glm::mat3* model_matrix;
    uint32_t index_offset;
    uint32_t index_count;
};

class PipelineStorage {
    public:

    auto begin() { return pipelines.begin(); }
    auto end() { return pipelines.end();}

    void CleanUp(VkDevice& device, const VkAllocationCallbacks* pAllocator);

    /**
     * @brief Creates a Vkpipeline and Vkpipeline layout entry
     * @param name The name to tag with them
     * @return true It was created
     * @return false It wasn't created (already there)
     */
    bool CreateEntry(std::string name);

    VkPipelineLayout& GetPipelineLayout(const std::string& pipeline_name);
    VkPipeline& getPipeline(const std::string& pipeline_name);
    protected:
    std::map<std::string, std::pair<VkPipeline, VkPipelineLayout>> pipelines;

};

// Wrapped in struct in case other data needs to be stored within renderpass
struct RenderPassStorage {
    std::vector<VkRenderPass> renderpass;
};

struct PipelineCreateInfo {
    VkDevice& device;
    VkRenderPass& render_pass;
    std::string name;
    VkDescriptorSetLayout* descriptor_set_layout;
};

class PipelineCreator {
    public:
    PipelineStorage& storage_location_;
    VkExtent2D& swap_chain_extent_;

    uint32_t subpass_number_ = 0;

    VkPipelineShaderStageCreateInfo vert_shader_stage_info_{};
    VkPipelineShaderStageCreateInfo frag_shader_stage_info_{};

    VkPipelineVertexInputStateCreateInfo vertex_input_info_{};
    VkPipelineInputAssemblyStateCreateInfo input_assembly_{};

    VkViewport viewport_{};
    VkRect2D scissor_{};
    VkPipelineViewportStateCreateInfo view_port_state_{};

    VkPipelineRasterizationStateCreateInfo rasterizer_{};
    VkPipelineMultisampleStateCreateInfo multisampling_{};

    VkPipelineColorBlendAttachmentState color_blend_attachment_{};
    VkPipelineColorBlendStateCreateInfo color_blending_{};
    
    VkStencilOpState front_{};
    VkStencilOpState back_{};
    VkPipelineDepthStencilStateCreateInfo depth_stencil_{};

    VkPipelineLayoutCreateInfo pipeline_layout_info_{};
    VkGraphicsPipelineCreateInfo pipeline_info_{};

    PipelineCreator(PipelineStorage& sLocation,  VkExtent2D& scExtent, VkDescriptorSetLayout& dsLayout) 
    : storage_location_(sLocation), 
      swap_chain_extent_(scExtent) {}

    void InitialiseValues();
    void CreatePipeline(PipelineCreateInfo creation_info);
    protected:


};

struct RenderInfo { const std::vector<RenderObject>& objects_to_render;
                    VkBuffer& vertex_buffer; 
                    VkBuffer& index_buffer;
                    VkCommandBufferInheritanceInfo inheritance_info; 
                    std::size_t pipeline_wanted;
                    std::size_t frame_wanted;
                    const VkDescriptorSet* descriptor_set;
};

struct QueueFamilyIndices {
    std::optional<uint32_t> graphics_family;
    std::optional<uint32_t> present_family;

    bool IsComplete() {
        return graphics_family.has_value() && present_family.has_value();
    }
};

}
#endif