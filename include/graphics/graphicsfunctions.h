#ifndef VVV_GRAPHICSFUNCTIONS_H_INCLUDED
#define VVV_GRAPHICSFUNCTIONS_H_INCLUDED

#include <vector>

#include "graphics/graphicsdata.h"

namespace graphics {

// Rounding constant for float precision
const float kRoundError = 5.0e-4f;

static const float kOneThird = 1.0f / 3.0f;
static const float kTwoThirds = 2.0f / 3.0f;

float RoundToZero(float val);

bool ApproxEqual(float f0, float f1);

// Classifying as according to Loop-Blinns schema
enum class CubicBezierCurveType {kSerpentine, kLoop, kCusp, kQuadratic, kLine, kPoint}; 
enum class FillSide {kLeft, kRight};

struct ClassifiedCurve {
    CubicBezierCurveType type;
    float d1;
    float d2;
    float d3;

    ClassifiedCurve(CubicBezierCurveType type, float d1, float d2, float d3) {
        this->type = type;
        this->d1 = d1;
        this->d2 = d2;
        this->d3 = d3;
    }
};

ClassifiedCurve ClassifyCurve(glm::vec3 control_points[4]);

std::vector<Vertex> GenerateCuvicBezierCurve(glm::vec3 control_points[4], glm::vec3 colour, FillSide fill_side = FillSide::kLeft); 

// Works from model space so 2D
std::vector<Vertex> GenerateQuadBezierStroke(glm::vec2 control_points[3], glm::vec3 colour, float stroke_radius, std::vector<uint32_t>& indices);

/**
 * @brief Get the Intersection Point of two lines
 * @param l1p1 Beginning point of Line 1
 * @param l1p2 End point of Line 1
 * @param l2p1 Beginning point of Line 2
 * @param l2p2 End point of Line 2
 * @param return_value The intersection point will be stored into this, it will have no change in case of parallel
 *        (see return for whether the point lies on the segments between the beginning and ending points)
 * @return true If the lines intersect
 * @return false If the lines do not intersect (parallel)
 */
bool GetIntersectionPoint(glm::vec2 l1p1, glm::vec2 l1p2, glm::vec2 l2p1, glm::vec2 l2p2, glm::vec2& return_value);

/**
 * @brief Check if two line segements inter=sect
 * 
 * @param l1p1 Beginning point of Line 1
 * @param l1p2 End point of Line 1
 * @param l2p1 Beginning point of Line 2
 * @param l2p2 End point of Line 2
 * @return true 
 * @return false 
 */
bool CheckLineSegmentIntersection(glm::vec2 l1p1, glm::vec2 l1p2, glm::vec2 l2p1, glm::vec2 l2p2); 

/**
 * @brief Splits a quadratic bezier at t < [0, 1]
 * 
 * @param bezier_curve The curve yopu want to split
 * @param z The 'time' at which to split
 * @return std::vector<glm::vec2> The two resultant curves (always of size 2)
 */
std::vector<std::array<glm::vec2, 3>> SplitQuadraticBezier(const std::array<glm::vec2, 3> bezier_curve, float t);

/**
 * @brief Checks if two triangles collide on a plane
 * @param triangle1 
 * @param triangle2 
 * @return true 
 * @return false 
 */
bool CheckTriangleCollision(const std::array<glm::vec2, 3> triangle1, const std::array<glm::vec2, 3> triangle2);

/**
 * @brief Get the area of a triangle
 * @param triangle 
 * @return float 
 */
float GetTriangleArea(const std::array<glm::vec2, 3> triangle);

/**
 * @brief Check if a point is within a triangle
 * @param point 
 * @param triangle 
 * @return true 
 * @return false 
 */
bool CheckPointInTriangle(const glm::vec2 point, const std::array<glm::vec2, 3> triangle);

}



#endif