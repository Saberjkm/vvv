#ifndef VVV_RENDERER_H_INCLUDED
#define VVV_RENDERER_H_INCLUDED

#include "graphics/graphicsdata.h"
namespace graphics {

class GraphicsRegistrar;

class Renderer {
    protected:
    GraphicsRegistrar& parent_registrar_;

    std::vector<VkCommandPool> command_pools_;
    std::vector<std::vector<VkCommandBuffer>> command_buffer_lists_;
    std::vector<std::string> pipelines_wanted_;

    std::size_t flight_frame_count_;

    public:

    Renderer() = delete;

    Renderer(GraphicsRegistrar& parent_registrar) : parent_registrar_(parent_registrar) {}

    virtual ~Renderer() = default;

    const std::vector<std::vector<VkCommandBuffer>>& command_buffer_lists() { return command_buffer_lists_; }
    virtual std::pair<const VkCommandBuffer*, uint32_t> command_buffers(std::size_t pipeline_wanted, std::size_t frame_wanted) { return {nullptr, -1}; }

    /**
     * @brief Cleans up all resources allocated by the renderer
     */
    virtual void CleanUp();

    /**
     * @brief Initialises the renderer
     */
    virtual void Initialise(std::vector<std::string> pipes_wanted, std::size_t max_frames_in_flight) { return; }

    /**
     * @brief Waits for the work of the renderer to finish
     */
    virtual void Wait() { return; }

    /**
     * @brief Sets up the draw commands for the primary command buffer based on given objects
     * @param inheritance_info The inheritance info for the primary command buffer
     * @param objects_to_render The objects which to build draw commands for
     * @return true No errors happened in rendering
     * @return false Some errors happened in rendering
     */
    virtual bool Render(RenderInfo render_info) { return true; }
};


}

#endif