#ifndef VVV_INPUT_H_INCLUDED
#define VVV_INPUT_H_INCLUDED

#include <map>

#include "input/key.h"
namespace input {

struct InputContext {
    std::string context_label; // Used to give a specific id to the context
    std::size_t priority_number;
    std::map<Key, std::pair<std::string, std::size_t>> inputs;
    logic::Target owner; // Where to issue the event that results from resolving a key for the context
};


}
#endif