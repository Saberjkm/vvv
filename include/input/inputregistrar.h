#ifndef VVV_INPUTREGISTRAR_H_INCLUDED
#define VVV_INPUTREGISTRAR_H_INCLUDED
#include <GLFW/glfw3.h>
#include <map>

#include "input/input.h"
#include "logic/logic.h"
#include "application/application.h"
#include "application/settings.h"

namespace logic { class LogicRegistrar; }

namespace input {

class InputRegistrar {
    protected:

    application::BaseApplication& global_context_; 
    logic::LogicRegistrar& related_logic_registrar_;

    std::map<std::string, Key> key_dictionary_;
    
    std::set<std::string> current_input_context_ids_;
    std::map<std::size_t, std::vector<InputContext>> input_contexts_;
    std::vector<Key> keys_used_this_frame_;

    std::set<std::string> next_input_context_ids_;
    std::set<std::string> next_deleted_input_context_ids_;
    std::vector<InputContext> next_input_contexts_;
    
    public:
    
    InputRegistrar() = delete;
    InputRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar)
        : global_context_(global_context), 
          related_logic_registrar_(related_logic_registrar) {}

    Key GetKey(const std::string& key_alias) {
        if (key_dictionary_.contains(key_alias)) return key_dictionary_[key_alias];
        return {};
    }

    const std::vector<Key>& GetKeysUsedThisFrame() { return keys_used_this_frame_; }

    const std::size_t InputContextCount() { return current_input_context_ids_.size(); }

    const bool HasContext(std::string context_label) { return current_input_context_ids_.contains(context_label); }

    /**
     * @brief Does the end of frame operations for the registrar
     * @return true Everything completed successfully
     * @return false Something failed
     */
    bool EndOfFrame();

    /**
     * @brief Does the advance frame operations for the registrar
     * @return true Everything completed successfully
     * @return false Something failed
     */
    bool AdvanceFrame();

    /**
     * @brief Adds a record for the key that is valid for this frame only
     * @param key Key you wish to add
     */
    void AddKey(Key key) { keys_used_this_frame_.emplace_back(key); }

    /**
     * @brief Initialises the registrar
     * 
     * @return true 
     * @return false 
     */
    bool Initialise(const application::InputRegistrarSettings& input_registrar_settings);

    /**
     * @brief Does the things needed before the engine starts but after the engine is initialised
     * 
     * @return true 
     * @return false 
     */
    bool FirstFrame();
    
    /**
     * @brief Adds an input context to the registrar (added next frame)
     * 
     * @param input_context The input context you want to add
     * @return true A new context was added successfully
     * @return false A new context was not added
     */
    bool AddInputContext(InputContext input_context);

    /**
     * @brief Undoes the adding of an input context to the registrar
     * 
     * @param context_label ID of the input context you want to undo
     * @return true An input context was undone
     * @return false No input context was undone
     */
    bool UndoAddInputContext(std::string context_label);

    /**
     * @brief Deletes an input context from the registrar
     * 
     * @param context_label ID of the input context you want to delete
     * @return true An input context was slated for deletion
     * @return false No input context was deleted
     */
    bool DeleteInputContext(std::string context_label);

    /**
     * @brief Undoes the deletion of an input context from the registrar
     * 
     * @param context_label ID of the input context you want to undo the deletion of
     * @return true Deletion for Input Context was undone
     * @return false No deletion of an input context was undone
     */
    bool UndoDeleteInputContext(std::string context_label);

    /**
     * @brief Maps the inputs received this frame into the proper inputs based on the recorded contexts
     */
    void MapInputs();
};

static InputRegistrar* main_input_registrar; 

static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

}
#endif