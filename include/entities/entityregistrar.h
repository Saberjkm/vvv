#ifndef VVV_ENTITYREGISTRAR_H_INCLUDED
#define VVV_ENTITYREGISTRAR_H_INCLUDED

#include <vector>
#include <stack>
#include <map>
#include <set>
#include <algorithm>
#include <type_traits>
#include <functional>

#include "entities/entitysearchtree.h"
#include "entities/entity.h"
#include "application/settings.h"

namespace application { class BaseApplication;
                        struct EntityRepresentation; }
namespace logic { class LogicRegistrar; }
namespace components { class ComponentRegistrar; }
namespace entities {
class EntityRegister;
class VVVEntityRegister;

class EntityRegistrar {
    protected:
    application::BaseApplication& global_context_; 
    logic::LogicRegistrar& related_logic_registrar_;

    EntitySearchTree search_root_;

    struct FrameData {
        std::set<std::size_t> created_this_frame;
        std::set<std::size_t> deleted_this_frame;
        
        std::vector<std::size_t> deleted_values;
    };

    FrameData current_frame_;
    FrameData next_frame_;

    std::shared_ptr<VVVEntityRegister> core_entity_register_;

    std::map<std::string, std::shared_ptr<EntityRegister>> entity_registers_;
    
    std::vector<bool> used_space_;
    std::vector<std::shared_ptr<Entity>> entity_list_;

    std::map<std::string, std::function<void(std::size_t)>> creation_callbacks_;   /**< Function callbacks when an entity is created */
    std::map<std::size_t, std::map<std::string, std::function<void(std::size_t)>>> specific_deletion_callbacks_;   /**< Function callbacks when a specific entity is deleted */
    std::map<std::string, std::function<void(std::size_t)>> general_deletion_callbacks_;

        
    /**
     * @brief Create a Entity from the given Entity Representation
     * 
     * @param component_list The paired list of component labels and their specific data
     * @return std::size_t 
     */
    virtual application::EntityData CreateNewEntity(const entities::EntityStorageData& entity, const application::EntityRepresentation& entity_rep);

    /**
     * @brief Create an Entity object
     * @return std::size_t The ID of the newly created entity
     */
    virtual application::EntityData CreateNewEntity(const application::EntityRepresentation& entity_rep);


    public:
    virtual ~EntityRegistrar() {}
    EntityRegistrar() = delete;
    EntityRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar);

    application::BaseApplication& global_context() { return global_context_; }
    components::ComponentRegistrar& related_component_registrar();

    /**
     * @brief Initialises the registrar
     * 
     * @return true 
     * @return false 
     */
    virtual bool Initialise(const application::EntityRegistrarSettings& entity_registrar_settings);

    /**
     * @brief does the things needed before the engine starts but after the engine is initialised
     * 
     * @return true 
     * @return false 
     */
    virtual bool FirstFrame();

    /**
     * @brief Resolves the changed entities
     * 
     * Copies the changed entities from the next frame into the curent frame
     */
    virtual bool AdvanceFrame();

    /**
     * @brief Puts the registrar in the end of frame state
     * @return true 
     * @return false 
     */
    virtual bool EndOfFrame();

    //-- These 'read' operations work on the current frame
    
    /**
     * @brief Checks for existence of entity based on ID
     * 
     * @param entity_id The entity ID you are looking for
     * @return true Has entity with the id
     * @return false Doesn't have entity with the id
     */
    virtual bool HasEntity(std::size_t entity_id);

    /**
     * @brief Checks for the existence of a register based on given label
     * 
     * @param register_label Label of the register you want to check
     * @return true The register exists
     * @return false The register doesn't exist
     */
    virtual bool HasRegister(std::string register_label);

    /**
     * @brief Returns a count of the entities within the registrar
     * 
     * @return std::size_t Number of entities within the registrar
     */
    virtual std::size_t CountEntities() { 
        return entity_list_.size() - current_frame_.deleted_values.size();
    }

    /**
     * @brief Gets a full entity without concern of access rights
     * 
     * @param entity_id ID of entity wanted
     * @return std::shared_ptr<Entity> A pointer to the Entity
     */
    virtual std::shared_ptr<Entity> operator[](std::size_t entity_id);

    /**
     * @brief Returns a list of entities which contain the given component labels
     * @param component_labels 
     * @return std::vector<int> List of entities which satisfy the labels
     */
    virtual std::vector<std::size_t> GatherEntities(const std::set<std::string>& component_labels);

    /**
     * @brief Returns a list of full entities (pointers instead of ids) which contain the given component labels
     * 
     * @param component_labels
     * @return std::vector<Entity*> List of entities which satisfy the labels 
     */
    virtual std::vector<std::shared_ptr<Entity>> GatherEntityPointers(const std::set<std::string>& component_labels);

    /**
     * @brief Returns a list of full entities which contain the given component labels and adheres to the gather condition
     * 
     * @param component_labels 
     * @param gather_condition The function which returns a bool of whether the entity adheres to the condition
     * @return std::vector<Entity*> 
     */
    virtual std::vector<std::shared_ptr<Entity>> GatherEntityPointers(const std::set<std::string>& component_labels, std::function<bool(const std::shared_ptr<entities::Entity>)> gather_condition);

    /**
     * @brief Gathers all the entities data representation based on a given component list
     * 
     * @param component_labels List of components you want the entities to have
     * @return std::map<std::string, std::map<std::size_t, std::pair<std::size_t, ComponentAccess>>> Map of Entity ids to their data representation
     */
    virtual std::map<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> GatherEntityData(const std::set<std::string>& component_labels);

    /**
     * @brief Get a weak pointer pointing to the entity of the given entity id
     * 
     * @param entity_id The ID of the entity you want to point to
     * @return std::weak_ptr<Entity> The pointer to said entity
     */
    virtual std::weak_ptr<Entity> GetWeakPointer(std::size_t entity_id);

    //-- End of current frame 'read' operations

    //-- These 'read' operations work on the next frame

    /**
     * @brief Returns whether or not the next frame has an entity with the given ID
     * @param entity_id 
     * @return true If the next frame has entity with given ID 
     * @return false If the next frame doesn't have entity with a given ID
     */
    virtual bool WillHaveEntity(std::size_t entity_id);

    /**
     * @brief Will return the entity in the next frame
     * @param entity_id The id of the entity wanted
     * @return const Entity* A representation of the entity, nullptr if it doesn't exist
     */
    virtual std::shared_ptr<Entity> QueryEntity(std::size_t entity_id);
    
    //-- End of next frame 'read' operations

    //-- These 'write' operations work on the current frame
    
    /**
     * @brief Adds a callback function to the registrar that is called whenever an entity is created.
     * @param callback The callback to add
     */
    virtual void AddCreationCallback(std::pair<std::string, std::function<void(std::size_t)>> callback);

    /**
     * @brief Deletes a callback that is within the registrar of a given ID
     * @return true The callback will not exist anymore
     * @return false The callback still exists
     */
    virtual bool DeleteCreationCallback(std::string callback_id);

    //-- End of current frame 'write' operations 

    //--These 'write' operations work on next frame
            
    virtual EntitySearchTree* InsertKey(std::size_t entity_key, const std::map<std::string, std::pair<std::size_t, ComponentAccess>>* component_data);

    /**
     * @brief Create a Empty Entity object
     * 
     * @return application::EntityData The newly created entity
     */
    virtual application::EntityData CreateEmptyEntity();

    /**
     * @brief Create an Entity from data stored in a register
     * 
     * @param register_label The label of the register to create the entity from
     * @param scene_label The label of the scene the entity belongs to
     * @param entity_label The ID of the entity
     * @return std::size_t The newly created entitie's ID
     */
    virtual application::EntityData CreateEntity(const application::EntityRepresentation& entity);

    /**
     * @brief Create a new Entity which is a full child of another Entity
     * 
     * Will create an Entity such that each of it's new components are simply references (read) from another Entity. 
     * Referencing an Entity with Components of {{1, 0, (RW|R)}, {2, 0, (RW|R)}} will result in a created Entity of {{1, 0, R}, {2, 0, R}}
     * 
     * @param target_entity_id Entity of which you are copying in its entirety
     * @return application::EntityData The data of the newly created entity
     */
    virtual application::EntityData CreateReferenceEntity(std::size_t target_entity_id);

    /**
     * @brief Deletes an entity from the registrar (the entity has to exist in the current frame)
     * @param entity_id The ID of the entity you want to delete
     * @return true If an entity was deleted
     * @return false If no entity was found
     */
    virtual bool DeleteEntity(std::size_t entity_id);

    /**
     * @brief Undoes the creation of an entity in the next frame with a given entity_id
     * @param entity_id The entity you wish to undo
     * @return true An entity's creation was undone
     * @return false No change has been enacted
     */
    virtual bool UndoCreation(const application::EntityData& entity);

    /**
     * @brief Undoes the deletion of an entity in the next frame with a given entity_id
     * @param entity_id The entity you wish to undo
     * @return true An entity's deletion was undone
     * @return false No change has been enacted
     */
    virtual bool UndoDeletion(std::size_t entity_id);

    /**
     * @brief Undoes the changes of an entity in the next frame with a given entity_id
     * @param entity_id The entity you wish to undo
     * @return true An entity's changes were undone
     * @return false No change has been enacted
     */
    virtual bool UndoChanges(std::size_t entity_id);

    /**
     * @brief Gets Component Types of an Entity registered within a given register
     * 
     * @param register_label The ID of the register you wish to read from
     * @param scene_label The ID of the scene the entity belongs to
     * @param entity_label The ID of the entity you want to get the component types from
     * @return std::vector<application::ComponentTypeRepresentation> The list of component types present
     */
    virtual std::vector<application::ComponentTypeRepresentation> GetComponentTypesFromRegister(std::string register_label, std::string scene_label, std::string entity_label);

    virtual void PrintTree() { search_root_.PrintTree("{"); }

    //-- End of next frame 'write' operations
};

}
#endif