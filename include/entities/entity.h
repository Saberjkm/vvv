#ifndef VVV_ENTITY_H_INCLUDED
#define VVV_ENTITY_H_INCLUDED

#include <map>
#include <vector>
#include <string>
#include <optional>
#include <set>

#include "entities/entities.h"
#include "application/console.h"
namespace entities { class EntityRegistrar; 
                     class EntitySearchTree;
                   }
namespace application { class BaseApplication; }
namespace entities {
    
class Entity {
    protected:
    application::BaseApplication& global_context_;
    EntityRegistrar& parent_registrar_; /**< The registrar of which this entity belongs **/
    std::size_t entity_id_ = -1;

    struct FrameData {
        bool has_loaded = false;
        EntitySearchTree* tree_location = nullptr;
        std::set<std::string> locks;
        std::map<std::string, std::pair<std::size_t, ComponentAccess>> component_data; /**< Components and their access rights **/  
        /**
         * @brief A list of the entities which are referencing given Component Label ID (indexed by it)
         */
        std::map<std::string, std::map<std::size_t, std::weak_ptr<Entity>>> child_entities;
        std::map<std::string, std::weak_ptr<Entity>> parent_entity;
    };

    FrameData current_frame_;
    FrameData next_frame_; 
    bool has_changed_ = false;

    /**
     * @brief Handles the flags given for this entity
     * 
     * @param flag_data Flags you want to handle
     * @return true No errors occurred
     * @return false An error occurred
     */
    bool HandleFlags(const application::FlagData& flag_data);

    public:

    Entity() = delete;

    Entity(std::size_t entity_id, application::BaseApplication& global_context, EntityRegistrar& related_entity_registrar)
        : global_context_(global_context), 
          parent_registrar_(related_entity_registrar) 
    {
        entity_id_ = entity_id;
    }

    //-- Current Frame 'Read' Operations

    bool IsInTree() const { return current_frame_.tree_location; }

    /**
     * @brief Check if entity is loaded
     * @return true Entity is loaded
     * @return false Entity isn't loaded
     */
    bool IsLoaded() const { return current_frame_.has_loaded;}

    /**
     * @brief Checks if a component exists in the current frame
     * @param component_label Label Id of component yopu are checking
     * @return true If it exists in the current frame
     * @return false If it doesn't exist in the current frame
     */
    bool HasComponent(std::string component_label) const { return current_frame_.component_data.contains(component_label); }

    /**
     * @brief Returns a count of all the components in the Entity
     * @return std::size_t The count of components
     */
    std::size_t CountComponents() const { return current_frame_.component_data.size(); }

    std::size_t entity_id() const { return entity_id_; }

    /**
     * @brief Get the Component ID of a given Component Label ID
     * @param component_label The label ID of the component of which ID you want 
     * @return std::size_t The component ID or -1 if there wasnt a component with given label
     */
    std::size_t component_id(std::string component_label) const { return (HasComponent(component_label) ? current_frame_.component_data.find(component_label)->second.first : -1); }

    /**
     * @brief Get the Entities which are referencing the given component label (Child Entities)
     * @param component_label The component label to check
     * @return std::vector<std::shared_ptr<Entity>> A copy of the list entities referencing this entity
     */
    std::map<std::size_t, std::weak_ptr<Entity>> child_references(std::string component_label) const;

    /**
     * @brief Checks whether there are children for a given component label
     * @param component_label_id The component label you want to check against
     * @return true Children exist
     * @return false Children don't exist
     */
    bool HasChildren(std::string component_label) const { return (current_frame_.child_entities.contains(component_label) && !current_frame_.child_entities.at(component_label).empty()); }

    /**
     * @brief Checks whether there are children for a given component label in the next frame
     * @param component_label The componenet label you want to check against
     * @return true Children will exist
     * @return false Children wont exist
     */
    bool WillHaveChildren(std::string component_label) const { return (next_frame_.child_entities.contains(component_label) && !next_frame_.child_entities.at(component_label).empty()); }

    /**
     * @brief Returns the entity which the given component label references
     * @param component_label 
     * @return std::shared_ptr<Entity>
     */
    std::weak_ptr<Entity> parent_reference(std::string component_label) const;

    /**
     * @brief Get the Component Access value of the given Component Label
     * @param component_label The label you want to check
     * @return componentAccess The access rights to the given component
     */
    ComponentAccess component_access(std::string component_label) const { return (HasComponent(component_label) ? current_frame_.component_data.find(component_label)->second.second : ComponentAccess::kNA); }

    /**
     * @brief Returns whether the entity has a given lock
     * @param lock_name The name of the lock to check
     * @return true The lock exists in the current frame
     * @return false The lock doesn't exist in the current frame
     */
    bool has_lock(std::string lock_name) { return current_frame_.locks.contains(lock_name); }

    /**
     * @brief Gets a representation of the Entity based on the component data it holds
     * 
     * @return std::pair<std::string, std::map<std::size_t, std::pair<std::size_t, ComponentAccess>>> The Entity ID paired with a map of component label ids and their related ids
     */
    std::pair<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> GetDataRepresentation();

    /**
     * @brief Get the Data Representation object
     * 
     * @param component_labels A list of the component labels you want to represent, no component labels means it will collect ALL components
     * @return std::pair<std::string, std::map<std::size_t, std::pair<std::size_t, ComponentAccess>>> The Entity ID paired with a map of component label ids and their related ids
     */
    std::pair<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> GetDataRepresentation(std::set<std::string> component_labels);

    //-- End of Current Frame 'Read' Operations

    //-- Next Frame 'Read' Operations
    
    /**
     * @brief Check if entity has been 'cleared' in the next frame (deleted)
     * @return true If it has been cleared
     * @return false If it hasn't been cleared
     */
    bool WillBeLoaded() const { return next_frame_.has_loaded; }

    /**
     * @brief Check if component exists in the next frame
     * @param component_label Label ID of wanted component
     * @return true If it will exist in next frame
     * @return false If it doesn't exist in the next frame
     */
    bool WillHaveComponent(std::string component_label) const { return next_frame_.component_data.find(component_label) != next_frame_.component_data.end(); }
    
    //-- End of Next Frame 'Read' Operations

    //-- Current Frame 'Write' Operations

    bool Load(const application::FlagData& flag_data);

    /**
     * @brief Does all the logic required to be done at End of Frame
     * @return true Was successful
     * @return false There was an error
     */
    bool EndOfFrame();

    /**
     * @brief Advances entity to the next frame
     * @return true If it did any operations
     * @return false If it didn't
     */
    bool AdvanceFrame();

    //-- End of Current Frame 'Write' Operations

    //-- Next Frame 'Write' Operations

    /**
     * @brief Deletes components (deletes at related registrars) and cleans up reference entities as well (all on next frame)
     * @return true If everything cleared up correctly
     * @return false The entity was not cleared correctly
     */
    bool Clear();

    /**
     * @brief Adds a lock to the Entity
     * @param lock_name The name of said lock (used to identify)
     * @return true The lock was added
     * @return false A new lock could not be added
     */
    bool AddLock(std::string lock_name);

    /**
     * @brief Undoes the adding of a lock to the Entity
     * @param lock_name Name of lock to undo
     * @return true The new lock was undone successfully 
     * @return false The new lock could not be undone
     */
    bool UndoAddLock(std::string lock_name);

    /**
     * @brief Unlocks a lock on the Entity
     * @param lock_name The lock to unlock
     * @return true The unlock was successfully undone
     * @return false No unlock was undone
     */
    bool Unlock(std::string lock_name);

    /**
     * @brief Undoes the unlocking of a lock on the Entity
     * @param lock_name The name of the lock that was unlocked
     * @return true 
     * @return false 
     */
    bool UndoUnlock(std::string lock_name);
    /**
     * @brief Creates a copy (r only) of each component from a given Entity will delete existing components in favour of new copied ones
     *        Will only copy components that exist in the current frame and the next frame.
     * @param other_entity The entity to copy from
     */
    void Reference(std::weak_ptr<Entity> other_entity);

    /**
     * @brief Adds a child reference to the entity
     * @param component_label The component label to add the reference to
     * @param child_reference The child entity
     * @return true There will exist a reference to the child entity in the next frame 
     * @return false No reference will exist
     */
    bool AddChildReference(std::string component_label, std::weak_ptr<Entity> child_reference);

    /**
     * @brief Deletes the reference to a child for a given component label id
     * @param component_label The ID which to delete at
     * @param reference The reference you want to delete
     * @return true The reference will no longer exist
     * @return false The reference will still exist
     */
    bool DeleteChildReference(std::string component_label, std::weak_ptr<Entity> reference);

    /**
     * @brief Undoes the deletion of a child for a given component label id
     * @param component_label The id which to undo
     * @param reference The reference to re-add
     * @return true The reference deletion was undone successfully
     * @return false The reference deletion was not undone
     */
    bool UndoChildReferenceDeletion(std::string component_label, std::weak_ptr<Entity> reference);

    /**
     * @brief Undoes the creation of a reference to a child in the next frame
     * @param component_label The ID which to undo
     * @param reference The reference you wish to undo
     * @return true Reference undone successfully
     * @return false No change happened
     */
    bool UndoChildReference(std::string component_label, std::weak_ptr<Entity> reference);

    /**
     * @brief Adds a single component to the entity (no checks to validity of component just if it already exists in entity)
     * @param component_label
     * @param component_id 
     * @return true If the entity had the component added to it
     * @return false If there was no change to the component
     */
    bool AddComponent(std::size_t component_label, std::size_t component_id, bool update_tree = true);

    /**
     * @brief Adds a component to the entity (no checks to validity of component just if it already exists in entity)
     * 
     * @param component_label 
     * @param component_id 
     * @return true If the entity had the component added to it
     * @return false If there was no change to the component
     */
    bool AddComponent(std::string component_label, std::size_t component_id, bool update_tree = true);

    /**
     * @brief Adds an initialiser list of std::pair<std::size_t, std::size_t> components to the Entity. 
     *  Will not add anything if a component in given list already exists in the entity
     * @param components The list of components
     * @return true If the entity had the component added to it
     * @return false If there was no change to the component
     */
    bool AddComponent(std::initializer_list<std::pair<std::string, std::size_t>> components);

    template<typename T>
    bool AddComponent(const ComponentPair auto& components) {
        if (!WillBeLoaded()) { return false; }
        std::map<std::string, std::pair<std::size_t, ComponentAccess>> new_components;
        for (auto component : components) {
            if (HasComponent(component.first) || WillHaveComponent(component.first) || new_components.contains(component.first)){
                return false;
            } else {
                new_components[component.first] = std::make_pair(component.second, ComponentAccess::kRW);
            }
        }
        next_frame_.component_data.insert(new_components.begin(), new_components.end());
        has_changed_ = true;
        return true;
    }

    /**
     * @brief Undoes the addition of a component
     * @param component_label The label id of the component
     * @return true Successfully undid component addition
     * @return false No change occured
     */
    bool UndoComponentAddition(std::string component_label);

    /**
     * @brief 
     * 
     * @param reference_entity The entity which holds the component you are referencing
     * @param component_label The component you are referencing
     * @return true A reference component was added
     * @return false No change to entity
     */
    bool AddRefComponent(std::weak_ptr<Entity> reference_entity, std::string component_label);

    /**
     * @brief Deletes a given component label from the data (will delete at component registrar)
     * @param component_label
     * @return true Component was deleted from this entity
     * @return false Component still exists
     */
    bool DeleteComponent(std::string component_label, bool update_tree = true);

    /**
     * @brief Undoes the deletion of a component
     * @param component_label The label of the component to undo
     * @param update_tree (=true) Whether the search tree gets updated afterwards
     * @return true Component was undone successfully
     * @return false No changes were made
     */
    bool UndoComponentDeletion(std::string component_label_id, bool update_tree = true);

    /**
     * @brief Modifies a component within the entity
     * @param component_label_id The label ID of the component you want to modify
     * @param component_data The data you want the label to be set with
     * @param clean_up_component (=true) Clear up existing references and such
     * @return true Modified successfully
     * @return false Unmodified
     */
    bool ModifyComponent(std::string component_label, std::pair<std::size_t, entities::ComponentAccess> component_data, bool clean_up_component = true);

    /**
     * @brief Undoes the creation of this specific entity
     * @return true Undone successfully
     * @return false No changes were made
     */
    bool UndoCreation(const application::EntityData& entity);

    /**
     * @brief Undoes the deletion of an Entity
     * @param ignore_bad_references (=false)  If true will still undo an entities deletion if there are references contained that will be invalid in the next frame.
     *                                       The bad references will not exist in the next frame but warnings will be sent about them.
     * @param show_warnings (=true) If true will show warnings if there are bad references.
     * @return true Undone successsfully
     * @return false No changes were made
     */
    bool UndoDeletion(bool ignore_bad_references = false, bool show_warnings = true);



    //-- End of Next Frame 'Write' Operations

};
}
#endif