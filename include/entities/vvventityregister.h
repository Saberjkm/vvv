#ifndef VVV_VVVENTITYREGISTRAR_H_INCLUDED
#define VVV_VVVENTITYREGISTRAR_H_INCLUDED

#include "entities/entityregister.h"
namespace entities {

class VVVEntityRegister : public EntityRegister {
    protected:
    public:

    VVVEntityRegister() = delete;
    VVVEntityRegister(EntityRegistrar& parent_registrar, std::string register_label);
};

}


#endif