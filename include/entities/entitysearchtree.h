#ifndef VVV_ENTITYSEARCHTREE_H_INCLUDED
#define VVV_ENTITYSEARCHTREE_H_INCLUDED

#include <set>
#include <map>
#include <vector>

#include "entities/entity.h"

namespace entities {
    class Entity;
        // Not sure if a search tree is better than just iteration checks
    // But have to have it implemented to check
    class EntitySearchTree {
        private:

        int depth_ = 0;
        std::set<std::size_t> entity_values_;
        std::set<std::size_t> next_entity_values_;
        std::map<std::string, EntitySearchTree*> child_nodes_;

        bool has_changed_ = false;

        public:

        ~EntitySearchTree() {
            for (auto item : child_nodes_) {
                if (item.second) { delete(item.second); } 
                item.second = nullptr;
            }
            child_nodes_.clear();
        }

        EntitySearchTree() {
        }

        EntitySearchTree(int depth) {
            this->depth_ = depth;
        }

        EntitySearchTree(const EntitySearchTree* other_tree) {
            this->depth_ = other_tree->depth_;
            this->entity_values_ = other_tree->entity_values_;
            this->child_nodes_ = other_tree->child_nodes_; 
        }

        void AdvanceFrame();

        EntitySearchTree* InsertKey(std::size_t entity_key, const std::map<std::string, std::pair<std::size_t, ComponentAccess>>* entity_components);

        /**
         * @brief Deletes the key in the tree
         * @param entity_key 
         * @return true Key no longer exists in tree
         * @return false Key still exists
         */
        bool DeleteKey(std::size_t entity_key);

        /**
         * @brief Undoes the addition of a key in the next frame
         * @param entity_key 
         * @return true Key will not exist next frame
         * @return false Key will exist next frame
         */
        bool UndoKeyAddition(std::size_t entity_key);

        /**
         * @brief Returns a list of all of the entities stored within the tree (or node)
         * @return std::vector<std::size_t> List of all Entities within the tree (or node)
         */
        std::vector<std::size_t> GatherAllEntities();
        
        /**
         * @brief Returns a list of entities (superset) which contain the given component labels within the tree and all of its children.
         * @param component_labels 
         * @return std::vector<int> List of entities which satisfy the labels
         */
        std::vector<std::size_t> GatherEntities(const std::set<std::string>& component_labels);

        void PrintTree(std::string prefix);
    };
}

#endif