#ifndef VVV_LOGIC_H_INCLUDED
#define VVV_LOGIC_H_INCLUDED

#include <string>
#include <vector>
#include <set>
#include <functional>
#include <variant>

#include "application/application.h"
#include "entities/entity.h"
#include "entities/entities.h"
#include "input/key.h"

namespace input { 
    class InputRegistrar;
}

namespace logic{
class LogicRegistrar;
struct Event;
//-- Target types

    // Defined as structs in case of modification for the future, and to make them distinct.
    struct EntityTarget {};
    struct ComponentTarget {};
    struct SystemTarget { std::string system_label; };

    struct EntityRegistrarTarget {};
    struct ComponentRegistrarTarget {};
    struct SystemRegistrarTarget {};
    struct GraphicsRegistrarTarget {};
    struct LogicRegistrarTarget {};
    struct InputRegistrarTarget { 
        std::reference_wrapper<input::InputRegistrar> input_registrar;
    };

    // Allowing for a global reference of targets and to encapsulate fo any changes to the list of targets
    using Target = std::variant<
        EntityTarget,
        ComponentTarget,
        SystemTarget,
        EntityRegistrarTarget,
        ComponentRegistrarTarget,
        SystemRegistrarTarget,
        GraphicsRegistrarTarget,
        LogicRegistrarTarget,
        InputRegistrarTarget
    >; 
//--

//-- Inherent Event Data

    // This counts as data that the event itself controls and/or is given at creation
    struct NoneInherentData {};

    struct TimerInherentData {
        std::size_t time_period = 0;
        bool repeat = false;
    };

    struct CallbackInherentData {
        std::function<void(Event& event)> call_back_function;
    };

    struct KeyInherentData {
        input::Key key;
    };

    // Allowing a global reference for Inherent Data and to encapsulate for any changes to the list in the future
    using InherentData = std::variant<
        NoneInherentData,
        TimerInherentData,
        CallbackInherentData,
        KeyInherentData
    >;
//--

//-- Gathered Event Data

    // This counts as data collected from other placces in the system upon event evaluation or other situations

    struct NoneGatherData {};
    //-- Gather Actions
        struct EntityRelatedGatherAction {
            std::set<std::string> components = std::set<std::string>();
        };

        struct EntityAllGatherAction {
            std::set<std::string> components = {};
        };

        struct EntityConditionalGatherAction {
            std::set<std::string> components;
            std::function<bool(const std::shared_ptr<entities::Entity>)> condition = [](const std::shared_ptr<entities::Entity> entity) { return true; };
        };

        struct EntitySelectedGatherAction {
            std::set<std::size_t> entity_id_list;
        };
    //--
    struct EntityGatherData {
        std::variant<
            EntityRelatedGatherAction,
            EntityAllGatherAction,
            EntityConditionalGatherAction,
            EntitySelectedGatherAction
        > gather_action;

        std::map<std::size_t, std::map<std::string, std::pair<std::size_t, entities::ComponentAccess>>> entity_data; 
    };
    
    // Alowing for global reference and to encapsulate for any changes to the list
    using GatheredData =  std::variant<
        NoneGatherData,
        EntityGatherData
    >;
//--

//-- Variant Handlers (Visitors)

    /**
     * @brief Handles the inherent data of an Event. Returns a pair of <bool, unsigned long long>
     *        The bool indicates whether the event needs to be re-added, whilst the int is the delta logic step to indicate when
     * 
     */
    struct InherentDataHandler {
        Event& parent_event; 
        LogicRegistrar& logic_registrar;

        InherentDataHandler(Event& p_event, LogicRegistrar& l_registrar) : parent_event(p_event), logic_registrar(l_registrar) {}

        std::pair<bool, unsigned long long> operator()(NoneInherentData& non_data) { return {false, 0}; }

        std::pair<bool, unsigned long long> operator()(TimerInherentData& timer_data) {
            return (timer_data.repeat) ? std::pair<bool,unsigned long long>(true, timer_data.time_period) : std::pair<bool,unsigned long long>(false, 0);
        }

        std::pair<bool, unsigned long long> operator()(CallbackInherentData& callback_data) {
            callback_data.call_back_function(parent_event);
            return {false, 0};
        }

        std::pair<bool, unsigned long long> operator()(KeyInherentData& key_data) { return {false, 0}; }
    };

    struct EntityGatherDataHandler {
        EntityGatherData& parent_gather_data;
        LogicRegistrar& logic_registrar;

        EntityGatherDataHandler(EntityGatherData& eg_data, LogicRegistrar& l_registrar) : parent_gather_data(eg_data), logic_registrar(l_registrar) {}

        bool operator()(EntityRelatedGatherAction& related_gather_action);
        bool operator()(EntityAllGatherAction& related_gather_action);
        bool operator()(EntityConditionalGatherAction& related_gather_action);
        bool operator()(EntitySelectedGatherAction& related_gather_action);
    };

    struct GatheredDataHandler {
        Event& parent_event; 
        LogicRegistrar& logic_registrar;

        GatheredDataHandler(Event& p_event, LogicRegistrar& l_registrar) : parent_event(p_event), logic_registrar(l_registrar) {}

        bool operator()(NoneGatherData none_data) { return true; }
        bool operator()(EntityGatherData entity_data) {
            return std::visit(EntityGatherDataHandler(entity_data, logic_registrar), entity_data.gather_action);
        }
    };

    struct TargetHandler {
        Event& parent_event; 
        LogicRegistrar& logic_registrar;

        TargetHandler(Event& p_event, LogicRegistrar& l_registrar) : parent_event(p_event), logic_registrar(l_registrar) {}

        bool operator()(EntityTarget& target);
        bool operator()(ComponentTarget& target);
        bool operator()(SystemTarget& target);
        bool operator()(EntityRegistrarTarget& target);
        bool operator()(ComponentRegistrarTarget& target);
        bool operator()(SystemRegistrarTarget& target);
        bool operator()(GraphicsRegistrarTarget& target);
        bool operator()(LogicRegistrarTarget& target);
        bool operator()(InputRegistrarTarget& target);

    };
//-- 

struct Event {
    std::string event_label;
    Target owner, target; // The target is what the event is passed to, the owner is what generated the event
    InherentData inherent_data; // The inherent data and will be evaluated whether the event should be readded to queue
    GatheredData gathered_data;
    bool can_repeat = true; // Coupled with inherent data evaluation this will determine whether the event should be readded to queue
};

}

#endif