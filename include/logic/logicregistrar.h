#ifndef VVV_LOGICREGISTRAR_H_INCLUDED
#define VVV_LOGICREGISTRAR_H_INCLUDED

#include <map>
#include <set>
#include <vector>
#include <functional>

#include "systems/systemregistrar.h"
#include "components/componentregistrar.h"
#include "entities/entityregistrar.h"
#include "scenes/sceneregistrar.h"
#include "input/inputregistrar.h"
#include "graphics/graphicsdata.h"

namespace logic {

class LogicRegistrar {
    protected:
    application::BaseApplication& global_context_;

    input::InputRegistrar related_input_registrar_;
    entities::EntityRegistrar related_entity_registrar_; 
    components::ComponentRegistrar related_component_registrar_;
    scenes::SceneRegistrar related_scene_registrar_;
    systems::SystemRegistrar related_system_registrar_;

    unsigned long long logic_step_ = 0;

    std::set<std::string> registered_events_;
    std::map<unsigned long long, std::vector<Event>> event_queue_; /**< Holds which events there are in a given logic step */

    unsigned long long render_objects_index_ = 1; // This will increase by 1 when the objects change so you can reference if you need an update
    std::set<std::string> render_objects_systems;
    
    /**
     * @brief Handles an event given depending on its attributes
     * 
     * @param event The event you want to handle
     * @return true All operations succeeded 
     * @return false An operation failed
     */
    virtual bool HandleEvent(Event& event);

    /**
     * @brief Handles the logic events registered in the system (will not advance frame)
     * 
     * @return true 
     * @return false 
     */
    virtual bool HandleEvents();

    public:
    LogicRegistrar() = delete;
    LogicRegistrar(application::BaseApplication& global_context);
    ~LogicRegistrar() {}
    
    // -- Getters and setters

    unsigned long long logic_step() { return logic_step_; }

    virtual entities::EntityRegistrar& entity_registrar() { return related_entity_registrar_; }
    virtual components::ComponentRegistrar& component_registrar() { return related_component_registrar_; }
    virtual systems::SystemRegistrar& system_registrar() { return related_system_registrar_; }

    unsigned long long render_objects_index() { return render_objects_index_; }
    std::vector<std::pair<const glm::mat3*, std::string>> get_render_objects(unsigned long long& index_store);
    // -- End of Getters and Setters

    /**
     * @brief Initialises the logic registrar and all related registrars
     * 
     * @return true 
     * @return false 
     */
    virtual bool Initialise(const application::LogicRegistrarSettings& logic_registrar_settings);

    /**
     * @brief Does all the things needed for the first frame of the engine (load initial scenes etc...)
     * 
     * @return true No errors occurred
     * @return false Errors occurred
     */
    virtual bool FirstFrame();

    /**
     * @brief Advances to next logic step (assumes no more logic for current frame)
     * @return true Advanced
     * @return false Did not advance
     */
    virtual bool AdvanceFrame();

    /**
     * @brief Does all the logic required for the end of the current frame. Essentially puts the registrar into the position of 'End of Frame'
     * and ready for the frame to be advanced. 
     * @return true No errors
     * @return false Some errors
     */
    virtual bool EndOfFrame();

    /**
     * @brief Cleans up the registrar for deletion
     * 
     */
    virtual void CleanUp() { return; }

    /**
     * @brief Handles a logic step for the registrar
     * 
     */
    virtual void HandleLogicStep();

    /**
     * @brief Causes an event to be handled immediately.
     * 
     * @param event The event you want to handle
     * @return true Event was handled correctly
     * @return false There was an error handling event
     */
    bool DispatchEvent(Event&& event);

    /**
     * @brief Adds an event to be handled within the event queue. Will be added dirrectly to the next frames logic step.
     * 
     * @param event The event you want to add to the queue
     * @return true The event was added successfully
     * @return false The event was not added
     */
    bool QueueEvent(Event&& event);

    /**
     * @brief Gathers the model matrices of the system to be drawn
     * 
     * @return std::vector<std::pair<glm::mat4, std::string>> A list of model matrices paired with their graphics name
     */
    std::vector<std::pair<glm::mat4, std::string>> GatherModelMatrices();
};

}
#endif