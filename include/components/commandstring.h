#ifndef VVV_COMMANDSTRING_H_INCLUDED
#define VVV_COMMANDSTRING_H_INCLUDED

#include <string>

namespace strings {
    class CommandString {
        protected:

        bool has_built_ = false;      /*< Checks if BuildString has been called for current plainText */
        std::string plain_text_ = ""; /*< The command string in its plain text form */ 

        public:

        CommandString(){} 

        CommandString(std::string pt) : plain_text_(pt) {}

        /**
         * @brief Get the plain text version of this command string
         * @return std::string Plain text version of the string being represented
         */
        std::string plain_text() { return plain_text_; }

        /**
         * @brief Set the plain text of this Command String
         * @param plainString The new text
         */
        void SetString(std::string plain_string) { 
            plain_text_ = plain_string; 
            has_built_ = false;
        }
        
        /**
         * @brief Builds a string out of the plain text incorporating commands that were contained within
         * @return std::string The final built string
         */
        std::string BuildString();

        /**
         * @brief Returns the built string of this Command String
         */
        std::string GetBuiltString();
    };
}
#endif