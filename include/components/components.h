#ifndef VVV_COMPONENTS_H_INCLUDED
#define VVV_COMPONENTS_H_INCLUDED

#include <string>

namespace components {

static const std::string kVVVComponentRegisterName = "VVV_CoreSystemRegister";
static const std::string k2DGraphicType = "VVV_2DGraphics";

}
#endif