#ifndef VVV_COMPONENTLIST_H_INCLUDED
#define VVV_COMPONENTLIST_H_INCLUDED

#include <memory>
#include <stack>
#include <vector>
#include <iostream>
#include <any>
#include <functional>
#include <map>
#include <set>
#include <bitset>
#include <typeinfo>

#include "application/application.h"
#include "application/console.h"

namespace components {

    class ComponentList {
        private:
        application::BaseApplication& global_context_;
        std::any default_type_value_;
         
        struct FrameData {
            bool has_changed = false;
            bool has_loaded = false;

            std::set<std::string> locks;
            std::size_t count = 0;                              /**< The amount of active components */
            std::vector<std::any> component_data;               /**< Holds all the components, and associates them with an index */
            std::set<std::size_t> created_this_frame;           /**< Indices that were created this frame */
            std::set<std::size_t> modified_this_frame;          /**< Indices that were modified this frame */
            std::set<std::size_t> deleted_this_frame;           /**< Indices that were deleted this frame*/
            std::vector<bool> used_space;                       /**< Indicates which index holds valid value i.e non-deleted components */
        };
        
        // These control whether the data needs to be run through a secondary function when it has changed/created
        // This will happen after end of frame but before the data is transferred from next frame to current frame
        bool secondary_calculation_ = false;
        std::function<void(std::any&)> secondary_calculation_function_;

        std::set<std::size_t> deleted_values_;                                                      /**< Indices to be overwritten when new components are added */
        std::map<std::size_t, std::vector<std::function<void(std::size_t)>>> deletion_callbacks_;   /**< Function callbacks when a component is deleted */

        FrameData current_frame_;
        FrameData next_frame_;

        /**
         * @brief Handles the flags for a given compent type representation
         * 
         * @param component_type The data for the flags you want to handle
         * @return true No errors when handling flags
         * @return false Some errors when handling flags
         */
        bool HandleFlags(const application::FlagData& flag_data) {
            bool flag_error = false;
            if (next_frame_.has_loaded) {
                if (flag_data.flags & application::kLock) {
                    if (!AddLock(flag_data.lock_owner_id)) flag_error = true;
                }
            }
            return !flag_error;
        }

        /**
         * @brief Undoes the handling of flags
         * 
         * @param component_type The flags to be undone
         * @return true No errors when undoing 
         * @return false Some errors when undoing
         */
        bool UndoHandleFlags(const application::FlagData& flag_data) {
            bool flag_error = false;
            if (flag_data.flags & application::kLock) {
                if (!UndoAddLock(flag_data.lock_owner_id).second) flag_error = true;
            }
            return !flag_error;
        }
        
        public:

        ComponentList() = delete;
        ComponentList(application::BaseApplication& globalC) : global_context_(globalC) {}

        bool AdvanceFrame() {
            // Handle secondary calculations
            if (secondary_calculation_) {
                for (auto new_index : next_frame_.created_this_frame) {
                    secondary_calculation_function_(next_frame_.component_data[new_index]);
                }
                for (auto changed_index : next_frame_.modified_this_frame) {
                    secondary_calculation_function_(next_frame_.component_data[changed_index]);
                }
            }
            if (next_frame_.has_changed) {
                current_frame_ = next_frame_;
                next_frame_.has_changed = false;
                next_frame_.created_this_frame.clear();
                next_frame_.modified_this_frame.clear();
                next_frame_.deleted_this_frame.clear();
                std::copy(current_frame_.deleted_this_frame.begin(),current_frame_.deleted_this_frame.end(), std::inserter(deleted_values_, deleted_values_.begin()));
            }
            return true;
        }

        bool EndOfFrame() {
            bool no_errors = true;
            if (!current_frame_.has_loaded && next_frame_.has_loaded) {
                // Load event happens
                //TODO: Do Load event properly
            }
            if (current_frame_.has_loaded && !next_frame_.has_loaded) {
                // Unload event happens
                deleted_values_.clear();
                for (std::size_t i = 0; i < next_frame_.used_space.size(); i++) {
                    if (next_frame_.used_space[i]) next_frame_.deleted_this_frame.emplace(i);
                }
                next_frame_.count = 0;
                next_frame_.has_loaded = false;
                next_frame_.has_changed = true;
                next_frame_.component_data.clear();    
                next_frame_.used_space.clear();
            }
            for (auto& deleted_index : next_frame_.deleted_this_frame) {
                if (deletion_callbacks_.contains(deleted_index)) {
                    for (auto& callback : deletion_callbacks_[deleted_index]) {
                        callback(deleted_index);
                    }
                    deletion_callbacks_.erase(deleted_index);
                }
            } 
            return no_errors;
        }


        template<class T>
        void InitialiseList() {
            current_frame_ = {};
            next_frame_ = {};
            default_type_value_ = T();
        }

        template<class T>
        void InitialiseList(T&& default_value) {
            current_frame_ = {};
            next_frame_ = {};
            default_type_value_ = default_value;
        }
        
        void InitialiseList(std::any value_type) {
            current_frame_ = {};
            next_frame_ = {};
            default_type_value_ = value_type;
        }
        
        template <class T>
        bool IsOfType() const {
            return (typeid(T) == default_type_value_.type());
        }

        bool IsWellFormed() { return std::any().type() != default_type_value_.type(); }

        bool HasSameType(const std::any& data) { return default_type_value_.type() == data.type(); }

        bool CanCreateComponent() { return IsWellFormed() &&
                                           next_frame_.has_loaded;
        } 

        bool HasChanged() { return current_frame_.created_this_frame.size() || current_frame_.deleted_this_frame.size() || current_frame_.modified_this_frame.size(); }

        // Only correct after an end of frame operation
        bool WillChange() { return next_frame_.created_this_frame.size() || next_frame_.deleted_this_frame.size() || next_frame_.modified_this_frame.size(); }
        
        bool Clear() {
            current_frame_ = {};
            next_frame_ = {};
            deleted_values_.clear();
            for (auto& callbacks : deletion_callbacks_) {
                for (auto& function : callbacks.second) {
                    function(callbacks.first);
                }
            }
            deletion_callbacks_.clear();
            return true;
        }

        std::size_t has_lock(std::string lock_name) { return current_frame_.locks.contains(lock_name); }

        std::size_t is_locked() { return current_frame_.locks.size(); }

        bool loaded() { return current_frame_.has_loaded; }

        /**
         * @brief Sets the list to be loaded next frame
         * @return true The list will be loaded next frame
         * @return false The list will not be loaded next frame
         */
        bool Load(const application::FlagData& flag_data) {
            if (!current_frame_.has_loaded && !next_frame_.has_loaded) {
                next_frame_.has_loaded = true;
                next_frame_.has_changed = true;
            }
            if (!HandleFlags(flag_data)) {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::Load(const application::ComponentTypeRepresentation& component_type)",
                    "Invalid Flags",
                    "Trying to handle flags in a component list but there was an error."
                );
            }
            return next_frame_.has_loaded;
        }

        /**
         * @brief Undoes the loading of a list
         * 
         * @return std::pair<bool, bool> The bools represent <A Load was undone, The list will not be loaded next frame>
         */
        std::pair<bool, bool> UndoLoad(const application::FlagData& flag_data)  {
            std::pair<bool, bool> return_pair = {false, !next_frame_.has_loaded};
            if (!UndoHandleFlags(flag_data)) {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::UndoLoad(const application::ComponentTypeRepresentation& component_type)",
                    "Error Undoing Flags",
                    "Trying to undo the handling of flags in a component list but there was an error."
                );
                return return_pair;
            }
            if (!current_frame_.has_loaded && next_frame_.has_loaded && next_frame_.locks.size() == 0) {
                next_frame_.has_loaded = false;
                next_frame_.has_changed = true;
                return_pair = {true, true};
            }
            return return_pair;
        }

        /**
         * @brief Sets the list to be unloaded next frame
         * @return true The list will be unloaded next frame
         * @return false The list will still be loaded next frame
         */
        bool Unload() {
            if (next_frame_.locks.size() == 0 && current_frame_.has_loaded && next_frame_.has_loaded) {
                next_frame_.has_loaded = false;
                next_frame_.has_changed = true;
            }
            return !next_frame_.has_loaded;
        }

        /**
         * @brief Undoes the loading of a list
         * 
         * @return std::pair<bool, bool> The bools represent <An Unload was undone, The list will be loaded next frame>
         */
        std::pair<bool, bool> UndoUnload() {
            std::pair<bool, bool> return_pair = {false, next_frame_.has_loaded};
            if (current_frame_.has_loaded && !next_frame_.has_loaded) {
                next_frame_.has_loaded = true;
                next_frame_.has_changed = true;
                return_pair = {true, true};
            }
            return return_pair;
        }

        /**
         * @brief Adds a lock to the component list
         * @param lock_name The name of the lock (used to identify said lock)
         * @return true A new lock was added successfully 
         * @return false A new lock could not be added
         */
        bool AddLock(std::string lock_name) {
            if (lock_name.size() != 0) {
                if (!next_frame_.has_loaded) {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::AddLock(std::string)",
                        "Invalid List",
                        fmt::format("Trying add a lock with Name {}, but the list will not be loaded next frame.", lock_name)
                    );
                    return false;
                } 
                if (!current_frame_.locks.contains(lock_name) && !next_frame_.locks.contains(lock_name)) {
                    next_frame_.locks.emplace(lock_name);
                    next_frame_.has_changed = true;
                    return true;
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::AddLock(std::string)",
                        "Invalid Lock",
                        fmt::format("Trying add a lock with Name {}, but it already has a lock with that name.", lock_name)
                    );
                    return false;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::AddLock(std::string)",
                    "Invalid Lock Name",
                    "Trying to add a Lock with an empty name."
                );
                return false;
            }
        }

        /**
         * @brief Undoes the adding of a lock
         * 
         * @param lock_name The name of the lock (used to identify said lock)
         * @return std::pair<bool, bool> The bools represent <The adding of a lock was undone, The lock will not exist next frame>
         */
        std::pair<bool, bool> UndoAddLock(std::string lock_name) {
            std::pair<bool, bool> return_pair = {false, !next_frame_.locks.contains(lock_name)};
            if (lock_name.size() != 0) {
                if (!current_frame_.locks.contains(lock_name) && next_frame_.locks.contains(lock_name)) {
                    next_frame_.locks.erase(lock_name);
                    next_frame_.has_changed = true;
                    return_pair = {true, true};
                    return return_pair;
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::UndoAddLock(std::string)",
                        "Invalid Lock",
                        fmt::format("Trying to undo adding a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
                    );
                    return return_pair;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::UndoAddLock(std::string)",
                    "Invalid Lock Name",
                    "Trying to undo adding a Lock with an empty name."
                );
                return return_pair;
            }
        }

        /**
         * @brief Unlocks a lock on the list
         * @param lock_name The name of the lock to unlock
         * @return true The lock was successfully unlocked
         * @return false No lock was unlocked
         */
        bool Unlock(std::string lock_name) {
            if (lock_name.size() != 0) {
                if (current_frame_.locks.contains(lock_name)) {
                    if (next_frame_.locks.contains(lock_name)) {
                        next_frame_.locks.erase(lock_name);
                        next_frame_.has_changed = true;
                        return true;
                    } else {
                        global_context_.main_console().IssueWarning(
                            "components::ComponentList::Unlock(std::string)",
                            "Invalid Lock",
                            fmt::format("Trying to ulock a lock with Name {}, but it has already been unlocked.", lock_name)
                        );
                        return false;
                    }
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::Unlock(std::string)",
                        "Invalid Lock",
                        fmt::format("Trying to unlock a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
                    );
                    return false;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::Unlock(std::string)",
                    "Invalid Lock Name",
                    "Trying to unlock a Lock with an empty name."
                );
                return false;
            }
        }

        /**
         * @brief Undoes an unlock on the list
         * 
         * @param lock_name The name of the lock to undo
         * @return std::pair<bool, bool> The bools represent <The unlocking of a lock was undone, The lock will exist next frame>
         */
        std::pair<bool, bool> UndoUnlock(std::string lock_name) {
            std::pair<bool, bool> return_pair = {false, next_frame_.locks.contains(lock_name)};
            if (lock_name.size() != 0) {
                if (current_frame_.locks.contains(lock_name)) {
                    if (!next_frame_.locks.contains(lock_name)) {
                        next_frame_.locks.emplace(lock_name);
                        next_frame_.has_changed = true;
                        return_pair = {true, true};
                        return return_pair;
                    } else {
                        global_context_.main_console().IssueWarning(
                            "components::ComponentList::UndoAddLock(std::string)",
                            "Invalid Lock",
                            fmt::format("Trying to undo an unlock of a lock with Name {}, but it has not been unlocked.", lock_name)
                        );
                        return return_pair;
                    }
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::UndoAddLock(std::string)",
                        "Invalid Lock",
                        fmt::format("Trying to undo an unlock of a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
                    );
                    return return_pair;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::UnLock(std::string)",
                    "Invalid Lock Name",
                    "Trying to undo an unlock for a Lock with an empty name."
                );
                return return_pair;
            }
        }
        
        //--These 'read' operations operate on the current frame

        const std::vector<std::function<void(std::size_t)>>* deletion_callbacks(std::size_t component_index) {
            if (HasObject(component_index)) {
                return &deletion_callbacks_[component_index];
            }
            global_context_.main_console().IssueWarning(
                "ComponentList::deletion_callbacks(std::size_t)", 
                "Invalid Index",
                fmt::format("Trying to get component deletion callbacks with ID: {}", component_index)
            );
            return nullptr;
        }

        // Returns a copy to the component represented by the index value
        template<class T>
        T get(std::size_t index) const {
            if (IsOfType<T>()) {
                if (HasObject(index)) {
                    return (std::any_cast<T>(current_frame_.component_data[index]));
                } else {
                    // Index Mismatch
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::get<T>(std::size_t)", 
                        "Invalid Index",
                        fmt::format("Trying to get component with ID: {}", index)
                    );
                    return (std::any_cast<T>(default_type_value_));
                }
            } else {
                // Type mismatch
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::get<T>(std::size_t)", 
                    "Invalid Type",
                    fmt::format("Trying to get component with ID: {}", index)
                );
                return T();
            }
        }

        // Returns whether the index in list has a valid object
        bool HasObject(std::size_t index) const { 
            if (index < current_frame_.component_data.size()) {
                return current_frame_.used_space[index];
            }
            return false;
        }

        /**
         * @brief Returns a count of the number of components
         * @return std::size_t The count
         */
        std::size_t count() const { return current_frame_.count; }

        bool HasDeletionCallback(std::size_t index) {
            if (deletion_callbacks_.contains(index)) {
                return (deletion_callbacks_[index].size() > 0);
            }
            return false;
        }

        //--End of current frame 'read' operations

        //--These 'read' operations operate on the next frame
        
        bool WillHaveObject(std::size_t index) const {
            if (index < next_frame_.component_data.size()) {
                return next_frame_.used_space[index];
            }
            return false;
        }

        // This returns the value at the next frame's index
        template<class T>
        T query_value(std::size_t index) {
            if (WillHaveObject(index)) {
                if (IsOfType<T>()) {
                    return (std::any_cast<T>(next_frame_.component_data[index]));
                } else {
                    // Type mismatch
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::query_values<T>(std::size_t)", 
                        "Invalid Type",
                        fmt::format("Trying to query component with ID {}", index)
                    );
                    return T();
                }
            } else {
                // Index Mismatch
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::query_values<T>(std::size_t)", 
                    "Invalid Index",
                    fmt::format("Trying to query component with ID {}", index)
                );
                return T();
            }
        }

        //--End of next frame 'read' operations

        //--These 'write' operations operate on the next frame

        // Explicitly defining a bunch of functions to avoid one bloated one in a much needed function
        // (Speed concerns, should be valid?)

        // Makes a component and returns the index it takes in the list
        // Will return -1 if it cant create one
        std::size_t CreateNewComponent() {
            std::size_t return_int = -1;
            if (CanCreateComponent()) {
                if (deleted_values_.size() != 0) {
                    // Deletion will construct an empty value
                    return_int = *deleted_values_.begin();
                    deleted_values_.erase(deleted_values_.begin()); 
                    next_frame_.used_space[return_int] = true;
                    next_frame_.count++;
                }else {
                    return_int = next_frame_.component_data.size();
                    next_frame_.component_data.emplace_back(default_type_value_);
                    next_frame_.used_space.emplace_back(true);
                    next_frame_.count++;
                }
                next_frame_.created_this_frame.emplace(return_int);
                next_frame_.has_changed = true;
            }
            return return_int;
        }

        std::size_t CreateNewComponent(std::any component_data) {
            if (component_data.type() != default_type_value_.type()) {
                global_context_.main_console().IssueWarning(
                        "components::ComponentList::CreateNewComponent(std::any)", 
                        "Invalid Type",
                        "Given data isn't of the same type as the list."
                );
                return -1;
            }
            std::size_t return_int = CreateNewComponent();
            if (return_int != (std::size_t) -1) {
                next_frame_.component_data[return_int] = component_data;
            } else {
                global_context_.main_console().IssueWarning(
                        "components::ComponentList::CreateNewComponent(std::any)", 
                        "Error",
                        "Could not create a new blank component."
                );
            }
            return return_int;
        }
        
        std::size_t CreateNewComponent(std::function<void(std::size_t)> callback){
            std::size_t return_int = CreateNewComponent();
            if (return_int != (std::size_t) -1) {
                deletion_callbacks_[return_int].emplace_back(std::move(callback));
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::CreateNewComponent(std::function<void(std::size_t)> )", 
                    "Error",
                    "Could not create a new blank component."
                );
            }
            return return_int;
        }

        std::size_t CreateNewComponent(std::any component_data, std::function<void(std::size_t)> callback) {
            if (component_data.type() != default_type_value_.type()) {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::CreateNewComponent(std::any, std::function<void(std::size_t)>)", 
                    "Invalid Type",
                    "Component data supplied does not match list"
                );
                return -1;
            }
            std::size_t return_int = CreateNewComponent(component_data);
            if (return_int != (std::size_t) -1) {
                deletion_callbacks_[return_int].emplace_back(std::move(callback));
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::CreateNewComponent(std::any, std::function<void(std::size_t)>)", 
                    "Error",
                    "Could not create component"
                );
            }
            return return_int;
        }
        
        std::size_t CreateNewComponent(std::vector<std::function<void(std::size_t)>> callbacks){
            std::size_t return_int = CreateNewComponent();
            if (return_int != (std::size_t) -1) {
                deletion_callbacks_[return_int] = std::move(callbacks);
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::CreateNewComponent(std::vector<std::function<void(std::size_t)>>)", 
                    "Error",
                    "Could not create component"
                );
            }
            return return_int;
        }

        std::size_t CreateNewComponent(std::any component_data, std::vector<std::function<void(std::size_t)>> callbacks){
            if (component_data.type() != default_type_value_.type()) {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::CreateNewComponent(std::any, std::vector<std::function<void(std::size_t)>>)", 
                    "Invalid Type",
                    "Component data supplied does not match list"
                );
                return -1;
            }
            std::size_t return_int = CreateNewComponent(component_data);
            if (return_int != (std::size_t) -1) {
                deletion_callbacks_[return_int] = std::move(callbacks);
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::CreateNewComponent(std::any, std::vector<std::function<void(std::size_t)>>)", 
                    "Error",
                    "Could not create component"
                );
            }
            return return_int;
        }

        /**
         * @brief Undoes the creation of a component
         * 
         * @param component_index The component to undo
         * @return std::pair<bool, bool> The bools represent <The creation of a component was undone, The component will not exist next frame>
         */
        std::pair<bool, bool> UndoCreation(std::size_t component_index) {
            if (!HasObject(component_index) && WillHaveObject(component_index)) { 
                next_frame_.used_space[component_index] = false;
                next_frame_.component_data[component_index] = default_type_value_;
                deleted_values_.emplace(component_index);
                next_frame_.count--;
                for (auto& callback : deletion_callbacks_[component_index]) {
                    callback(component_index);
                }
                deletion_callbacks_.erase(component_index);
                next_frame_.created_this_frame.erase(component_index);
                next_frame_.has_changed = true;
                return {true, true};
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::UndoCreation(std::size_t)", 
                    "Invalid ID",
                    fmt::format("Object will not exist with given ID {}", component_index)
                );
                return {false, true};
            }
        }

        // Deletes the component at the given index
        // Returns true if successful   
        bool DeleteComponent(std::size_t index) {
            if (HasObject(index)) {
                if (!WillHaveObject(index)) { return true; }
                next_frame_.used_space[index] = false;
                next_frame_.component_data[index] = default_type_value_;
                next_frame_.deleted_this_frame.emplace(index);
                next_frame_.count--;
                next_frame_.has_changed = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::DeleteComponent(std::size_t)", 
                    "Invalid ID",
                    fmt::format("Object does not exist with given ID {}", index)
                );
                return false;
            }
        }

        /**
         * @brief Undoes the deletion of a component
         * 
         * @param component_index The component to undo
         * @return std::pair<bool, bool> The bools represent <The deletion of a component was undone, The component will exist next frame>
         */
        std::pair<bool, bool> UndoDeletion(std::size_t component_index) {
            std::pair<bool, bool> return_pair = {false, WillHaveObject(component_index)};
            if (!WillHaveObject(component_index)) {
                if (HasObject(component_index)) {
                    next_frame_.used_space[component_index] = true;
                    next_frame_.component_data[component_index] = current_frame_.component_data[component_index];
                    next_frame_.deleted_this_frame.erase(component_index);
                    next_frame_.count++;
                    next_frame_.has_changed = true;
                    return_pair = {true, true};
                    return return_pair;
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::UndoDeletion(std::size_t)", 
                        "Invalid ID",
                        fmt::format("Trying to undo deletion of component that doesn't exist with ID {}", component_index)
                    );
                    return return_pair;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::UndoDeletion(std::size_t)", 
                    "Invalid ID",
                    fmt::format("Trying to undo deletion of component that wasn't deleted with ID {}", component_index)
                );
                return return_pair;
            }
        } 

        // Sets the component at 'index' to 'value'
        template<class T>
        bool SetComponent(std::size_t index, T&& value) {
            if (IsOfType<T>()) {
                if (WillHaveObject(index)) {
                    if (HasObject(index)) {
                        next_frame_.component_data[index] = value;
                        next_frame_.modified_this_frame.emplace(index);
                        next_frame_.has_changed = true;
                        return true;
                    } else {
                        global_context_.main_console().IssueWarning(
                            "components::ComponentList::SetComponent<T>(std::size_t, T&&)", 
                            "Invalid ID",
                            fmt::format("Trying to set component that doesn't exist yet with ID {}", index)
                        );
                        return false;
                    }
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::SetComponent<T>(std::size_t, T&&)", 
                        "Invalid ID",
                        fmt::format("Trying to set component that will be deleted with ID {}", index)
                    );
                    return false;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::SetComponent<T>(std::size_t, T&&)", 
                    "Invalid Type",
                    fmt::format("Trying to set component with a mistmatching type with ID {}", index)
                );
                return false;
            }
        }

        /**
         * @brief Undoes the setting of a component
         * 
         * @param component_index The component to undo
         * @return std::pair<bool, bool> The bools represent <The changes of a component was undone, The component will stay the same next frame>
         */
        std::pair<bool, bool> UndoSetComponent(std::size_t component_index) {
            if (WillHaveObject(component_index)) {
                if (HasObject(component_index)) {
                    next_frame_.component_data[component_index] = current_frame_.component_data[component_index];
                    next_frame_.modified_this_frame.erase(component_index);
                    next_frame_.has_changed = true;
                    return {true, true};
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::UndoSetComponent(std::size_t)", 
                        "Invalid ID",
                        fmt::format("Trying to undo set of component that doesn't exist yet with ID {}", component_index)
                    );
                    return {false, false};
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::UndoSetComponent(std::size_t)", 
                    "Invalid ID",
                    fmt::format("Trying to undo set of component that will not exist with ID {}", component_index)
                );
                return {false, false};
            }
        }

        /**
         * @brief Adds a deletion callback to a component
         * @param component_index Component to add to
         * @param callback Function to add
         * @return true Added successfully
         * @return false No change has happened
         */
        bool AddDeletionCallback(std::size_t component_index, std::function<void(std::size_t)> callback) {
            if (WillHaveObject(component_index)) {
                if (HasObject(component_index)) {
                    deletion_callbacks_[component_index].emplace_back(std::move(callback));
                    next_frame_.has_changed = true;
                    return true;
                } else {
                    global_context_.main_console().IssueWarning(
                        "components::ComponentList::AddDeletionCallback(std::size_t, std::function<void(std::size_t)>)", 
                        "Invalid ID",
                        fmt::format("Trying to add a deletion callbacl to a component that does not exist with ID {}", component_index)
                    );
                    return false;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentList::AddDeletionCallback(std::size_t, std::function<void(std::size_t)>)", 
                    "Invalid ID",
                    fmt::format("Trying to add a deletion callbacl to a component that will not exist with ID {}", component_index)
                );
                return false;
            }
        }

        //--End of next frame 'write' operations
    };
}
#endif