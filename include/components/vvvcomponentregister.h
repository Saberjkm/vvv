#ifndef VVV_VVVCOMPONENTREGISTRAR_H_INCLUDED
#define VVV_VVVCOMPONENTREGISTRAR_H_INCLUDED

#include "components/componentregister.h"
#include "graphics/graphicsdata.h"
#include "components/components.h"

namespace components {

class VVVComponentRegister : public ComponentRegister {
    protected:

    public:

    VVVComponentRegister() = delete;
    VVVComponentRegister(ComponentRegistrar& parent_registrar, std::string register_name) : ComponentRegister(parent_registrar, register_name) {
        component_types_[k2DGraphicType] = graphics::ModelData2D();
    }

    ~VVVComponentRegister() {};
};

}
#endif