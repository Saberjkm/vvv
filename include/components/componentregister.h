#ifndef VVV_COMPONENTREGISTER_H_INCLUDED
#define VVV_COMPONENTREGISTER_H_INCLUDED

#include <any>
#include <map>
#include <string>

namespace components {
class ComponentRegistrar;

class ComponentRegister {
    protected:
    std::string register_name_;

    ComponentRegistrar& parent_registrar_; 

    std::map<std::string, std::any> component_types_;

    public:

    ComponentRegister() = delete;
    
    ComponentRegister(ComponentRegistrar& parent_registrar, std::string register_name) : 
        parent_registrar_(parent_registrar), 
        register_name_(register_name)
    {}

    virtual ~ComponentRegister() {};

    virtual std::any GetComponentType(std::string component_label);

    /**
     * @brief All component types within the register are registered within the parent registrar
     * 
     */
    virtual void LoadAllComponentTypes();
};

}

#endif