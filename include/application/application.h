#ifndef VVV_APPLICATION_H_INCLUDED
#define VVV_APPLICATION_H_INCLUDED


#include <iostream>
#include <vector>
#include <string>
#include <any>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

namespace console { class SystemConsole; }

namespace application {
struct LogicRegistrarSettings;

/**
 * @brief The various loading data flags for use in data management
 * 
*/
enum kAttributeMask {
    kNone = 0,          /** No flags are set */
    kKeepLoaded = 1,    /** The data should be kept loaded */
    kLock = 2           /** The data should be locked */
};

struct FlagData {
    std::size_t flags;
    std::string lock_owner_id;

    bool operator==(const FlagData& other_flag_data) const {
        return flags == other_flag_data.flags &&
               lock_owner_id == other_flag_data.lock_owner_id;
    }
};
static const FlagData kNoFlags = {}; /* Used to provide an empty flag data reference for various functions */

/**
 * @brief An Intermediary Storage for a Component to be loaded into the system
 * 
 */
struct ComponentRepresentation {
    std::string component_label;
    std::any component_value;

    bool operator==(const ComponentRepresentation& other_component_representation) const {
        return component_label == other_component_representation.component_label;
    }
};

/**
 * @brief An Intermediary Storage for a Component Type to be loaded into the system
 * 
 */
struct ComponentTypeRepresentation {
    std::string component_label;
    FlagData flag_data;

    bool operator==(const ComponentTypeRepresentation& other_component_type_representation) const {
        return component_label == other_component_type_representation.component_label &&
               flag_data == other_component_type_representation.flag_data;
    }
};


/**
 * @brief An Intermediary Storage for an Entity to be loaded into the system
 * 
 */
struct EntityRepresentation {
    std::string register_label;
    std::string scene_label;
    std::string entity_label;
    FlagData flag_data;

    bool operator==(const EntityRepresentation& other_entity_representation) const {
        return register_label == other_entity_representation.register_label &&
               scene_label == other_entity_representation.scene_label &&
               entity_label == other_entity_representation.entity_label &&
               flag_data == other_entity_representation.flag_data;
    }
};

/**
 * @brief An Intermediary Storage for a System to be loaded into the system
 * 
 */
struct SystemRepresentation {
    std::string system_label;
    FlagData flag_data;

    bool operator==(const SystemRepresentation& other_system_representation) const {
        return system_label == other_system_representation.system_label &&
               flag_data == other_system_representation.flag_data;
    }
};

/**
 * @brief A form of storage that is suitable for an Entity that has been loaded into the system
 * 
 */
struct EntityData{
    bool is_valid = false;
    std::string entity_label;
    std::size_t id = -1;
    FlagData flag_data;

    bool operator==(const EntityData& other_entity_data) const {
        return is_valid == other_entity_data.is_valid &&
               entity_label == other_entity_data.entity_label &&
               id == other_entity_data.id &&
               flag_data == other_entity_data.flag_data;
    }
};

/**
 * @brief A form of storage that is suitable for a Component Type that has been loaded into the system
 * 
 */
struct ComponentTypeData {
    bool is_valid = false;
    std::string label;
    FlagData flag_data;

    bool operator==(const ComponentTypeData& other_component_type_data) const {
        return is_valid == other_component_type_data.is_valid &&
               label == other_component_type_data.label &&
               flag_data == other_component_type_data.flag_data;
    }
};

/**
 * @brief A form of storage that is suitable for a System that has been loaded into the system
 * 
 */
struct SystemData {
    bool is_valid = false;
    std::string label;
    FlagData flag_data;

    bool operator==(const SystemData& other_system_data) const {
        return is_valid == other_system_data.is_valid &&
               label == other_system_data.label &&
               flag_data == other_system_data.flag_data;
    }
};

class BaseApplication {
    public:
    BaseApplication(){}
    
    virtual console::SystemConsole& main_console() = 0;
    
    virtual ~BaseApplication() = default;
    BaseApplication(const BaseApplication& other) = default;
    BaseApplication(BaseApplication&& other) = default;
    BaseApplication& operator=(const BaseApplication& other) = default;
    BaseApplication& operator=(BaseApplication&& other) = default;

    virtual void Run(const LogicRegistrarSettings& logic_registrar_settings) { return; };
    virtual void TerminateApplication() { return; }

};

}
#endif