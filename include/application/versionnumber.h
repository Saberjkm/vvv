#ifndef VVV_VERSIONNUMBER_H_INCLUDED
#define VVV_VERSIONNUMBER_H_INCLUDED

#include <regex>
namespace application {
static std::regex kVersionMatcher("(0|[1-9][0-9]*).(0|[1-9][0-9]*).(0|[1-9][0-9]*)+[abrz]");

class VersionNumber {
    protected:

    unsigned short int major_number_ = -1;
    unsigned short int minor_number_ = -1;
    unsigned short int patch_number_ = -1;
    char version_type_ = 'z';

    public:

    bool operator==(const VersionNumber& rhs) const {
        return this->major_number_ == rhs.major_number_ &&
               this->minor_number_ == rhs.minor_number_ &&
               this->patch_number_ == rhs.patch_number_ &&
               this->version_type_ == rhs.version_type_;
    }

    unsigned short int major_number() { return major_number_;}
    void set_major_number(unsigned short int major_number) { major_number_ = major_number; }

    unsigned short int minor_number() { return minor_number_;}
    void set_minor_number(unsigned short int minor_number) { minor_number_ = minor_number; }

    unsigned short int patch_number() { return patch_number_;}
    void set_patch_number(unsigned short int patch_number) { patch_number_ = patch_number; }

    char version_type() { return version_type_; }
    void set_version_type(char version_type) { version_type_ = version_type; }

    std::string get_string() { return std::to_string(major_number_) + "." + std::to_string(minor_number_) + "." + std::to_string(patch_number_) + version_type_; }

    bool is_valid() { return ((major_number_ != (unsigned short int) -1) && 
                              (minor_number_ != (unsigned short int) -1) && 
                              (patch_number_ != (unsigned short int) -1) &&
                              (version_type_ == 'a' || version_type_ == 'b' || version_type_ == 'r')); }
};

VersionNumber CreateVersionNumber(std::string version_number);

}

#endif