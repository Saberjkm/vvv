#ifndef VVV_GAMEAPPLICATION_H_INCLUDED
#define VVV_GAMEAPPLICATION_H_INCLUDED

#include "application/console.h"
#include "graphics/graphicsregistrar.h"
#include "logic/logicregistrar.h"

namespace application {


class GameApplication : public BaseApplication {
    protected:
    // Game stuff
    console::SystemConsole system_console_;

    logic::LogicRegistrar related_logic_registrar_;
    graphics::GraphicsRegistrar related_graphics_registrar_;

    const double kTargetStepTime = 1.0/60;
    double target_frame_rate_ = 1.0/240;
    double previous_time_ = 1;
    double current_time_ = 0;
    bool should_exit_ = false;

    public:

    GameApplication() :  related_logic_registrar_(*this), related_graphics_registrar_(*this, related_logic_registrar_) {}

    ~GameApplication() {};
    console::SystemConsole& main_console() override { return system_console_; }

    /**
     * @brief Starts the application
     */
    void Run(const LogicRegistrarSettings& logic_registrar_settings) override;

    /**
     * @brief Sends the command to shut down the application
     */
    void TerminateApplication() override;

    private:

    /**
     * @brief Cleans up the program for destruction
     */
    void CleanUp();

    /**
     * @brief Does all the set up for the Game Application
     */
    void InitialiseGame(const LogicRegistrarSettings& logic_registrar_settings);

    /**
     * @brief Does stuff that needs to be done before the game begins but after initialisation
     * 
     */
    void FirstFrame();

    /**
     * @brief Main loop for the application
     */
    void MainLoop();

};

}

#endif