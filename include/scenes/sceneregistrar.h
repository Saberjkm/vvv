#ifndef VVV_SCENEREGISTRAR_H_INCLUDED
#define VVV_SCENEREGISTRAR_H_INCLUDED

#include <filesystem>
#include <map>
#include <set>

#include "scenes/scenes.h"
#include "application/settings.h"

namespace application { class BaseApplication; }
namespace logic { class LogicRegistrar; }

namespace scenes {

class SceneRegister;
class VVVSceneRegister;

class SceneRegistrar {
    protected:
    application::BaseApplication& global_context_; 
    logic::LogicRegistrar& related_logic_registrar_;

    std::shared_ptr<VVVSceneRegister> core_scene_register_;

    std::map<std::string, std::shared_ptr<SceneRegister>> scene_registers_;
    std::vector<std::string> initial_scenes_; // Holds the scenes which will load on the first frame event
    std::map<std::string, SceneInfo> scene_info_;
    std::map<std::string, SceneData> scene_data_;

    struct FrameData {
        std::set<std::string> changed_scenes_this_frame;
        std::set<std::string> unloaded_scenes_this_frame;
        std::set<std::string> loaded_scenes_this_frame;
        std::map<std::string, SceneData> new_scene_data; 
        std::string main_scene; // The scene which all new entities (not part of a scene load) are considered as part of.
        std::set<std::size_t> main_scene_entities;
    };

    FrameData current_frame_;
    FrameData next_frame_;

    /**
     * @brief Unloads the data for a given scene
     * 
     * @param scene_info The information about said scene
     * @param scene_data The data you want to unload
     * @return true It was fully unloaded
     * @return false It was not fully unloaded
     */
    bool UnloadSceneData(const SceneInfo& scene_info, SceneData& scene_data);

    public:

    SceneRegistrar() = delete;
    SceneRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar);

    bool ContainsRegister(std::string register_id) { return scene_registers_.contains(register_id); }
    bool HasSceneInfo(std::string scene_info_id) { return scene_info_.contains(scene_info_id); }
    SceneInfo GetSceneInfo(std::string scene_info_id) { return (scene_info_.contains(scene_info_id)) ? scene_info_[scene_info_id] : SceneInfo(); }
    bool SceneLoaded(std::string scene_id) { return scene_data_.contains(scene_id); }
    application::BaseApplication& global_context() { return global_context_; }
    logic::LogicRegistrar& logic_registrar() { return related_logic_registrar_; }
    
    /**
     * @brief Initialises the registrar
     * 
     * @return true All operations were successful
     * @return false An operation failed
     */
    virtual bool Initialise(const application::SceneRegistrarSettings& scene_registrar_settings);
    
    /**
     * @brief Does the things needed before the engine starts but after the engine is initialised
     * 
     * @return true 
     * @return false 
     */
    virtual bool FirstFrame();

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool AdvanceFrame();

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool EndOfFrame();

    /**
     * @brief Adds a new scene register to the registrar
     * @param register_name The register's id
     * @param register_ref The pointer to the register
     * @return true A new register was added successfully
     * @return false No new register was added
     */
    virtual bool AddSceneRegister(const std::string& register_label, const std::shared_ptr<SceneRegister>& register_ref);

    /**
     * @brief Adds info about a scene to the registrar
     * 
     * @param scene_info The info about the scene you want to add
     * @return true A scene info given was added to the registrar
     * @return false No new info was added to the registrar
     */
    virtual bool AddSceneInfo(SceneInfo scene_info);

    /**
     * @brief Loads all the scenes within scene_file_locations_ into the registrar.
     *        (Just loads the related scenes meta data into the registrar not the scenes related game data)
     */
    virtual void RegisterScenes();

    /**
     * @brief Loads a scene and all related things into the game
     * @param scene_id The ID of the scene to load
     * @return true All the data was loaded successfully
     * @return false Data couldn't be loaded
     */
    virtual bool LoadScene(std::string scene_id);

    /**
     * @brief Unloads a scene that has been loaded
     * @param scene_id The id of the scene
     * @return true All the data was unloaded successfully
     * @return false Data couldnt be unloaded
     */
    virtual bool UnloadScene(std::string scene_id);

    /**
     * @brief Adds an entity to the main scene for the next frame
     * 
     * @param entity_id The ID of the entity
     */
    virtual void AddEntity(std::size_t entity);
};

}
#endif