#ifndef VVV_VVVSCENEREGISTER_H_INCLUDED
#define VVV_VVVSCENEREGISTER_H_INCLUDED

#include "scenes/sceneregister.h"

namespace scenes {

class VVVSceneRegister : public SceneRegister {
    protected:

    public:
    VVVSceneRegister() = delete;

    VVVSceneRegister(SceneRegistrar& parent_registrar, std::string register_name) : SceneRegister(parent_registrar, register_name) {}

};

}

#endif