#ifndef VVV_SCENES_H_INCLUDED
#define VVV_SCENES_H_INCLUDED

#include <vector>
#include <string>
#include <bitset>
#include <map>
#include <any>

#include "application/application.h"
#include "entities/entities.h"

namespace scenes {
class SceneRegister;

static const std::string kVVVSceneRegisterName = "VVV_CoreSceneRegister";
static const std::string kVVVCoreSceneName = "VVV_CoreScene";

/** 
 * Holds the data that the scene register will use to load the scene into the engine
*/
struct SceneLoadData {
    std::string scene_label; // The Scene's ID label
    std::vector<application::EntityRepresentation> entities;
    std::vector<application::ComponentTypeRepresentation> component_types;
    std::vector<application::SystemRepresentation> systems;
};

struct SceneInfo {
    std::string related_register;
    std::string scene_label;
    bool override;
};

struct SceneData {
    bool is_valid = false;
    std::vector<application::EntityData> scene_entities;
    std::vector<application::ComponentTypeData> scene_component_types;
    std::vector<application::SystemData> scene_systems;
};

}

#endif