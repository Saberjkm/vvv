#ifndef VVV_SYSTEMS_H_INCLUDED
#define VVV_SYSTEMS_H_INCLUDED

#include <string>

namespace systems {

static const std::string kVVVSystemRegisterName = "VVV_Core";
static const std::string kVVV2DGraphicsSystemName = "VVV_2DGraphics";

}

#endif