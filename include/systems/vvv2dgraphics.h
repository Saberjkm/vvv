#ifndef VVV_VVV2DGRAPHICS_H_INCLUDED
#define VVV_VVV2DGRAPHICS_H_INCLUDED

#include "systems/system.h"

namespace systems {

class VVV2DGraphics : public AbstractSystem {
    protected:

    public:

    VVV2DGraphics(application::BaseApplication& globalC, logic::LogicRegistrar& relatedLR) : AbstractSystem(globalC, relatedLR) {}
};

}

#endif