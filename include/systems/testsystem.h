#ifndef VVV_TESTSYSTEM_H_INCLUDED
#define VVV_TESTSYSTEM_H_INCLUDED

#include "systems/system.h"

namespace systems {

class TestSystem : public AbstractSystem {

    public:

    TestSystem() = delete;

    TestSystem(application::BaseApplication& globalC, logic::LogicRegistrar& relatedLR)
        : AbstractSystem(globalC, relatedLR) {}
};

}
#endif