#ifndef VVV_SYSTEM_H_INCLUDED
#define VVV_SYSTEM_H_INCLUDED

#include <set>
#include <string>

#include "logic/logicregistrar.h"
namespace components { class ComponentRegistrar; }
namespace entities { class EntityRegistrar; }
namespace input { struct Key; }
namespace systems {

class AbstractSystem {
    protected:

    struct FrameData {
        bool has_loaded = false;
        bool has_changed = false;
        
        std::size_t system_id = -1;
        std::set<std::string> locks;
    };

    application::BaseApplication& global_context_;
    logic::LogicRegistrar& related_logic_registrar_;


    components::ComponentRegistrar& related_component_registrar() { return related_logic_registrar_.component_registrar(); }
    entities::EntityRegistrar& related_entity_registrar() { return related_logic_registrar_.entity_registrar(); }
    
    std::set<std::string> related_component_labels;
    
    std::string system_name_id_;

    FrameData current_frame_;
    FrameData next_frame_;


    /**
     * @brief Allows for inheritance of Advance Frame
     * @return true Successful
     * @return false An Error Occurred
     */
    virtual bool AdvanceFrameImpl() { return true; };

    /**
     * @brief Allows for inheritance of End Of Frame
     * @return true Successful
     * @return false An Error Occurred
     */
    virtual bool EndOfFrameImpl() { return true; };

    /**
     * @brief Handles an event for the system. Called by HandleEvent, so you can implement your own event logic
     * 
     * @param event 
     * @return true 
     * @return false 
     */
    virtual bool HandleEventImpl(logic::Event& event) { return true; }

    /**
     * @brief Allows for inheritance of Load event
     * @return true Successful
     * @return false An Error Occurred
     */
    virtual bool LoadImpl() { return true; }

    /**
     * @brief Allows for inheritance of Unload event
     * @return true Successful
     * @return false An Error Occurred
     */
    virtual bool UnloadImpl() { return true; }

    /**
     * @brief Handles the flags given
     * 
     * @param flag_data Flags you want to be handled
     * @return true No errors occurred
     * @return false An error occurred
     */
    bool HandleFlags(const application::FlagData& flag_data);

    /**
     * @brief Undoes the handling of the flags given
     * 
     * @param flag_data Flags you want to be undone
     * @return true No errors occurred
     * @return false An error occurred
     */
    bool UndoHandleFlags(const application::FlagData& flag_data); 

    public:

    AbstractSystem() = delete;

    AbstractSystem(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar) 
        : global_context_(global_context), 
          related_logic_registrar_(related_logic_registrar) 
    {}

    bool loaded() { return current_frame_.has_loaded; }
    
    bool will_be_loaded() { return next_frame_.has_loaded; }

    bool valid() { return !(current_frame_.system_id == -1); }
    bool will_be_valid() { return !(next_frame_.system_id == -1); }

    bool has_lock(std::string lock_name) { return current_frame_.locks.contains(lock_name); }

    bool DataHasChanged() {
        for (auto component_label_id : related_component_labels) {
            if (related_component_registrar().DataHasChanged(component_label_id)) return true;
        }
        return false;
    }
    
    bool DataWillChange() {
        for (auto component_label_id : related_component_labels) {
            if (related_component_registrar().DataHasChanged(component_label_id)) return true;
        }
        return false;
    }
    
    std::size_t get_id() { return current_frame_.system_id; }

    /**
     * @brief Advances the frame of the system
     * @return true Successful
     * @return false An Error Occurred
     */
    bool AdvanceFrame();

    /**
     * @brief Ends the frame of the system
     * @return true Successful
     * @return false An Error Occurred
     */
    bool EndOfFrame();

    /**
     * @brief Registers an ID to the system 
     * 
     * @param id The ID you want to register
     * @return true The ID is now registered for next frame
     * @return false The ID was not registered due to an error
     */
    bool RegisterID(std::size_t id);

    /**
     * @brief Loads the system
     * @return true The system will be loaded next frame
     * @return false The system will not be loaded next frame 
     */
    bool Load(const application::FlagData& flag_data);

    /**
     * @brief Undoes the load of the system
     * @return true The system will be unloaded next frame
     * @return false The system will be loaded next frame
     */
    bool UndoLoad(const application::FlagData& flag_data);

    /**
     * @brief Unloads the system
     * @return true 
     * @return false 
     */
    bool Unload();

    /**
     * @brief Undoes the unload of the system
     * @return true The system will be loaded next frame
     * @return false The system will be unloaded next frame
     */
    bool UndoUnload();
    
    /**
     * @brief Adds a lock to the system
     * 
     * @param lock_name The name of said lock
     * @return true The lock was added successfully 
     * @return false The lock couldnt be added
     */
    bool AddLock(std::string lock_name);

    /**
     * @brief Undoes the adding of a lock to the system
     * 
     * @param lock_name The name of said lock
     * @return true The locks addition was undone
     * @return false No locks addition was undone
     */
    bool UndoAddLock(std::string lock_name);

    /**
     * @brief Unlocks a lock on the system
     * 
     * @param lock_name The name of said lock
     * @return true The lock was unlocked
     * @return false No lock was unlocked
     */
    bool Unlock(std::string lock_name);

    /**
     * @brief Undoes the unlock of a lock on the system
     * 
     * @param lock_name The name of said lock
     * @return true The unlock was undone
     * @return false No undoing of an unlock was done
     */
    bool UndoUnlock(std::string lock_name);
    
    /**
     * @brief Handles an event for the system
     * 
     * @param event The event 
     * @return true 
     * @return false 
     */
    bool HandleEvent(logic::Event& event);

};


}
#endif