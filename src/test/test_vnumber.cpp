#include "gtest/gtest.h"

#include "application/versionnumber.h"

TEST(CreateVersionNumber, BadVersionNumbers) {
    std::vector<std::string> bad_version_numbers = {"", "a.b.cd", "a.1.1a", "1.b.1a", "1.1.ca", "1a", "1.1a"};
    std::vector<application::VersionNumber> test_numbers(bad_version_numbers.size());
    for (std::size_t i = 0; i < bad_version_numbers.size(); i++) {
        test_numbers[i] = application::CreateVersionNumber(bad_version_numbers[i]);
        EXPECT_FALSE(test_numbers[i].is_valid());
    }
}

TEST(CreateVersionNumber, VersionNumberWork) {
    std::vector<std::string> good_version_numbers = {"0.0.0a", "1.1.1b", "22.22.22r"};
    std::vector<application::VersionNumber> test_numbers(good_version_numbers.size());
    for (std::size_t i = 0; i < good_version_numbers.size(); i++) {
        test_numbers[i] = application::CreateVersionNumber(good_version_numbers[i]);
        ASSERT_TRUE(test_numbers[i].is_valid());
    }
    // a character test
    application::VersionNumber& test_number = test_numbers[0];
    EXPECT_EQ(test_number.major_number(), 0);
    EXPECT_EQ(test_number.minor_number(), 0);
    EXPECT_EQ(test_number.patch_number(), 0);
    EXPECT_EQ(test_number.version_type(), 'a');
    // b character test
    test_number = test_numbers[1];
    EXPECT_EQ(test_number.major_number(), 1);
    EXPECT_EQ(test_number.minor_number(), 1);
    EXPECT_EQ(test_number.patch_number(), 1);
    EXPECT_EQ(test_number.version_type(), 'b');
    // r character test
    test_number = test_numbers[2];
    EXPECT_EQ(test_number.major_number(), 22);
    EXPECT_EQ(test_number.minor_number(), 22);
    EXPECT_EQ(test_number.patch_number(), 22);
    EXPECT_EQ(test_number.version_type(), 'r');
}