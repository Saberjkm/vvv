#include "gtest/gtest.h"

#include "scenes/sceneregistrar.h"
#include "scenes/scenes.h"
#include "scenes/sceneregister.h"

#include "test/mock_console.h"
#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"
#include "test/mock_entityregistrar.h"
#include "test/mock_componentregistrar.h"
#include "test/mock_systemregistrar.h"
#include "test/mock_sceneregister.h"

using ::testing::_;
using ::testing::Return;

class SceneRegistrarTest :  public ::testing::Test {
    protected:
    const std::string kRegisterName = "test";
    const std::string kSceneName = "test_scene";

    console::MockSystemConsole mock_console_;
    application::MockApplication mock_global_context_;
    logic::MockLogicRegistrar mock_logic_registrar_;
    entities::MockEntityRegistrar mock_entity_registrar_;
    components::MockComponentRegistrar mock_component_registrar_;
    systems::MockSystemRegistrar mock_system_registrar_;
    std::shared_ptr<scenes::MockSceneRegister> mock_scene_register_;
    std::shared_ptr<scenes::MockSceneRegister> bad_mock_scene_register_;
    
    application::SceneRegistrarSettings scene_registrar_settings_;
    scenes::SceneRegistrar srt1_;

    SceneRegistrarTest()
        : mock_global_context_(mock_console_),
          mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, mock_component_registrar_, mock_system_registrar_),
          mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_system_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_scene_register_(std::make_shared<scenes::MockSceneRegister>(srt1_, kRegisterName)),
          srt1_(mock_global_context_, mock_logic_registrar_)
    {
      scene_registrar_settings_.scene_registers.emplace(kRegisterName, mock_scene_register_);
    }
};

TEST_F(SceneRegistrarTest, Initialise_Works) {
    EXPECT_CALL(mock_entity_registrar_, AddCreationCallback)
      .Times(1);
    EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
      .Times(1);
    srt1_.Initialise(scene_registrar_settings_);
}

TEST_F(SceneRegistrarTest, AddSceneRegister_SceneAlreadyExists) {
    EXPECT_CALL(mock_console_, IssueWarning)
      .Times(1);
    srt1_.Initialise(scene_registrar_settings_);
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();

    EXPECT_FALSE(srt1_.AddSceneRegister(kRegisterName, mock_scene_register_));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.ContainsRegister(kRegisterName)); // Check it doesnt modify data on failure
}

TEST_F(SceneRegistrarTest, AddSceneRegister_BadPointer) {
    EXPECT_CALL(mock_console_, IssueWarning)
      .Times(1);

    EXPECT_FALSE(srt1_.AddSceneRegister(kRegisterName, nullptr));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    EXPECT_FALSE(srt1_.ContainsRegister(kRegisterName)); // Check it doesnt modify data on failure
}

TEST_F(SceneRegistrarTest, AddSceneRegister_GoodRegister) {
    ASSERT_TRUE(srt1_.AddSceneRegister(kRegisterName, mock_scene_register_));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.ContainsRegister(kRegisterName));
}

TEST_F(SceneRegistrarTest, AddSceneInfo_BadSceneID) { 
    scenes::SceneInfo test_info = {};

    EXPECT_CALL(mock_console_, IssueWarning)
      .Times(1);

    EXPECT_FALSE(srt1_.AddSceneInfo(test_info));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    EXPECT_FALSE(srt1_.HasSceneInfo(""));
}

TEST_F(SceneRegistrarTest, AddSceneInfo_DuplicateNoOverride) {
    scenes::SceneInfo test_info = {kRegisterName, kSceneName, false};

    EXPECT_CALL(mock_console_, IssueWarning)
    	.Times(1);

    srt1_.AddSceneInfo(test_info);
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.HasSceneInfo(test_info.scene_label));

    EXPECT_FALSE(srt1_.AddSceneInfo(test_info));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.HasSceneInfo(test_info.scene_label));
    EXPECT_FALSE(srt1_.GetSceneInfo(kRegisterName).override);
}

TEST_F(SceneRegistrarTest, AddSceneInfo_GoodSceneInfo) {
    scenes::SceneInfo test_info = {kRegisterName, kSceneName, false};

    ASSERT_TRUE(srt1_.AddSceneInfo(test_info));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.HasSceneInfo(test_info.scene_label));
}

TEST_F(SceneRegistrarTest, AddSceneInfo_DuplicateOverride) {
    scenes::SceneInfo test_info = {kRegisterName, kSceneName, false};

    ASSERT_TRUE(srt1_.AddSceneInfo(test_info));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.HasSceneInfo(test_info.scene_label));
    
    test_info.override = true;
    ASSERT_TRUE(srt1_.AddSceneInfo(test_info));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.HasSceneInfo(test_info.scene_label));
    ASSERT_TRUE(srt1_.GetSceneInfo(kSceneName).override);
}

TEST_F(SceneRegistrarTest, LoadScene_BadID) {
    EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
    	.WillOnce(Return(std::vector<scenes::SceneInfo>({{kRegisterName, kSceneName, false}})));
    srt1_.Initialise(scene_registrar_settings_);

    EXPECT_CALL(mock_console_, IssueWarning)
    	.Times(1);

    EXPECT_FALSE(srt1_.LoadScene("bad_test_scene"));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    EXPECT_FALSE(srt1_.SceneLoaded("bad_test_scene"));
}

TEST_F(SceneRegistrarTest, LoadScene_BadSceneLoad) {
    EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
    	.WillOnce(Return(std::vector<scenes::SceneInfo>({{kRegisterName, kSceneName, false}})));
    srt1_.Initialise(scene_registrar_settings_);

    EXPECT_CALL(mock_console_, IssueWarning)
    	.Times(2);

    EXPECT_FALSE(srt1_.LoadScene(kSceneName));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    EXPECT_FALSE(srt1_.SceneLoaded(kSceneName));
}

TEST_F(SceneRegistrarTest, LoadScene_GoodSceneLoad) {
    EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
    	.WillOnce(Return(std::vector<scenes::SceneInfo>({{kRegisterName, kSceneName, false}})));
    mock_scene_register_->AddSceneLoadData({kSceneName});
    srt1_.Initialise(scene_registrar_settings_);

    ASSERT_TRUE(srt1_.LoadScene(kSceneName));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
    ASSERT_TRUE(srt1_.SceneLoaded(kSceneName));
}

TEST_F(SceneRegistrarTest, UnloadScene_BadID) {
	EXPECT_CALL(mock_console_, IssueWarning)
    	.Times(1);
	
	EXPECT_FALSE(srt1_.UnloadScene("bad_scene"));
	srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
	EXPECT_FALSE(srt1_.SceneLoaded("bad_scene")); 
}

TEST_F(SceneRegistrarTest, UnloadScene_NotLoaded) {
	EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
    	.WillOnce(Return(std::vector<scenes::SceneInfo>({{kRegisterName, kSceneName, false}})));
    srt1_.Initialise(scene_registrar_settings_);

	EXPECT_CALL(mock_console_, IssueWarning)
    	.Times(1);

	EXPECT_FALSE(srt1_.UnloadScene(kSceneName));
	srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
	EXPECT_FALSE(srt1_.SceneLoaded(kSceneName));
	ASSERT_TRUE(srt1_.HasSceneInfo(kSceneName)); 
}

TEST_F(SceneRegistrarTest, UnloadScene_BadUnload) {
	EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
    	.WillOnce(Return(std::vector<scenes::SceneInfo>({{kRegisterName, kSceneName, false}})));
    mock_scene_register_->AddSceneLoadData({kSceneName});
    srt1_.Initialise(scene_registrar_settings_);

	EXPECT_CALL(mock_console_, IssueWarning)
    	.Times(1);

    ASSERT_TRUE(srt1_.LoadScene(kSceneName));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();

	EXPECT_FALSE(srt1_.UnloadScene("bad_scene"));
	srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
	ASSERT_TRUE(srt1_.SceneLoaded(kSceneName));
}

TEST_F(SceneRegistrarTest, UnloadScene_GoodData) {
	EXPECT_CALL(*mock_scene_register_, GetSceneInfo)
    	.WillOnce(Return(std::vector<scenes::SceneInfo>({{kRegisterName, kSceneName, false}})));
    mock_scene_register_->AddSceneLoadData({kSceneName});
    srt1_.Initialise(scene_registrar_settings_);

    ASSERT_TRUE(srt1_.LoadScene(kSceneName));
    srt1_.EndOfFrame();
    srt1_.AdvanceFrame();

	ASSERT_TRUE(srt1_.UnloadScene(kSceneName));
	srt1_.EndOfFrame();
    srt1_.AdvanceFrame();
	EXPECT_FALSE(srt1_.SceneLoaded(kSceneName));
}