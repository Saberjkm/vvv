#include <iostream>
#include <string>

#include "gtest/gtest.h"

#include "components/componentregistrar.h" 
#include "test/mock_console.h"
#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"
#include "test/mock_entityregistrar.h"
#include "test/mock_componentregister.h"
#include "test/test.h"

using ::testing::_;
using namespace test;

class ComponentRegistrarTest : public ::testing::Test {
    protected:
        console::MockSystemConsole mock_console_;
        application::MockApplication mock_global_context_;
        logic::MockLogicRegistrar mock_logic_registrar_;
        entities::MockEntityRegistrar mock_entity_registrar_;
        systems::MockSystemRegistrar mock_system_registrar_;
        std::shared_ptr<components::MockComponentRegister> mock_component_register_;
        components::ComponentRegistrar crt1_;
        application::ComponentTypeRepresentation component_type_representation_1;
        application::ComponentTypeRepresentation component_type_representation_2;

        application::ComponentRegistrarSettings crt1_settings;
        
        ComponentRegistrarTest() 
            : mock_global_context_(mock_console_),
              mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, crt1_, mock_system_registrar_),
              mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
              mock_system_registrar_(mock_global_context_, mock_logic_registrar_),
              mock_component_register_(std::make_shared<components::MockComponentRegister>(crt1_, "test_register")),
              crt1_(mock_global_context_, mock_logic_registrar_)
        {
            crt1_settings.component_registers.emplace("test_register", mock_component_register_);
            crt1_.Initialise(crt1_settings);

            component_type_representation_1.component_label = "test1";
            component_type_representation_2.component_label = "test3";
        }

};

TEST_F(ComponentRegistrarTest, GetList_GoodLabel) {
    // Get correct list
    components::ComponentList* testList = crt1_.GetList("test1");
    ASSERT_TRUE(testList != nullptr);
}

TEST_F(ComponentRegistrarTest, GetList_BadLabel) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);

    components::ComponentList* badList = crt1_.GetList("badlist");
    ASSERT_TRUE(badList == nullptr);
}

TEST_F(ComponentRegistrarTest, GetComponent_GoodLabel_GoodID) {

    crt1_.LoadComponentType(component_type_representation_1);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_EQ(crt1_.GetComponent<int>("test1", test_int_1), 1);
    EXPECT_EQ(crt1_.GetComponent<int>("test1", test_int_2), 2);
}

TEST_F(ComponentRegistrarTest, GetComponent_BadLabel_GoodID) {
    crt1_.LoadComponentType(component_type_representation_1);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(4);
    EXPECT_EQ(crt1_.GetComponent<int>("", test_int_1), int());
    EXPECT_EQ(crt1_.GetComponent<int>("bad_list", test_int_1), int());
    EXPECT_EQ(crt1_.GetComponent<int>("", test_int_2), int());
    EXPECT_EQ(crt1_.GetComponent<int>("bad_list", test_int_2), int());
}

TEST_F(ComponentRegistrarTest, GetComponent_GoodLabel_BadID) {
    crt1_.LoadComponentType(component_type_representation_1);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_EQ(crt1_.GetComponent<int>("test1", (std::size_t) -1), int());
    EXPECT_EQ(crt1_.GetComponent<int>("test1", (std::size_t) 10), int());
}

TEST_F(ComponentRegistrarTest, GetComponent_BadLabel_BadID) {
    crt1_.LoadComponentType(component_type_representation_1);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(4);
    EXPECT_EQ(crt1_.GetComponent<int>("", (std::size_t) -1), int());
    EXPECT_EQ(crt1_.GetComponent<int>("", (std::size_t) 10), int());
    EXPECT_EQ(crt1_.GetComponent<int>("bad_list", (std::size_t) -1), int());
    EXPECT_EQ(crt1_.GetComponent<int>("bad_list", (std::size_t) 10), int());

}

TEST_F(ComponentRegistrarTest, DeleteComponent_GoodLabel_GoodID) {
    crt1_.LoadComponentType(component_type_representation_1);

    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    ASSERT_TRUE(crt1_.DeleteComponent("test1", test_int_1));
    ASSERT_TRUE(crt1_.DeleteComponent("test1", test_int_2));
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.GetList("test1")->count(), (std::size_t) 0);
}

TEST_F(ComponentRegistrarTest, DeleteComponent_BadLabel_GoodID) {
    crt1_.LoadComponentType(component_type_representation_1);

    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_FALSE(crt1_.DeleteComponent("", test_int_1));
    EXPECT_FALSE(crt1_.DeleteComponent("", test_int_2));
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.GetList("test1")->count(), (std::size_t) 2);
}

TEST_F(ComponentRegistrarTest, DeleteComponent_GoodLabel_BadID) {
    crt1_.LoadComponentType(component_type_representation_1);

    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_FALSE(crt1_.DeleteComponent("test1", -1));
    EXPECT_FALSE(crt1_.DeleteComponent("test1", -1));
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.GetList("test1")->count(), (std::size_t) 2);
}

TEST_F(ComponentRegistrarTest, DeleteComponent_BadLabel_BadID) {
    crt1_.LoadComponentType(component_type_representation_1);

    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test1", 2);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    // Tests
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_FALSE(crt1_.DeleteComponent("", (std::size_t) -1));
    EXPECT_FALSE(crt1_.DeleteComponent("badlist", (std::size_t) -1));
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.GetList("test1")->count(), (std::size_t) 2);
}

TEST_F(ComponentRegistrarTest, CreateNewComponent_GoodLabel_NoData) {
    crt1_.LoadComponentType(component_type_representation_1);
    crt1_.LoadComponentType(component_type_representation_2);

    std::size_t test_int_1 = crt1_.CreateNewComponent("test1");
    std::size_t test_int_2 = crt1_.CreateNewComponent("test3");
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    auto test1_pointer = crt1_.GetList("test1");
    auto test3_pointer = crt1_.GetList("test3");

    ASSERT_TRUE(test1_pointer->HasObject(test_int_1));
    ASSERT_TRUE(test3_pointer->HasObject(test_int_2));

}

TEST_F(ComponentRegistrarTest, CreateNewComponent_GoodLabel_GoodData) {
    crt1_.LoadComponentType(component_type_representation_1);
    crt1_.LoadComponentType(component_type_representation_2);

    std::size_t test_int_1 = crt1_.CreateNewComponent("test1", (int) 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("test3", (std::string) "test");
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();

    auto test1_pointer = crt1_.GetList("test1");
    auto test3_pointer = crt1_.GetList("test3");

    ASSERT_TRUE(test1_pointer->HasObject(test_int_1));
    ASSERT_TRUE(test3_pointer->HasObject(test_int_2));
    EXPECT_EQ(test1_pointer->get<int>(test_int_1), (int) 1);
    EXPECT_EQ(test3_pointer->get<std::string>(test_int_2), (std::string) "test");
}

TEST_F(ComponentRegistrarTest, CreateNewComponent_BadLabel_NoData) {
    crt1_.LoadComponentType(component_type_representation_1);
    crt1_.LoadComponentType(component_type_representation_2);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1_bad");
    std::size_t test_int_2 = crt1_.CreateNewComponent("");

    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);

}

TEST_F(ComponentRegistrarTest, CreateNewComponent_BadLabel_Data) {
    crt1_.LoadComponentType(component_type_representation_1);
    crt1_.LoadComponentType(component_type_representation_2);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1_bad", (int) 1);
    std::size_t test_int_2 = crt1_.CreateNewComponent("", (std::string) "test");

    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
}

TEST_F(ComponentRegistrarTest, CreateNewComponent_GoodLabel_BadData) {
    crt1_.LoadComponentType(component_type_representation_1);
    crt1_.LoadComponentType(component_type_representation_2);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    std::size_t test_int_1 = crt1_.CreateNewComponent("test1",(std::string) "test");
    std::size_t test_int_2 = crt1_.CreateNewComponent("test3", (int) 1);

    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 0);
}

TEST_F(ComponentRegistrarTest, UndoDeleteComponent_GoodLabel_GoodID) {
    crt1_.LoadComponentType(component_type_representation_1);

    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 3);

    ASSERT_TRUE(crt1_.DeleteComponent("test1", 1));
    ASSERT_TRUE(crt1_.UndoDeleteComponent("test1", 1).second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 3);
    ASSERT_TRUE(crt1_.GetList("test1")->HasObject(1));
}

TEST_F(ComponentRegistrarTest, UndoDeleteComponent_GoodLabel_BadID) {
    crt1_.LoadComponentType(component_type_representation_1);
    
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 3);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    ASSERT_TRUE(crt1_.DeleteComponent("test1", 1));
    auto result = crt1_.UndoDeleteComponent("test1", -1);
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 2);
}

TEST_F(ComponentRegistrarTest, UndoDeleteComponent_BadLabel) {
    crt1_.LoadComponentType(component_type_representation_1);

    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 3);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    ASSERT_TRUE(crt1_.DeleteComponent("test1", 1));
    auto result = crt1_.UndoDeleteComponent("bad_list", 1);
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 2);

}

TEST_F(ComponentRegistrarTest, UndoDeleteComponent_BadOperation) {
    crt1_.LoadComponentType(component_type_representation_1);

    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = crt1_.UndoDeleteComponent("test1", 1);
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 3);
}

TEST_F(ComponentRegistrarTest, UndoCreateNewComponent_GoodLabel_GoodID) {
    crt1_.LoadComponentType(component_type_representation_1);

    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    
    ASSERT_TRUE(crt1_.UndoCreateNewComponent("test1", 1).second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 2);
}

TEST_F(ComponentRegistrarTest, UndoCreateNewComponent_GoodLabel_BadID) {
    crt1_.LoadComponentType(component_type_representation_1);

    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    auto result = crt1_.UndoCreateNewComponent("test1", -1);
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
    result = crt1_.UndoCreateNewComponent("test1", 100);
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 3);
}

TEST_F(ComponentRegistrarTest, UndoCreateNewComponent_BadOperation) {
    crt1_.LoadComponentType(component_type_representation_1);

    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.CreateNewComponent("test1");
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    ASSERT_TRUE(crt1_.DeleteComponent("test1", 1));

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = crt1_.UndoCreateNewComponent("test1", 1);
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
    crt1_.EndOfFrame();
    crt1_.AdvanceFrame();
    EXPECT_EQ(crt1_.CountAllComponents(), (std::size_t) 2);
}