#include "gtest/gtest.h"

#include "scenes/sceneregistrar.h"
#include "scenes/scenes.h"
#include "scenes/sceneregister.h"

#include "test/mock_console.h"
#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"
#include "test/mock_entityregistrar.h"
#include "test/mock_componentregistrar.h"
#include "test/mock_systemregistrar.h"
#include "test/mock_sceneregistrar.h"
#include "test/mock_sceneregister.h"

using ::testing::_;
using ::testing::Return;

class SceneRegisterTest :  public ::testing::Test {
    protected:
    const std::string kDefaultRegisterName = "test";
    const std::string kDefaultSceneNames[3] = {"test_scene_0", "test_scene_1", "test_scene_2"};
    const std::string kDefaultBadSceneNames[3] = {"test_scene_bad_0", "test_scene_bad_1", "test_scene_bad_2"};
    const scenes::SceneInfo kDefaultSceneInfos[3] = {{kDefaultRegisterName, kDefaultSceneNames[0], false},
                                                     {kDefaultRegisterName, kDefaultSceneNames[1], false},
                                                     {kDefaultRegisterName, kDefaultSceneNames[2], false}};
    const std::string kDefaultComponentTypeNames[3] = {"test_type_0", "test_type_1", "test_type_2"};
    const std::string kDefaultSystemNames[3] = {"test_system_0", "test_system_1", "test_system_2"};

    console::MockSystemConsole mock_console_;
    application::MockApplication mock_global_context_;
    logic::MockLogicRegistrar mock_logic_registrar_;
    entities::MockEntityRegistrar mock_entity_registrar_;
    components::MockComponentRegistrar mock_component_registrar_;
    systems::MockSystemRegistrar mock_system_registrar_;
    scenes::MockSceneRegistrar mock_scene_registrar_;

    std::shared_ptr<scenes::MockSceneRegister> srt1_;

    application::SceneRegistrarSettings scene_registrar_settings_;
    

    SceneRegisterTest()
        : mock_global_context_(mock_console_),
          mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, mock_component_registrar_, mock_system_registrar_),
          mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_system_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_scene_registrar_(mock_global_context_, mock_logic_registrar_),
          srt1_(std::make_shared<scenes::MockSceneRegister>(mock_scene_registrar_, kDefaultRegisterName))
    {
        scene_registrar_settings_.scene_registers.emplace(kDefaultRegisterName, srt1_);
    }

    virtual void SetUp() {
        mock_scene_registrar_.Initialise(scene_registrar_settings_);
        mock_scene_registrar_.EndOfFrame();
        mock_scene_registrar_.AdvanceFrame();
    }
};

// Test Common methods across different implementations

TEST_F(SceneRegisterTest, LoadScene_EmptySceneLoadData) {
    scenes::SceneLoadData empty_test;
    empty_test.scene_label = kDefaultSceneNames[0];
    srt1_->AddSceneLoadData(empty_test);

    EXPECT_CALL(mock_console_, IssueWarning)
      .Times(0);
    auto test_result = srt1_->LoadScene(empty_test.scene_label); 
    ASSERT_TRUE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_systems.size(), (std::size_t) 0);
}

TEST_F(SceneRegisterTest, LoadComponentTypes_NoComponentTypes) {
    scenes::SceneLoadData component_types_test;
    component_types_test.scene_label = kDefaultSceneNames[0];
    srt1_->AddSceneLoadData(component_types_test);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(0);

    auto test_result = srt1_->LoadScene(component_types_test.scene_label); 
    EXPECT_TRUE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_systems.size(), (std::size_t) 0);
}

TEST_F(SceneRegisterTest, LoadComponentTypes_BadEntityComponentType) {
    scenes::SceneLoadData component_types_test;
    component_types_test.scene_label = kDefaultSceneNames[0];

    application::EntityRepresentation entity_test;
    entity_test.entity_label = "test_entity";
    component_types_test.entities.emplace_back(entity_test);

    srt1_->AddSceneLoadData(component_types_test);
    
    application::ComponentTypeRepresentation bad_rep;
    bad_rep.component_label = "error";
    EXPECT_CALL(mock_entity_registrar_, GetComponentTypesFromRegister)
        .WillOnce(Return(std::vector<application::ComponentTypeRepresentation>({bad_rep})));

    application::ComponentTypeData bad_data;
    EXPECT_CALL(mock_component_registrar_, LoadComponentType)
        .WillOnce(Return(bad_data));

    auto test_result = srt1_->LoadScene(component_types_test.scene_label); 
    EXPECT_FALSE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_systems.size(), (std::size_t) 0);
}

TEST_F(SceneRegisterTest, LoadComponentTypes_BadExplicitComponentType) {
    scenes::SceneLoadData component_types_test;
    component_types_test.scene_label = kDefaultSceneNames[0];

    application::ComponentTypeRepresentation test_explicit_type;
    test_explicit_type.component_label = "error";
    component_types_test.component_types.emplace_back(test_explicit_type);

    srt1_->AddSceneLoadData(component_types_test);

    application::ComponentTypeData bad_data;
    EXPECT_CALL(mock_component_registrar_, LoadComponentType(test_explicit_type))
        .WillOnce(Return(bad_data));

    auto test_result = srt1_->LoadScene(component_types_test.scene_label); 
    EXPECT_FALSE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_systems.size(), (std::size_t) 0);
}

TEST_F(SceneRegisterTest, LoadComponentTypes_DuplicateComponentTypes_NoMask) {
}

TEST_F(SceneRegisterTest, LoadSystems_BadSystemsData_NoMask) {
    scenes::SceneLoadData bad_system_data_test;
    bad_system_data_test.scene_label = kDefaultSceneNames[0];
    bad_system_data_test.systems = {{kDefaultSystemNames[0], 0}, {kDefaultSystemNames[1], 0}};
    srt1_->AddSceneLoadData(bad_system_data_test);

    application::SystemData bad_data;
    EXPECT_CALL(mock_system_registrar_, LoadSystem)
        .WillOnce(Return(bad_data));

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(3);

    auto test_result = srt1_->LoadScene(bad_system_data_test.scene_label); 
    EXPECT_FALSE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_systems.size(), (std::size_t) 0);
}

TEST_F(SceneRegisterTest, LoadSystems_BadSystemsData_Lock) {
    scenes::SceneLoadData bad_system_data_test;
    bad_system_data_test.scene_label = kDefaultSceneNames[0];
    bad_system_data_test.systems = {{kDefaultSystemNames[0], application::kLock}, {kDefaultSystemNames[1], application::kLock}};
    srt1_->AddSceneLoadData(bad_system_data_test);

    application::SystemData bad_data;
    EXPECT_CALL(mock_system_registrar_, LoadSystem)
        .WillOnce(Return(bad_data));

    EXPECT_CALL(mock_system_registrar_, LockSystem)
        .Times(0);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(3);

    auto test_result = srt1_->LoadScene(bad_system_data_test.scene_label); 
    EXPECT_FALSE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_systems.size(), (std::size_t) 0);
}

TEST_F(SceneRegisterTest, LoadSystems_GoodSystemsData_NoMask) {
    scenes::SceneLoadData good_system_data_test;
    good_system_data_test.scene_label = kDefaultSceneNames[0];
    good_system_data_test.systems =  {{kDefaultSystemNames[0], 0}, {kDefaultSystemNames[1], 0}};
    srt1_->AddSceneLoadData(good_system_data_test);
    
    application::SystemData good_data_1;
    good_data_1.is_valid = true;
    good_data_1.label = kDefaultSystemNames[0];
    application::SystemData good_data_2;
    good_data_2.is_valid = true;
    good_data_2.label = kDefaultSystemNames[1];
    EXPECT_CALL(mock_system_registrar_, LoadSystem)
        .Times(2)
        .WillOnce(Return(good_data_1))
        .WillOnce(Return(good_data_2));

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(0);

    auto test_result = srt1_->LoadScene(good_system_data_test.scene_label); 
    EXPECT_TRUE(test_result.is_valid);
    EXPECT_EQ(test_result.scene_entities.size(), (std::size_t) 0);
    EXPECT_EQ(test_result.scene_component_types.size(), (std::size_t) 0);
    ASSERT_EQ(test_result.scene_systems.size(), (std::size_t) 2);
    EXPECT_EQ(test_result.scene_systems[0].label, kDefaultSystemNames[0]);
    EXPECT_EQ(test_result.scene_systems[1].label, kDefaultSystemNames[1]);
}

TEST_F(SceneRegisterTest, LoadSystems_GoodSystemsData_Lock) {
}