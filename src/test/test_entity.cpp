#include "gtest/gtest.h"

#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"

#include "entities/entity.h"

using ::testing::_;
using ::testing::Return;

class EntityTest : public ::testing::Test {
    protected:
        console::MockSystemConsole mock_console_;
        application::MockApplication mock_global_context_;
        logic::MockLogicRegistrar mock_logic_registrar_;
        entities::MockEntityRegistrar mock_entity_registrar_;
        components::MockComponentRegistrar mock_component_registrar_;
        systems::MockSystemRegistrar mock_system_registrar_;
        entities::EntitySearchTree test_tree;
        std::shared_ptr<entities::Entity> et1_;
        std::shared_ptr<entities::Entity> et2_;
        std::shared_ptr<entities::Entity> et3_;
        std::shared_ptr<entities::Entity> et4_;

        EntityTest() 
            : mock_global_context_(mock_console_),
              mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, mock_component_registrar_, mock_system_registrar_),
              mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
              mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
              mock_system_registrar_(mock_global_context_, mock_logic_registrar_),
              et1_(std::make_shared<entities::Entity>(0, mock_global_context_, mock_entity_registrar_)),
              et2_(std::make_shared<entities::Entity>(1, mock_global_context_, mock_entity_registrar_)),
              et3_(std::make_shared<entities::Entity>(2, mock_global_context_, mock_entity_registrar_)),
              et4_(std::make_shared<entities::Entity>(3, mock_global_context_, mock_entity_registrar_))
        {}

        void SetUp() override {
            EXPECT_CALL(mock_entity_registrar_, InsertKey)
                .WillRepeatedly(Return(&test_tree));
            EXPECT_CALL(mock_entity_registrar_, GetWeakPointer(0))
                .WillRepeatedly(Return(std::weak_ptr<entities::Entity>(et1_)));
            EXPECT_CALL(mock_entity_registrar_, GetWeakPointer(1))
                .WillRepeatedly(Return(std::weak_ptr<entities::Entity>(et2_)));
            EXPECT_CALL(mock_entity_registrar_, GetWeakPointer(2))
                .WillRepeatedly(Return(std::weak_ptr<entities::Entity>(et3_)));
            EXPECT_CALL(mock_entity_registrar_, GetWeakPointer(3))
                .WillRepeatedly(Return(std::weak_ptr<entities::Entity>(et4_)));
            et1_->Load(application::kNoFlags);
            et2_->Load(application::kNoFlags);
            et3_->Load(application::kNoFlags);
            et4_->Load(application::kNoFlags);
        }
};

TEST_F(EntityTest, AddComponent_Singular_GoodComponent) {
    ASSERT_TRUE(et1_->AddComponent("test1",1));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);

    ASSERT_TRUE(et1_->AddComponent("test2",2));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);
}

TEST_F(EntityTest, AddComponent_Singular_CopyOfComponent) {
    ASSERT_TRUE(et1_->AddComponent("test1",1));
    EXPECT_FALSE(et1_->AddComponent("test1", 1));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->AddComponent("test1", 1));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
}

TEST_F(EntityTest, AddComponent_Singular_AlreadyExistingComponentType) {
    ASSERT_TRUE(et1_->AddComponent("test1", 1));
    EXPECT_FALSE(et1_->AddComponent("test1", 1));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->AddComponent({{"test1",2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
}

TEST_F(EntityTest, AddComponent_List_GoodComponents) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);

    std::vector<std::pair<std::string, std::size_t>> components = {{"test3", 3}, {"test4", 4}};
    ASSERT_TRUE(et1_->AddComponent<decltype(components)>(components));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 4);
    EXPECT_EQ(et1_->component_id("test3"), (std::size_t) 3);
    EXPECT_EQ(et1_->component_id("test4"), (std::size_t) 4);
}

TEST_F(EntityTest, AddComponent_List_CopyOfComponentsInList) {
    EXPECT_FALSE(et1_->AddComponent({{"test1", 1}, {"test2", 2}, {"test1", 1}}));
    EXPECT_FALSE(et1_->AddComponent({{"test1", 1}, {"test2", 2}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 0);
}

TEST_F(EntityTest, AddComponent_List_CopyOfComponentsInEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    EXPECT_FALSE(et1_->AddComponent({{"test2", 2}, {"test3", 3}}));
    EXPECT_FALSE(et1_->AddComponent({{"test3", 3}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);
}

TEST_F(EntityTest, AddComponent_List_AlreadyExistingComponentTypeInList) {
    EXPECT_FALSE(et1_->AddComponent({{"test1", 1}, {"test2", 2}, {"test1", 11}}));
    EXPECT_FALSE(et1_->AddComponent({{"test1", 1}, {"test2", 2}, {"test2", 12}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 0);
}

TEST_F(EntityTest, AddComponent_List_AlreadyExistingComponentTypeInEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->AddComponent({{"test1", 11}, {"test2", 12}}));
    EXPECT_FALSE(et1_->AddComponent({{"test2", 12}, {"test3", 3}}));
    EXPECT_FALSE(et1_->AddComponent({{"test3", 3}, {"test2", 12}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);
}

TEST_F(EntityTest, AddRefComponent_GoodComponent) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    ASSERT_TRUE(et2_->AddRefComponent(std::weak_ptr<entities::Entity>(et1_), "test1"));
    et2_->EndOfFrame();
    et2_->AdvanceFrame();
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et2_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et2_->component_access("test1"), entities::kR);
}

TEST_F(EntityTest, AddRefComponent_BadReference) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et2_->AddRefComponent(std::weak_ptr<entities::Entity>(et1_), "test3"));
    et2_->EndOfFrame();
    et2_->AdvanceFrame();
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 0);
}

TEST_F(EntityTest, AddRefComponent_ReferenceDeletedNextFrame) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    ASSERT_TRUE(et1_->DeleteComponent("test1"));

    EXPECT_FALSE(et2_->AddRefComponent(std::weak_ptr<entities::Entity>(et1_), "test1"));
    et2_->EndOfFrame();
    et2_->AdvanceFrame();
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 0);
}

TEST_F(EntityTest, AddRefComponent_AlreadyExistingComponent) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    ASSERT_TRUE(et2_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et2_->EndOfFrame();
    et2_->AdvanceFrame();

    EXPECT_FALSE(et2_->AddRefComponent(std::weak_ptr<entities::Entity>(et1_), "test1"));
    et2_->EndOfFrame();
    et2_->AdvanceFrame();
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et2_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et2_->component_access("test1"), entities::kRW);
}

TEST_F(EntityTest, DeleteComponent_GoodComponent) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test1", 1))
        .Times(1);
    ASSERT_TRUE(et1_->DeleteComponent("test1"));
    ASSERT_TRUE(et1_->HasComponent("test1"));
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);
    EXPECT_FALSE(et1_->HasComponent("test1"));
}

TEST_F(EntityTest, DeleteComponent_GoodComponentReference) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->Reference(std::weak_ptr<entities::Entity>(et1_));
    et2_->EndOfFrame();
    et2_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test1", 1))
        .Times(1);
    ASSERT_TRUE(et1_->DeleteComponent("test1"));
    ASSERT_TRUE(et1_->HasComponent("test1"));
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
    et2_->EndOfFrame();
    et2_->AdvanceFrame();
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et2_->component_id("test2"), (std::size_t) 2);
    EXPECT_FALSE(et2_->HasComponent("test1"));
    ASSERT_TRUE(et1_->HasComponent("test1"));
    EXPECT_FALSE(et1_->HasChildren("test1"));
}

TEST_F(EntityTest, DeleteComponent_BadComponent) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test3", 3))
        .Times(0);
    EXPECT_FALSE(et1_->DeleteComponent("test3"));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);
}

TEST_F(EntityTest, DeleteComponent_ComponentAlreadyDeleted) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test1", 1))
        .Times(1);
    ASSERT_TRUE(et1_->DeleteComponent("test1"));
    EXPECT_FALSE(et1_->DeleteComponent("test1"));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);
    EXPECT_FALSE(et1_->HasComponent("test1"));
}

TEST_F(EntityTest, DeleteComponent_ComponentOnlyExistsInNextFrame) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test3", 3))
        .Times(0);
    EXPECT_FALSE(et1_->DeleteComponent("test1"));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);

}

TEST_F(EntityTest, Reference_GoodEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    
    et2_->Reference(std::weak_ptr<entities::Entity>(et1_));

    // Check its added to next frame
    EXPECT_EQ(et2_->CountComponents(), 0);
    EXPECT_FALSE(et1_->HasChildren("test1"));
    EXPECT_FALSE(et1_->HasChildren("test2"));

    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    EXPECT_EQ(et2_->CountComponents(), 2);
    ASSERT_TRUE(et1_->HasChildren("test1"));
    ASSERT_TRUE(et1_->HasChildren("test2"));
    EXPECT_EQ(et2_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et2_->component_id("test2"), (std::size_t) 2);
    EXPECT_EQ(et1_->component_access("test1"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et1_->component_access("test2"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et2_->component_access("test1"), entities::ComponentAccess::kR);
    EXPECT_EQ(et2_->component_access("test2"), entities::ComponentAccess::kR);
}

TEST_F(EntityTest, Reference_ReferenceEntityContainsReferences) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->Reference(std::weak_ptr<entities::Entity>(et1_));
    et2_->AddComponent("test3", 3);
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    et3_->Reference(std::weak_ptr<entities::Entity>(et2_));
    // Check its added to next frame
    EXPECT_EQ(et3_->CountComponents(), 0);
    EXPECT_FALSE(et2_->HasChildren("test1"));
    EXPECT_FALSE(et2_->HasChildren("test2"));
    EXPECT_FALSE(et2_->HasChildren("test3"));

    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et3_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();
    et3_->AdvanceFrame();

    EXPECT_EQ(et3_->CountComponents(), 3);
    ASSERT_TRUE(et2_->HasChildren("test1"));
    ASSERT_TRUE(et2_->HasChildren("test2"));
    ASSERT_TRUE(et2_->HasChildren("test3"));
    EXPECT_EQ(et3_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et3_->component_id("test2"), (std::size_t) 2);
    EXPECT_EQ(et3_->component_id("test3"), (std::size_t) 3);
    EXPECT_EQ(et1_->component_access("test1"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et1_->component_access("test2"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et2_->component_access("test1"), entities::ComponentAccess::kR);
    EXPECT_EQ(et2_->component_access("test2"), entities::ComponentAccess::kR);
    EXPECT_EQ(et2_->component_access("test3"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et3_->component_access("test1"), entities::ComponentAccess::kR);
    EXPECT_EQ(et3_->component_access("test2"), entities::ComponentAccess::kR);
    EXPECT_EQ(et3_->component_access("test3"), entities::ComponentAccess::kR);
}

TEST_F(EntityTest, Reference_EntityAlreadyContainsComponents) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    ASSERT_TRUE(et2_->AddComponent({{"test1", 11}, {"test2", 12}}));
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test1", 11))
        .Times(1);
    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test2", 12))
        .Times(1);
    et2_->Reference(std::weak_ptr<entities::Entity>(et1_));

    //Check it Modifies next frame
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et2_->component_id("test1"), (std::size_t) 11);
    EXPECT_EQ(et2_->component_id("test2"), (std::size_t) 12);

    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et2_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et2_->component_id("test2"), (std::size_t) 2);
    EXPECT_EQ(et1_->component_access("test1"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et1_->component_access("test2"), entities::ComponentAccess::kRW);
    EXPECT_EQ(et2_->component_access("test1"), entities::ComponentAccess::kR);
    EXPECT_EQ(et2_->component_access("test2"), entities::ComponentAccess::kR);
}

TEST_F(EntityTest, Reference_ReferencingItself) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent)
        .Times(0);
    et1_->Reference(std::weak_ptr<entities::Entity>(et1_));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 2);
    EXPECT_EQ(et1_->component_id("test1"), (std::size_t) 1);
    EXPECT_EQ(et1_->component_id("test2"), (std::size_t) 2);
}

TEST_F(EntityTest, Clear_EmptyEntity) {
    ASSERT_TRUE(et1_->Clear());
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->IsLoaded());
}

TEST_F(EntityTest, Clear_NoReferenceEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test1", 1))
        .Times(1);
    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test2", 2))
        .Times(1);
    ASSERT_TRUE(et1_->Clear());
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->IsLoaded());
    EXPECT_EQ(et1_->CountComponents(), (std::size_t) 0);
}

TEST_F(EntityTest, Clear_MixedEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    ASSERT_TRUE(et2_->AddComponent({{"test3", 3}, {"test4", 4}}));
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    et2_->Reference(std::weak_ptr<entities::Entity>(et1_));
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 4);

    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test3", 3))
        .Times(1);
    EXPECT_CALL(mock_component_registrar_, DeleteComponent("test4", 4))
        .Times(1);
    ASSERT_TRUE(et2_->Clear());
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    EXPECT_FALSE(et2_->IsLoaded());
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 0);
    EXPECT_FALSE(et1_->HasChildren("test1"));
    EXPECT_FALSE(et1_->HasChildren("test2"));
}

TEST_F(EntityTest, Clear_FullReferenceEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    et2_->Reference(std::weak_ptr<entities::Entity>(et1_));
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    EXPECT_CALL(mock_component_registrar_, DeleteComponent)
        .Times(0);
    ASSERT_TRUE(et2_->Clear());
    et1_->EndOfFrame();
    et2_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->AdvanceFrame();

    EXPECT_FALSE(et2_->IsLoaded());
    EXPECT_EQ(et2_->CountComponents(), (std::size_t) 0);
    EXPECT_FALSE(et1_->HasChildren("test1"));
    EXPECT_FALSE(et1_->HasChildren("test2"));
}

TEST_F(EntityTest, AddLock_NoExistingLock) {
    ASSERT_TRUE(et1_->AddLock("test"));

    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    ASSERT_TRUE(et1_->has_lock("test"));
}

TEST_F(EntityTest, AddLock_AlreadyExistingLock) {
    ASSERT_TRUE(et1_->AddLock("test"));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->AddLock("test"));

    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    ASSERT_TRUE(et1_->has_lock("test"));
}

TEST_F(EntityTest, AddLock_OtherLocksExist) {
    ASSERT_TRUE(et1_->AddLock("other_lock"));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    ASSERT_TRUE(et1_->AddLock("test"));
    ASSERT_TRUE(et1_->has_lock("other_lock"));

    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    
    ASSERT_TRUE(et1_->has_lock("other_lock"));
    ASSERT_TRUE(et1_->has_lock("test"));
}

TEST_F(EntityTest, UndoAddLock_NoLocks) {
    EXPECT_FALSE(et1_->UndoAddLock("test"));
    EXPECT_FALSE(et1_->has_lock("test"));

    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    EXPECT_FALSE(et1_->has_lock("test"));
}

TEST_F(EntityTest, GetDataRepresentation_InvalidEntity) {
    auto test_representation = et1_->GetDataRepresentation();
    EXPECT_EQ(test_representation.first, (std::size_t) -1);
    EXPECT_EQ(test_representation.second.size(), 0);
}

TEST_F(EntityTest, GetDataRepresentation_EmptyEntity) {
    et3_->EndOfFrame();
    et3_->AdvanceFrame();
    
    auto test_representation = et3_->GetDataRepresentation();
    EXPECT_EQ(test_representation.first, (std::size_t) 2);
    EXPECT_EQ(test_representation.second.size(), 0);
}

TEST_F(EntityTest, GetDataRepresentation_NormalEntity) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    auto test_representation = et1_->GetDataRepresentation();
    EXPECT_EQ(test_representation.first, (std::size_t) 0);
    EXPECT_EQ(test_representation.second.size(), 2);
    ASSERT_TRUE(test_representation.second.contains("test1"));
    ASSERT_TRUE(test_representation.second.contains("test2"));
    EXPECT_EQ(test_representation.second["test1"].first, 1);
    EXPECT_EQ(test_representation.second["test2"].first, 2);
}

TEST_F(EntityTest, GetDataRepresentation_SelectedComponents) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();
    et2_->Reference(et1_);
    et2_->EndOfFrame();
    et2_->AdvanceFrame();

    auto test_representation = et2_->GetDataRepresentation({"test1"});
    EXPECT_EQ(test_representation.first, (std::size_t) 1);
    EXPECT_EQ(test_representation.second.size(), 1);
    ASSERT_TRUE(test_representation.second.contains("test1"));
    EXPECT_EQ(test_representation.second["test1"].first, 1);
    EXPECT_EQ(test_representation.second["test1"].second, entities::ComponentAccess::kR);
}

TEST_F(EntityTest, GetDataRepresentation_NonExistingComponents) {
    ASSERT_TRUE(et1_->AddComponent({{"test1", 1}, {"test2", 2}}));
    et1_->EndOfFrame();
    et1_->AdvanceFrame();

    auto test_representation = et1_->GetDataRepresentation({"test1", "test3"});
    EXPECT_EQ(test_representation.first, (std::size_t) 0);
    EXPECT_EQ(test_representation.second.size(), 1);
    ASSERT_TRUE(test_representation.second.contains("test1"));
    EXPECT_EQ(test_representation.second["test1"].first, 1);
    EXPECT_FALSE(test_representation.second.contains("test3"));
}
