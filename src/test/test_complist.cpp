
#include <iostream>

#include "gtest/gtest.h"

#include "components/componentlist.h" 
#include "test/mock_application.h"
#include "test/test.h"

using ::testing::_;
using namespace test;

class ComponentListTest : public ::testing::Test {
    protected:
    console::MockSystemConsole mock_console_;  // Should have a value if global context declares a mock system console
    application::MockApplication mock_global_context_;
    components::ComponentList clt1_;
    components::ComponentList clt2_;

    ComponentListTest() 
        : mock_global_context_(mock_console_),
          clt1_(mock_global_context_), 
          clt2_(mock_global_context_) 
    {
        clt1_.InitialiseList<int>();
        clt1_.Load(application::kNoFlags);

        clt2_.InitialiseList<int>();
    }

};

// Tests when list has been changed by adding things
TEST_F(ComponentListTest, AdvanceFrame_ChangeAdd) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 2);
    ASSERT_TRUE(clt1_.AdvanceFrame());
    EXPECT_EQ(clt1_.count(), (std::size_t) 2);
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
    ASSERT_TRUE(clt1_.AdvanceFrame());
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
}

// Tests when list has been changed by removing things
TEST_F(ComponentListTest, AdvanceFrame_ChangeRemove) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
    ASSERT_TRUE(clt1_.AdvanceFrame());
    EXPECT_EQ(clt1_.count(), (std::size_t) 2);

}

TEST_F(ComponentListTest, AdvanceFrame_ChangeData) {
    clt1_.CreateNewComponent((int) 1);
    clt1_.CreateNewComponent((int) 2);
    clt1_.CreateNewComponent((int) 3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
    EXPECT_EQ(clt1_.get<int>(0), 1);
    EXPECT_EQ(clt1_.get<int>(1), 2);
    EXPECT_EQ(clt1_.get<int>(2), 3);
    clt1_.SetComponent(1, (int) 100);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
    EXPECT_EQ(clt1_.get<int>(0), 1);
    EXPECT_EQ(clt1_.get<int>(1), 100);
    EXPECT_EQ(clt1_.get<int>(2), 3);
}

// Tests when list has not been changed
TEST_F(ComponentListTest, AdvanceFrame_NoChange) {
    clt1_.EndOfFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 0);
    ASSERT_TRUE(clt1_.AdvanceFrame());
    EXPECT_EQ(clt1_.count(), (std::size_t) 0);
}

TEST_F(ComponentListTest, CreateNewComponent_EmptyList_EmptyComponent) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    EXPECT_TRUE(clt1_.WillHaveObject(0));
    EXPECT_TRUE(clt1_.WillHaveObject(1));
    EXPECT_TRUE(clt1_.WillHaveObject(2));
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
    ASSERT_TRUE(clt1_.HasObject(0));
    ASSERT_TRUE(clt1_.HasObject(1));
    ASSERT_TRUE(clt1_.HasObject(2));
}

TEST_F(ComponentListTest, CreateNewComponent_EmptyList_DataComponent) {
    clt1_.CreateNewComponent((int) 1);
    clt1_.CreateNewComponent((int) 2);
    clt1_.CreateNewComponent((int) 3);
    EXPECT_TRUE(clt1_.WillHaveObject(0));
    EXPECT_TRUE(clt1_.WillHaveObject(1));
    EXPECT_TRUE(clt1_.WillHaveObject(2));
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
    EXPECT_EQ(clt1_.get<int>(0), 1);
    EXPECT_EQ(clt1_.get<int>(1), 2);
    EXPECT_EQ(clt1_.get<int>(2), 3);
}

TEST_F(ComponentListTest, CreateNewComponent_EmptyList_CallbackComponent) {
    // Main Callback
    std::function<void(std::size_t)> callback = [&console = mock_console_](std::size_t component_index) { console.IssueWarning("test", "test", std::to_string(component_index)); };
    
    clt1_.CreateNewComponent(callback);
    clt1_.CreateNewComponent({callback, callback});
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    
    // Check they have deletion callbacks
    ASSERT_TRUE(clt1_.HasDeletionCallback(0));
    ASSERT_TRUE(clt1_.HasDeletionCallback(1));

    // Check for proper callbacks
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(0)))
        .Times(1);
    clt1_.DeleteComponent(0);
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(1)))
        .Times(2);
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
}

TEST_F(ComponentListTest, CreateNewComponent_EmptyList_CallbackDataComponent) {
    // Main Callback
    std::function<void(std::size_t)> callback = [&console = mock_console_](std::size_t component_index) { console.IssueWarning("test", "test", std::to_string(component_index)); };
    
    clt1_.CreateNewComponent(1, callback);
    clt1_.CreateNewComponent(2, {callback, callback});
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    
    // Check they have deletion callbacks
    ASSERT_TRUE(clt1_.HasDeletionCallback(0));
    ASSERT_TRUE(clt1_.HasDeletionCallback(1));

    // Check they have data
    EXPECT_EQ(clt1_.count(), (std::size_t) 2);
    EXPECT_EQ(clt1_.get<int>(0), 1);
    EXPECT_EQ(clt1_.get<int>(1), 2);

    // Check for proper callbacks
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(0)))
        .Times(1);
    clt1_.DeleteComponent(0);
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(1)))
        .Times(2);
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
}

TEST_F(ComponentListTest, CreateNewComponent_PopulatedList_EmptyComponent) {
    // Set-up
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    // Test start
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 4);
}

TEST_F(ComponentListTest, CreateNewComponent_PopulatedList_DataComponent) {
    // Set-up
    clt1_.CreateNewComponent((int) 1);
    clt1_.CreateNewComponent((int) 2);
    clt1_.CreateNewComponent((int) 3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    // Test start
    std::size_t test_int1 = clt1_.CreateNewComponent((int) 4);
    std::size_t test_int2 = clt1_.CreateNewComponent((int) 5);
    EXPECT_NE(test_int1, (std::size_t) -1);
    EXPECT_NE(test_int1, (std::size_t) -1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 4);
    EXPECT_EQ(clt1_.get<int>(test_int1), 4);
    EXPECT_EQ(clt1_.get<int>(test_int2), 5);
}

TEST_F(ComponentListTest, CreateNewComponent_PopulatedList_CallbackComponent) {
    // Main Callback
    std::function<void(std::size_t)> callback = [&console = mock_console_](std::size_t component_index) { console.IssueWarning("test", "test", std::to_string(component_index)); };
    
    // Set-up
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    std::size_t test_int_1 = clt1_.CreateNewComponent(callback);
    std::size_t test_int_2 = clt1_.CreateNewComponent({callback, callback});
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    // Check they have deletion callbacks
    ASSERT_TRUE(clt1_.HasDeletionCallback(test_int_1));
    ASSERT_TRUE(clt1_.HasDeletionCallback(test_int_2));

    // Check for proper callbacks
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(test_int_1)))
        .Times(1);
    clt1_.DeleteComponent(test_int_1);
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(test_int_2)))
        .Times(2);
    clt1_.DeleteComponent(test_int_2);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
}

TEST_F(ComponentListTest, CreateNewComponent_PopulatedList_CallbackDataComponent) {
    // Main Callback
    std::function<void(std::size_t)> callback = [&console = mock_console_](std::size_t component_index) { console.IssueWarning("test", "test", std::to_string(component_index)); };
    
    // Set-up
    clt1_.CreateNewComponent(1);
    clt1_.CreateNewComponent(2);
    clt1_.CreateNewComponent(3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    // Test start
    std::size_t test_int_1 = clt1_.CreateNewComponent((int) 4, callback);
    std::size_t test_int_2 = clt1_.CreateNewComponent((int) 5, {callback, callback});
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.HasDeletionCallback(test_int_1));
    ASSERT_TRUE(clt1_.HasDeletionCallback(test_int_2));
    EXPECT_EQ(clt1_.count(), (std::size_t) 4);
    EXPECT_EQ(clt1_.get<int>(test_int_1), 4);
    EXPECT_EQ(clt1_.get<int>(test_int_2), 5);

    // Check for proper callbacks
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(test_int_1)))
        .Times(1);
    clt1_.DeleteComponent(test_int_1);
    EXPECT_CALL(mock_console_, IssueWarning("test", "test", std::to_string(test_int_2)))
        .Times(2);
    clt1_.DeleteComponent(test_int_2);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
}

TEST_F(ComponentListTest, DeleteComponent_EmptyList) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    EXPECT_FALSE(clt1_.DeleteComponent(0));
}

TEST_F(ComponentListTest, DeleteComponent_PopulatedList_GoodIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    std::size_t bad_int_1 = 0;
    std::size_t bad_int_2 = 2;

    ASSERT_TRUE(clt1_.DeleteComponent(bad_int_1));
    ASSERT_TRUE(clt1_.DeleteComponent(bad_int_2));

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), 1);
    EXPECT_FALSE(clt1_.HasObject(bad_int_1));
    EXPECT_FALSE(clt1_.HasObject(bad_int_2));
}

TEST_F(ComponentListTest, DeleteComponent_PopulatedList_OutOfBoundsIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_FALSE(clt1_.DeleteComponent(clt1_.count()));
    EXPECT_FALSE(clt1_.DeleteComponent((std::size_t) -1));

}

TEST_F(ComponentListTest, DeleteComponent_PopulatedList_AlreadyDeletedIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    
    std::size_t bad_int_1 = 0;
    clt1_.DeleteComponent(bad_int_1);
    
    ASSERT_TRUE(clt1_.DeleteComponent(bad_int_1));
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_FALSE(clt1_.DeleteComponent(bad_int_1));
}

TEST_F(ComponentListTest, SetComponent_EmptyList) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    EXPECT_FALSE(clt1_.SetComponent<int>(0, 1));
}

TEST_F(ComponentListTest, SetComponent_PopulatedList_GoodIndex) {
    std::size_t good_int_1 = clt1_.CreateNewComponent(1);
    clt1_.CreateNewComponent(2);
    clt1_.CreateNewComponent(3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.SetComponent<int>(good_int_1, 4));
    EXPECT_EQ(clt1_.get<int>(good_int_1), 1); // Check it acts on next frame
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.get<int>(good_int_1), 4); // Check it acts on next frame
}

TEST_F(ComponentListTest, SetComponent_PopulatedList_OutOfBoundsIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_FALSE(clt1_.SetComponent<int>((std::size_t) -1, 4));
    EXPECT_FALSE(clt1_.SetComponent<int>(clt1_.count(), 4));
}

TEST_F(ComponentListTest, SetComponent_PopulatedList_DeletedIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent((std::size_t) 0);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent((std::size_t) 1);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    EXPECT_FALSE(clt1_.SetComponent<int>(0, 0)); // Deleted current frame
    EXPECT_FALSE(clt1_.SetComponent<int>(1, 1));    // Deleted next frame
}

TEST_F(ComponentListTest, UndoCreation_EmptyList) {
    std::size_t test_int_1 = clt1_.CreateNewComponent();
    ASSERT_TRUE(clt1_.UndoCreation(test_int_1).second);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 0);
}

TEST_F(ComponentListTest, UndoCreation_PopulatedList_GoodIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    std::size_t test_int_1 = clt1_.CreateNewComponent();
    ASSERT_TRUE(clt1_.UndoCreation(test_int_1).second);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.count(), (std::size_t) 3);
}

TEST_F(ComponentListTest, UndoCreation_PopulatedList_OutOfBoundsIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    auto result = clt1_.UndoCreation((std::size_t) -1); 
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
    result = clt1_.UndoCreation(clt1_.count());
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
}

TEST_F(ComponentListTest, UndoCreation_PopulatedList_BadIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.CreateNewComponent();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoCreation(0); 
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
}

TEST_F(ComponentListTest, UndoCreation_PopulatedList_DifferentOperation) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoCreation(1); 
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
}

TEST_F(ComponentListTest, UndoDeletion_EmptyList) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoDeletion(0); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
}

TEST_F(ComponentListTest, UndoDeletion_PopulatedList_GoodIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);

    ASSERT_TRUE(clt1_.UndoDeletion(1).second);
}

TEST_F(ComponentListTest, UndoDeletion_PopulatedList_OutOfBoundsIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    auto result = clt1_.UndoDeletion((std::size_t) -1); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
    result = clt1_.UndoDeletion(clt1_.count()); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
}

TEST_F(ComponentListTest, UndoDeletion_PopulatedList_BadIndex) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoDeletion(0); 
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
}

TEST_F(ComponentListTest, UndoDeletion_PopulatedList_DifferentOperation) {
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.CreateNewComponent();
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.CreateNewComponent();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoDeletion(3); 
    EXPECT_FALSE(result.first);
    EXPECT_TRUE(result.second);
}

TEST_F(ComponentListTest, UndoSetComponent_EmptyList) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoSetComponent(0); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
}

TEST_F(ComponentListTest, UndoSetComponent_PopulatedList_GoodIndex) {
    clt1_.CreateNewComponent(1);
    clt1_.CreateNewComponent(2);
    clt1_.CreateNewComponent(3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.SetComponent(1, 4));
    ASSERT_TRUE(clt1_.UndoSetComponent(0).second);
    ASSERT_TRUE(clt1_.UndoSetComponent(1).second);

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    EXPECT_EQ(clt1_.get<int>(0), 1);
    EXPECT_EQ(clt1_.get<int>(1), 2);
}

TEST_F(ComponentListTest, UndoSetComponent_PopulatedList_OutOfBoundsIndex) {
    clt1_.CreateNewComponent(1);
    clt1_.CreateNewComponent(2);
    clt1_.CreateNewComponent(3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    auto result = clt1_.UndoSetComponent(-1); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
    result = clt1_.UndoSetComponent(clt1_.count()); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
}

TEST_F(ComponentListTest, UndoSetComponent_PopulatedList_BadIndex) {
    clt1_.CreateNewComponent(1);
    clt1_.CreateNewComponent(2);
    clt1_.CreateNewComponent(3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoSetComponent(1); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
}

TEST_F(ComponentListTest, UndoSetComponent_PopulatedList_DifferentOperation) {
    clt1_.CreateNewComponent(1);
    clt1_.CreateNewComponent(2);
    clt1_.CreateNewComponent(3);
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.DeleteComponent(1);
    
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(1);
    auto result = clt1_.UndoSetComponent(1); 
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);
}

TEST_F(ComponentListTest, Load_NotLoaded) {
    ASSERT_TRUE(clt2_.Load(application::kNoFlags));
    EXPECT_FALSE(clt2_.loaded());

    clt2_.EndOfFrame();
    clt2_.AdvanceFrame();

    ASSERT_TRUE(clt2_.loaded());
}

TEST_F(ComponentListTest, Load_LoadedNextFrame) {
    ASSERT_TRUE(clt1_.Load(application::kNoFlags));
    EXPECT_FALSE(clt1_.loaded());

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.loaded());
}

TEST_F(ComponentListTest, Load_LoadedCurrentFrame) {
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.Load(application::kNoFlags));
    ASSERT_TRUE(clt1_.loaded());
    
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.loaded());
}

TEST_F(ComponentListTest, Load_UnloadedNextFrame) {
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.Unload();

    EXPECT_FALSE(clt1_.Load(application::kNoFlags));
    ASSERT_TRUE(clt1_.loaded());
    
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.loaded());
}

TEST_F(ComponentListTest, UndoLoad_NotLoaded) {
    ASSERT_TRUE(clt2_.UndoLoad(application::kNoFlags).second);
    EXPECT_FALSE(clt2_.loaded());

    clt2_.EndOfFrame();
    clt2_.AdvanceFrame();

    EXPECT_FALSE(clt2_.loaded());
}

TEST_F(ComponentListTest, UndoLoad_LoadedNextFrame) {
    ASSERT_TRUE(clt1_.UndoLoad(application::kNoFlags).second);
    
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.loaded());
}

TEST_F(ComponentListTest, UndoLoad_LoadedCurrentFrame) {
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    auto result = clt1_.UndoLoad(application::kNoFlags);
    EXPECT_FALSE(result.first);
    EXPECT_FALSE(result.second);

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.loaded());
}

TEST_F(ComponentListTest, Unload_LockedList) {
    ASSERT_TRUE(clt1_.AddLock("test"));
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.Unload());

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    
    ASSERT_TRUE(clt1_.loaded());
}

TEST_F(ComponentListTest, Unload_NotLoadedCurrentFrame_NotLoadedNextFrame) {
    ASSERT_TRUE(clt2_.Unload());

    clt2_.EndOfFrame();
    clt2_.AdvanceFrame();

    EXPECT_FALSE(clt2_.loaded());
}

TEST_F(ComponentListTest, Unload_NotLoadedCurrentFrame_LoadedNextFrame) {
    EXPECT_FALSE(clt1_.Unload());

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.loaded());
}

TEST_F(ComponentListTest, Unload_LoadedCurrentFrame_LoadedNextFrame) {
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.Unload());
    ASSERT_TRUE(clt1_.loaded());

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.loaded());
}

TEST_F(ComponentListTest, Unload_LoadedCurrentFrame_NotLoadedNextFrame) {
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.Unload();

    ASSERT_TRUE(clt1_.Unload());
    ASSERT_TRUE(clt1_.loaded());

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.loaded());
}

TEST_F(ComponentListTest, AddLock_UnloadedList) {
    EXPECT_FALSE(clt2_.AddLock("test"));

    clt2_.EndOfFrame();
    clt2_.AdvanceFrame();

    EXPECT_FALSE(clt2_.has_lock("test"));
}

TEST_F(ComponentListTest, AddLock_LoadedList) {
    ASSERT_TRUE(clt1_.AddLock("test"));

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.has_lock("test"));
}

TEST_F(ComponentListTest, AddLock_AlreadyExistingLock) {
    ASSERT_TRUE(clt1_.AddLock("test"));
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.AddLock("test"));

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    ASSERT_TRUE(clt1_.has_lock("test"));
}

TEST_F(ComponentListTest, AddLock_UnloadedNextFrame) {
    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();
    clt1_.Unload();

    EXPECT_FALSE(clt1_.AddLock("test"));

    clt1_.EndOfFrame();
    clt1_.AdvanceFrame();

    EXPECT_FALSE(clt1_.has_lock("test"));
}
