#include "gtest/gtest.h"

#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"
#include "test/mock_system.h"
#include "test/mock_systemregister.h"
#include "systems/systemregistrar.h"

using ::testing::Return;

class SystemRegistrarTest :  public ::testing::Test {
    protected:
    console::MockSystemConsole mock_console_;  // Should have a value if global context declares a mock system console
    application::MockApplication mock_global_context_;
    logic::MockLogicRegistrar mock_logic_registrar_;
    entities::MockEntityRegistrar mock_entity_registrar_;
    components::MockComponentRegistrar mock_component_registrar_;
    std::shared_ptr<systems::MockSystemRegister> mock_system_register_;
    std::shared_ptr<systems::MockSystem> mock_system_;

    systems::SystemRegistrar srt1_; 

    SystemRegistrarTest()
        : mock_global_context_(mock_console_),
          mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, mock_component_registrar_, srt1_),
          mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_system_register_(std::make_shared<systems::MockSystemRegister>(srt1_, "test")),
          mock_system_(std::make_shared<systems::MockSystem>((application::BaseApplication&) mock_global_context_, mock_logic_registrar_)),
          srt1_(mock_global_context_, mock_logic_registrar_)
    {
        srt1_.LoadSystemRegister("test", mock_system_register_);
    }
    
    
};

TEST_F(SystemRegistrarTest, RegisterSystem_BadRegister) {
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);
    
    ASSERT_FALSE(srt1_.RegisterSystem("", ""));
    ASSERT_FALSE(srt1_.RegisterSystem("test_bad", "test_bad"));
    
    ASSERT_TRUE(srt1_.EndOfFrame());
    ASSERT_TRUE(srt1_.AdvanceFrame());
}

TEST_F(SystemRegistrarTest, RegisterSystem_GoodRegister_BadID) {
    EXPECT_CALL(*mock_system_register_, GetSystem)
        .WillRepeatedly(Return(std::weak_ptr<systems::AbstractSystem>()));
    
    EXPECT_CALL(mock_console_, IssueWarning)
        .Times(2);

    ASSERT_FALSE(srt1_.RegisterSystem("test", ""));
    ASSERT_FALSE(srt1_.RegisterSystem("test", "test_bad"));

    ASSERT_TRUE(srt1_.EndOfFrame());
    ASSERT_TRUE(srt1_.AdvanceFrame());

    ASSERT_EQ(srt1_.CountSystems(), (std::size_t) 0);
}

TEST_F(SystemRegistrarTest, RegisterSystem_GoodRegister_GoodID) {
    EXPECT_CALL(*mock_system_register_, GetSystem)
        .WillRepeatedly(Return(std::weak_ptr<systems::AbstractSystem>(mock_system_)));

    EXPECT_CALL(*mock_system_, Load)
        .WillRepeatedly(Return(true));

    ASSERT_TRUE(srt1_.RegisterSystem("test", "test_1"));
    ASSERT_TRUE(srt1_.RegisterSystem("test", "test_2"));

    ASSERT_TRUE(srt1_.EndOfFrame());
    ASSERT_TRUE(srt1_.AdvanceFrame());

    ASSERT_EQ(srt1_.CountSystems(), (std::size_t) 2);
}

TEST_F(SystemRegistrarTest, ProcessSystemWorks) {
}