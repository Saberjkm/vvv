#include <iostream>

#include "gtest/gtest.h"

#include "systems/system.h"
#include "systems/testsystem.h"
#include "test/mock_console.h"
#include "test/mock_system.h"
#include "test/mock_application.h"
#include "test/mock_logicregistrar.h"

using ::testing::_;

class SystemTest :  public ::testing::Test {
    protected:
    console::MockSystemConsole mock_console_;
    application::MockApplication mock_global_context_;
    logic::MockLogicRegistrar mock_logic_registrar_;
    entities::MockEntityRegistrar mock_entity_registrar_;
    components::MockComponentRegistrar mock_component_registrar_;
    systems::MockSystemRegistrar mock_system_registrar_;

    systems::TestSystem st1_;
    systems::TestSystem st2_;
    
    SystemTest() 
        : mock_global_context_(mock_console_),
          mock_logic_registrar_(mock_global_context_, mock_entity_registrar_, mock_component_registrar_, mock_system_registrar_),
          mock_entity_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_component_registrar_(mock_global_context_, mock_logic_registrar_),
          mock_system_registrar_(mock_global_context_, mock_logic_registrar_),
          st1_(mock_global_context_, mock_logic_registrar_),
          st2_(mock_global_context_, mock_logic_registrar_)
    {
        st1_.Load(application::kNoFlags);
        st1_.RegisterID(0);

        st2_.RegisterID(1);
    }
};

TEST_F(SystemTest, Load_NotLoaded) {
    ASSERT_TRUE(st2_.Load(application::kNoFlags));
    EXPECT_FALSE(st2_.loaded());

    st2_.EndOfFrame();
    st2_.AdvanceFrame();

    ASSERT_TRUE(st2_.loaded());
}

TEST_F(SystemTest, Load_LoadedNextFrame) {
    ASSERT_TRUE(st1_.Load(application::kNoFlags));
    EXPECT_FALSE(st1_.loaded());

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.loaded());
}

TEST_F(SystemTest, Load_LoadedCurrentFrame) {
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.Load(application::kNoFlags));
    ASSERT_TRUE(st1_.loaded());
    
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.loaded());
}

TEST_F(SystemTest, Load_UnloadedNextFrame) {
    st1_.EndOfFrame();
    st1_.AdvanceFrame();
    st1_.Unload();

    EXPECT_FALSE(st1_.Load(application::kNoFlags));
    ASSERT_TRUE(st1_.loaded());
    
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.loaded());
}

TEST_F(SystemTest, UndoLoad_NotLoaded) {
    ASSERT_TRUE(st2_.UndoLoad(application::kNoFlags));
    EXPECT_FALSE(st2_.loaded());

    st2_.EndOfFrame();
    st2_.AdvanceFrame();

    EXPECT_FALSE(st2_.loaded());
}

TEST_F(SystemTest, UndoLoad_LoadedNextFrame) {
    ASSERT_TRUE(st1_.UndoLoad(application::kNoFlags));
    
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.loaded());
}

TEST_F(SystemTest, UndoLoad_LoadedCurrentFrame) {
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.UndoLoad(application::kNoFlags));

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.loaded());
}

TEST_F(SystemTest, Unload_LockedList) {
    ASSERT_TRUE(st1_.AddLock("test"));
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.Unload());

    st1_.EndOfFrame();
    st1_.AdvanceFrame();
    
    ASSERT_TRUE(st1_.loaded());
}

TEST_F(SystemTest, Unload_NotLoadedCurrentFrame_NotLoadedNextFrame) {
    ASSERT_TRUE(st2_.Unload());

    st2_.EndOfFrame();
    st2_.AdvanceFrame();

    EXPECT_FALSE(st2_.loaded());
}

TEST_F(SystemTest, Unload_NotLoadedCurrentFrame_LoadedNextFrame) {
    EXPECT_FALSE(st1_.Unload());

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.loaded());
}

TEST_F(SystemTest, Unload_LoadedCurrentFrame_LoadedNextFrame) {
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.Unload());
    ASSERT_TRUE(st1_.loaded());

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.loaded());
}

TEST_F(SystemTest, Unload_LoadedCurrentFrame_NotLoadedNextFrame) {
    st1_.EndOfFrame();
    st1_.AdvanceFrame();
    st1_.Unload();

    ASSERT_TRUE(st1_.Unload());
    ASSERT_TRUE(st1_.loaded());

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.loaded());
}

TEST_F(SystemTest, AddLock_UnloadedList) {
    EXPECT_FALSE(st2_.AddLock("test"));

    st2_.EndOfFrame();
    st2_.AdvanceFrame();

    EXPECT_FALSE(st2_.has_lock("test"));
}

TEST_F(SystemTest, AddLock_LoadedList) {
    ASSERT_TRUE(st1_.AddLock("test"));

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.has_lock("test"));
}

TEST_F(SystemTest, AddLock_AlreadyExistingLock) {
    ASSERT_TRUE(st1_.AddLock("test"));
    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.AddLock("test"));

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    ASSERT_TRUE(st1_.has_lock("test"));
}

TEST_F(SystemTest, AddLock_UnloadedNextFrame) {
    st1_.EndOfFrame();
    st1_.AdvanceFrame();
    st1_.Unload();

    EXPECT_FALSE(st1_.AddLock("test"));

    st1_.EndOfFrame();
    st1_.AdvanceFrame();

    EXPECT_FALSE(st1_.has_lock("test"));
}