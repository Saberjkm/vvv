#include "gtest/gtest.h"

#include <string>
#include <vector>

#include "strings/parser.h"

namespace strings::parser {

class ParserTest : public ::testing::Test {
    protected:
    unsigned consumed_amount_ = 0;
    std::string result_string_ = "";
    std::string test_string_ = "";
    std::vector<std::string> invalid_strings_;
    std::vector<std::string> valid_strings_;
};

TEST_F(ParserTest, isCharacterworks) {
    // INVALID - Test for no string
    EXPECT_FALSE(IsCharacter('t', consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Test for wrong character in some string
    test_string_ = "f";
    EXPECT_FALSE(IsCharacter('t', consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Test for right character in wrong place
    test_string_ = "ft";
    EXPECT_FALSE(IsCharacter('t', consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Test on a string containing no right characters
    test_string_ = "t";
    EXPECT_FALSE(IsCharacter("hello", consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // VALID - Test for right character
    test_string_ = "t";
    ASSERT_TRUE(IsCharacter('t', consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 1);
    EXPECT_EQ(result_string_, "t");
    // VALID - Test on string containing right character
    test_string_ = "t";
    EXPECT_FALSE(IsCharacter("est", consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 1);
    EXPECT_EQ(result_string_, "t");
}

TEST_F(ParserTest, parseStringCharacterWorks) {
    // INVALID - Test for no string
    EXPECT_FALSE(ParseStringCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Command character
    test_string_ = "\\";
    EXPECT_FALSE(ParseStringCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // VALID - Command character with escape
    test_string_ = "\\@";
    ASSERT_TRUE(ParseStringCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 2);
    EXPECT_EQ(result_string_, "\\@");
    // VALID - String Character
    test_string_ = "a";
    consumed_amount_ = 0;
    result_string_ = "";
    ASSERT_TRUE(ParseStringCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 1);
    EXPECT_EQ(result_string_, "a");
}

TEST_F(ParserTest, parseNumberCharacterWorks) {
    // INVALID - Test for no string
    EXPECT_FALSE(ParseNumberCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Command character
    test_string_ = "\\";
    EXPECT_FALSE(ParseNumberCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Normal Character
    test_string_ = "a";
    EXPECT_FALSE(ParseNumberCharacter(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // VALID - Number Characters
    for (int i = 0; i < 10; i++) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = std::to_string(i);
        ASSERT_TRUE(ParseNumberCharacter(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 1);
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseOptStringWorks) {
    // SET UP
    valid_strings_.emplace_back("");
    valid_strings_.emplace_back("t");
    valid_strings_.emplace_back("12341 515");
    valid_strings_.emplace_back("test12345");
    valid_strings_.emplace_back("test");

    // VALID - Test stop on command character
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "test@";
    ASSERT_TRUE(ParseOptString(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size() - 1);
    EXPECT_EQ(result_string_, "test");

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        ASSERT_TRUE(ParseOptString(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseStringWorks) {
    // INVALID - Empty String
    EXPECT_FALSE(ParseString(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Command character first
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "@test";
    EXPECT_FALSE(ParseString(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // VALID - Full String
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "test";
    ASSERT_TRUE(ParseOptString(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
    EXPECT_EQ(result_string_, "test");
    // VALID - Stop on Command
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "test@";
    ASSERT_TRUE(ParseOptString(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size() - 1);
    EXPECT_EQ(result_string_, "test");
}


TEST_F(ParserTest, parseNumberWorks) {
    // INVALID - Empty String
    EXPECT_FALSE(ParseNumber(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - Command character first
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "@1234";
    EXPECT_FALSE(ParseNumber(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // INVALID - String character first
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "a1234";
    EXPECT_FALSE(ParseNumber(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) 0);
    EXPECT_EQ(result_string_, "");
    // VALID - Full Number
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "1234";
    ASSERT_TRUE(ParseNumber(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
    EXPECT_EQ(result_string_, "1234");
    // VALID - Stop on Command
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "1234@5";
    ASSERT_TRUE(ParseNumber(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size() - 2);
    EXPECT_EQ(result_string_, "1234");
    // VALID - Stop on Character
    consumed_amount_ = 0;
    result_string_ = "";
    test_string_ = "1234a5";
    ASSERT_TRUE(ParseNumber(consumed_amount_, test_string_, result_string_));
    EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size() - 2);
    EXPECT_EQ(result_string_, "1234");
}

TEST_F(ParserTest, parseSystemWorks) {
    invalid_strings_.emplace_back("");
    invalid_strings_.emplace_back("s32]");
    invalid_strings_.emplace_back("s[32");
    invalid_strings_.emplace_back("e[32]");
    invalid_strings_.emplace_back("s@32]");
    invalid_strings_.emplace_back("s[]");

    valid_strings_.emplace_back("s[1032]");
    valid_strings_.emplace_back("s[hello]");

    // INVALID - Test list of strings
    for (auto invalidString : invalid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = invalidString;
        EXPECT_FALSE(ParseSystem(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 0);
        EXPECT_EQ(result_string_, "");
    }

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        EXPECT_TRUE(ParseSystem(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseComponentWorks) {
    invalid_strings_.emplace_back("");
    invalid_strings_.emplace_back("c32|32]");
    invalid_strings_.emplace_back("c[32|32");
    invalid_strings_.emplace_back("s[32|32]");
    invalid_strings_.emplace_back("c[32@32]");
    invalid_strings_.emplace_back("c[test|test]");

    valid_strings_.emplace_back("c[32|32]");
    valid_strings_.emplace_back("c[hello|32]");

    // INVALID - Test list of strings
    for (auto invalidString : invalid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = invalidString;
        EXPECT_FALSE(ParseComponent(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 0);
        EXPECT_EQ(result_string_, "");
    }

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        EXPECT_TRUE(ParseComponent(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseEntityWorks) {
    invalid_strings_.emplace_back("");
    invalid_strings_.emplace_back("e32]");
    invalid_strings_.emplace_back("e[32");
    invalid_strings_.emplace_back("s[32]");
    invalid_strings_.emplace_back("e[3@]");

    valid_strings_.emplace_back("e[32]");

    // INVALID - Test list of strings
    for (auto invalidString : invalid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = invalidString;
        EXPECT_FALSE(ParseEntity(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 0);
        EXPECT_EQ(result_string_, "");
    }

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        EXPECT_TRUE(ParseEntity(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseSpecialisedCommandWorks) {
    invalid_strings_.emplace_back("");
    invalid_strings_.emplace_back("e32]");
    invalid_strings_.emplace_back("s[32");
    invalid_strings_.emplace_back("c[32]");
    invalid_strings_.emplace_back("e[3@]");

    valid_strings_.emplace_back("e[32]");
    valid_strings_.emplace_back("s[32]");
    valid_strings_.emplace_back("c[33|32]");

    // INVALID - Test list of strings
    for (auto invalidString : invalid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = invalidString;
        EXPECT_FALSE(ParseSpecialisedCommand(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 0);
        EXPECT_EQ(result_string_, "");
    }

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        EXPECT_TRUE(ParseSpecialisedCommand(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseBaseCommandWorks) {
    invalid_strings_.emplace_back("");
    invalid_strings_.emplace_back("\\e[32]");
    invalid_strings_.emplace_back("@d[32");
    invalid_strings_.emplace_back("|c[32]");
    invalid_strings_.emplace_back("sds@e[3@]");

    valid_strings_.emplace_back("@e[32]");
    valid_strings_.emplace_back("@s[32]");
    valid_strings_.emplace_back("@c[33|32]");

    // INVALID - Test list of strings
    for (auto invalidString : invalid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = invalidString;
        EXPECT_FALSE(ParseBaseCommand(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 0);
        EXPECT_EQ(result_string_, "");
    }

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        EXPECT_TRUE(ParseBaseCommand(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

TEST_F(ParserTest, parseCommandStringWorks) {
    invalid_strings_.emplace_back("@");
    invalid_strings_.emplace_back("@d[32");
    invalid_strings_.emplace_back("|c[32]");
    invalid_strings_.emplace_back("@e[3@]");

    valid_strings_.emplace_back("test @e[32] end");
    valid_strings_.emplace_back("test @s[32]");
    valid_strings_.emplace_back("@c[33|32] end");
    valid_strings_.emplace_back("test");
    valid_strings_.emplace_back("");

    // INVALID - Test list of strings
    for (auto invalidString : invalid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = invalidString;
        EXPECT_TRUE(ParseCommandString(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) 0);
        EXPECT_EQ(result_string_, "");
    }

    // VALID - Test list of strings
    for (auto validString : valid_strings_) {
        consumed_amount_ = 0;
        result_string_ = "";
        test_string_ = validString;
        EXPECT_TRUE(ParseCommandString(consumed_amount_, test_string_, result_string_));
        EXPECT_EQ(consumed_amount_, (unsigned) test_string_.size());
        EXPECT_EQ(result_string_, test_string_);
    }
}

}

