#include "scenes/sceneregistrar.h"
#include "application/xml.h"
#include "logic/logicregistrar.h"
#include "scenes/sceneregister.h"
#include "scenes/vvvsceneregister.h"
#include "scenes/scenes.h"
#include "application/console.h"
#include "application/file.h"

namespace scenes {

//-- Scene Registrar methods

    bool SceneRegistrar::UnloadSceneData(const SceneInfo& scene_info, SceneData& scene_data) {
        return scene_registers_[scene_info.related_register]->UnloadScene(scene_info.scene_label, scene_data);
    }

    SceneRegistrar::SceneRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar) 
        : global_context_(global_context),
          related_logic_registrar_(related_logic_registrar),
          core_scene_register_(std::make_shared<VVVSceneRegister>(*this, kVVVSceneRegisterName))
    {
        scene_registers_[kVVVSceneRegisterName] = core_scene_register_;
        initial_scenes_.emplace_back(kVVVCoreSceneName);
    }

    bool SceneRegistrar::AdvanceFrame() {
        // Changed scenes means that scenes exist in both frames (not unloaded)
        scene_data_.insert(next_frame_.new_scene_data.begin(), next_frame_.new_scene_data.end());
        next_frame_.new_scene_data.clear();
        current_frame_.changed_scenes_this_frame = next_frame_.changed_scenes_this_frame;
        next_frame_.changed_scenes_this_frame.clear();
        current_frame_.unloaded_scenes_this_frame = next_frame_.unloaded_scenes_this_frame;
        next_frame_.unloaded_scenes_this_frame.clear();
        current_frame_.loaded_scenes_this_frame = next_frame_.loaded_scenes_this_frame;
        next_frame_.loaded_scenes_this_frame.clear();
        current_frame_.main_scene = next_frame_.main_scene;
        current_frame_.main_scene_entities = next_frame_.main_scene_entities;
        return true;
    }

    bool SceneRegistrar::EndOfFrame() {
        bool return_value = true;
        // Unloads the scenes that have been scheduled to be unloaded
        for (auto& unloaded_scene : next_frame_.unloaded_scenes_this_frame) {
            UnloadSceneData(scene_info_[unloaded_scene], scene_data_[unloaded_scene]) ? scene_data_.erase(unloaded_scene) : return_value = false;
        }
        return return_value;
    }

    bool SceneRegistrar::Initialise(const application::SceneRegistrarSettings& scene_registrar_settings) {
        std::function<void(std::size_t)> function = [this](std::size_t id) { this->AddEntity(id); };
        related_logic_registrar_.entity_registrar().AddCreationCallback({"VVV_scene_registrar", function});
        // Not checking for error here, should still initialise if you cant add the scene registers
        for (const auto& scene_register : scene_registrar_settings.scene_registers) {
            AddSceneRegister(scene_register.first, scene_register.second);
        }
        RegisterScenes();
        return true;
    }

    bool SceneRegistrar::FirstFrame() {
        for (auto& scene : initial_scenes_) {
            if (!LoadScene(scene)) return false;
        }
        return true;
    }

    bool SceneRegistrar::AddSceneRegister(const std::string& register_label, const std::shared_ptr<SceneRegister>& register_ref) {
        if (scene_registers_.contains(register_label)) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::AddSceneRegister(std::string, std::shared_ptr<SceneRegister>", 
                "Invalid Scene Register",
                fmt::format("Scene Register already exists with name {}", register_label)
            );
            return false;
        }
        if (register_ref) { 
            scene_registers_.insert({register_label, register_ref});
            return true;
        } else {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::AddSceneRegister(std::string, std::shared_ptr<SceneRegister>", 
                "Invalid Scene Register",
                fmt::format("The Scene Register with name {} doesn't exist", register_label)
            );
            return false;
        }
        return false;
    }

    bool SceneRegistrar::AddSceneInfo(SceneInfo scene_info) {
        if (scene_info.scene_label.size() == 0) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::AddSceneInfo(SceneInfo)", 
                "Invalid Scene Info ID",
                fmt::format("Trying to add Scene Info without an ID")
            );
            return false;
        }
        if (scene_info_.contains(scene_info.scene_label) && !scene_info.override) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::AddSceneInfo(SceneInfo)", 
                "Invalid Scene Info ID",
                fmt::format("Trying to add Scene Info that already exists with ID {} and not marked to override.", scene_info.scene_label)
            );
            return false;
        }
        scene_info_.insert_or_assign(scene_info.scene_label, scene_info);
        return true;
    }

    void SceneRegistrar::RegisterScenes() {
        for (auto& scene_register : scene_registers_) {
            for (auto& scene_info : scene_register.second->GetSceneInfo()) {
                AddSceneInfo(scene_info);
            }
        }
    }
    
    bool SceneRegistrar::LoadScene(std::string scene_label) {
        if (!scene_info_.contains(scene_label)) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::LoadScene(std::string)", 
                "Invalid Scene Label",
                fmt::format("Trying to load Scene that doesn't exist with Label {}", scene_label)
            );
            return false;
        }
        auto scene_data = scene_registers_[scene_info_[scene_label].related_register]->LoadScene(scene_label);
        if (!scene_data.is_valid) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::LoadScene(std::string)", 
                "Bad Load",
                fmt::format("Trying to load Scene with Label {} but there were some errors in loading.", scene_label)
            );
            return false;
        }
        next_frame_.new_scene_data.emplace(scene_label, scene_data);
        next_frame_.loaded_scenes_this_frame.emplace(scene_label);
        return true;    
    }

    bool SceneRegistrar::UnloadScene(std::string scene_label) {
        if (!scene_info_.contains(scene_label)) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::UnloadScene(std::string)", 
                "Invalid Scene ID",
                fmt::format("Trying to unload Scene that doesn't exist with ID {}", scene_label)
            );
            return false;
        }
        if (!scene_data_.contains(scene_label)) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::UnloadScene(std::string)", 
                "Scene Not Loaded",
                fmt::format("Trying to unload Scene that isn't loaded with ID {}", scene_label)
            );
            return false;
        }
        if (!scene_registers_[scene_info_[scene_label].related_register]->UnloadScene(scene_label, scene_data_[scene_label])) {
            global_context_.main_console().IssueWarning(
                "scenes::SceneRegister::LoadScene(std::string)", 
                "Bad Load",
                fmt::format("Trying to load Scene with ID {} but there were some errors in loading.", scene_label)
            );
            return false;
        }
        next_frame_.unloaded_scenes_this_frame.emplace(scene_label);
        return true;    
    }

    void SceneRegistrar::AddEntity(std::size_t entity_id) {
        if (!next_frame_.main_scene_entities.contains(entity_id)) next_frame_.main_scene_entities.emplace(entity_id);
    }

//--

}
