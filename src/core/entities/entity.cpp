
#include "entities/entity.h"
#include "entities/entitysearchtree.h"
#include "entities/entities.h"
#include "entities/entityregistrar.h"
#include "application/application.h"
#include "application/console.h"
#include "logic/logicregistrar.h"
#include "fmt/format.h"

namespace entities {

    //-- Entity Class

    bool Entity::HandleFlags(const application::FlagData& flag_data) {
        if (!WillBeLoaded()) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::HandleFlags(const application::FlagData&)", 
                "Invalid Entity",
                fmt::format("Entity {} won't be loaded next frame so handling flags is invalid.", entity_id_)
            );
            return {};
        }
        if (flag_data.flags & application::kLock) {
            if (!AddLock(flag_data.lock_owner_id)) return false;
        }
        return true;
    }
    
    std::map<std::size_t, std::weak_ptr<Entity>> Entity::child_references(std::string component_label) const {
        std::map<std::size_t, std::weak_ptr<Entity>> return_vector;
        if (current_frame_.child_entities.find(component_label) != current_frame_.child_entities.end()) {
            return_vector = current_frame_.child_entities.find(component_label)->second;
        } else {

        }
        return return_vector;
    }

    std::weak_ptr<Entity> Entity::parent_reference(std::string component_label) const {
        if (current_frame_.parent_entity.find(component_label) != current_frame_.parent_entity.end()) {
            return (current_frame_.parent_entity.at(component_label));
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::parent_reference(std::size_t)", 
                "Invalid Label ID",
                fmt::format("Entity doesn't reference an Entity for Label ID {}", component_label)
            );
            return {};
        }
    }

    std::pair<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> Entity::GetDataRepresentation() {
        if (IsLoaded()) {
            std::pair<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> return_representation;
            for (auto& component : current_frame_.component_data) {
                return_representation.second.emplace(component.first, component.second);
            }
            return_representation.first = entity_id_;
            return return_representation;
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::GetDataRepresentation()", 
                "Invalid Entity",
                fmt::format("Trying to get the representation of an Entity that isn't valid.")
            );
            return {(std::size_t) -1, {}};
        }

    }

    std::pair<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> Entity::GetDataRepresentation(std::set<std::string> component_labels) {
        if (component_labels.size() == 0) return GetDataRepresentation();
        if (IsLoaded()) {
            std::pair<std::size_t, std::map<std::string, std::pair<std::size_t, ComponentAccess>>> return_representation;
            for (auto component_label : component_labels) {
                if (current_frame_.component_data.contains(component_label)) {
                    return_representation.second.emplace(component_label, current_frame_.component_data[component_label]);
                }
            }
            return_representation.first = entity_id_;
            return return_representation;
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::GetDataRepresentation(std::set<std::size_t>)", 
                "Invalid Entity",
                fmt::format("Trying to get the representation of an Entity that isn't valid.")
            );
            return {(std::size_t) -1, {}};
        }
    }

    bool Entity::Load(const application::FlagData& flag_data) {
        // To make sure you arent initialising an existing entity
        if (!next_frame_.has_loaded) {
            next_frame_.has_loaded = true;
            has_changed_ = true;
        }
        HandleFlags(flag_data);
        return next_frame_.has_loaded;
    }

    bool Entity::EndOfFrame() {
        if (has_changed_) {
            if (WillBeLoaded()) {
                if (current_frame_.tree_location) { current_frame_.tree_location->DeleteKey(entity_id_); }
                next_frame_.tree_location = parent_registrar_.InsertKey(entity_id_, &next_frame_.component_data);
                return (next_frame_.tree_location);
            }
        }
        return true;
    }
    
    bool Entity::AdvanceFrame(){
        if (has_changed_) {
            has_changed_ = false;
            current_frame_ = next_frame_;
            return true;
        }
        return false; 
    }

    bool Entity::Clear() {
        bool return_bool = true;
        if (next_frame_.locks.size() != 0) { return false; }
        if (WillBeLoaded()) {
            // Components that need to be deleted
            for (auto& component : current_frame_.component_data) {
                if (!DeleteComponent(component.first, false)) return_bool = false;
            }
            // Components that will be created next frame so needs undoing
            for (auto& future_component : next_frame_.component_data) {
                if (!UndoComponentAddition(future_component.first)) return_bool = false;
            }
            next_frame_.component_data.clear();
            // The following will be cleared up by the componenent deletions unless for an error so just to make sure
            next_frame_.parent_entity.clear();
            next_frame_.child_entities.clear();
            //
            next_frame_.tree_location = nullptr;
            next_frame_.has_loaded = false;
            has_changed_ = true;
        }
        return return_bool;
    }

    bool Entity::AddLock(std::string lock_name) {
        if (!WillBeLoaded()) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::AddLock(std::string)",
                "Invalid Entity",
                fmt::format("Entity {} will not be loaded next frame to add Lock {}", entity_id_, lock_name)
            );
            return false;
        }
        if (lock_name.size() != 0) {
            if (!current_frame_.locks.contains(lock_name) && !next_frame_.locks.contains(lock_name) && WillBeLoaded()) {
                next_frame_.locks.emplace(lock_name);
                has_changed_ = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::AddLock(std::string)",
                    "Invalid Lock",
                    fmt::format("Trying add a lock with Name {}, but it already has a lock with that name.", lock_name)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::AddLock(std::string)",
                "Invalid Lock Name",
                "Trying to add a Lock with an empty name."
            );
            return false;
        }
    }

    bool Entity::UndoAddLock(std::string lock_name) {
        if (!WillBeLoaded()) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoAddLock(std::string)",
                "Invalid Entity",
                fmt::format("Entity {} will not be loaded next frame to undo adding of Lock {}", entity_id_, lock_name)
            );
            return false;
        }
        if (lock_name.size()) {
            if (!current_frame_.locks.contains(lock_name) && next_frame_.locks.contains(lock_name)) {
                next_frame_.locks.erase(lock_name);
                has_changed_ = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoAddLock(std::string)",
                    "Invalid Lock",
                    fmt::format("Trying to undo adding a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoAddLock(std::string)",
                "Invalid Lock Name",
                "Trying to undo adding a Lock with an empty name."
            );
            return false;
        }
    }

    bool Entity::Unlock(std::string lock_name) {
        if (!WillBeLoaded()) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::Unlock(std::string)",
                "Invalid Entity",
                fmt::format("Entity {} will not be loaded next frame to unlock Lock {}", entity_id_, lock_name)
            );
            return false; 
        }
        if (!IsLoaded()){
            global_context_.main_console().IssueWarning(
                "entities::Entity::Unlock(std::string)",
                "Invalid Entity",
                fmt::format("Entity {} is not loaded this frame to unlock Lock {}", entity_id_, lock_name)
            );
            return false; 
        }
        if (lock_name.size() != 0) {
            if (current_frame_.locks.contains(lock_name)) {
                if (next_frame_.locks.contains(lock_name)) {
                    next_frame_.locks.erase(lock_name);
                    has_changed_ = true;
                    return true;
                } else {
                    global_context_.main_console().IssueWarning(
                        "entities::Entity::Unlock(std::string)",
                        "Invalid Lock",
                        fmt::format("Trying to ulock a lock with Name {}, but it has already been unlocked.", lock_name)
                    );
                    return false;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::Unlock(std::string)",
                    "Invalid Lock",
                    fmt::format("Trying to unlock a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::Unlock(std::string)",
                "Invalid Lock Name",
                "Trying to unlock a Lock with an empty name."
            );
            return false;
        }
    }

    bool Entity::UndoUnlock(std::string lock_name) {
        if (!WillBeLoaded()) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoUnlock(std::string)",
                "Invalid Entity",
                fmt::format("Entity {} will not be loaded next frame to undo the unlock of Lock {}", entity_id_, lock_name)
            );
            return false; 
        }
        if (!IsLoaded()){
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoUnlock(std::string)",
                "Invalid Entity",
                fmt::format("Entity {} is not loaded this frame to undo the unlock of Lock {}", entity_id_, lock_name)
            );
            return false; 
        }
        if (lock_name.size() != 0) {
            if (current_frame_.locks.contains(lock_name)) {
                if (!next_frame_.locks.contains(lock_name)) {
                    next_frame_.locks.emplace(lock_name);
                    has_changed_ = true;
                    return true;
                } else {
                    global_context_.main_console().IssueWarning(
                        "entities::Entity::UndoUnlock(std::string)",
                        "Invalid Lock",
                        fmt::format("Trying to undo an unlock of a lock with Name {}, but it has not been unlocked.", lock_name)
                    );
                    return false;
                }
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoUnlock(std::string)",
                    "Invalid Lock",
                    fmt::format("Trying to unlock a lock with Name {}, but it doesnt have a lock with that name.", lock_name)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoUnlock(std::string)",
                "Invalid Lock Name",
                "Trying to undo an unlock for a Lock with an empty name."
            );
            return false;
        }
    }

    void Entity::Reference(std::weak_ptr<Entity> other_entity) {
        auto other_ent = other_entity.lock();
        if (other_ent->entity_id() == entity_id()) { return; }
        if (WillBeLoaded() && other_ent->WillBeLoaded()) {
            for (const auto& reference_component : other_ent->current_frame_.component_data) {
                if (HasComponent(reference_component.first) && WillHaveComponent(reference_component.first)) { 
                    if (other_ent->AddChildReference(reference_component.first, parent_registrar_.GetWeakPointer(entity_id_))) {
                        ModifyComponent(reference_component.first, {reference_component.second.first, entities::kR}); 
                        // References have been cleaned by modify, added child reference to other entity in if check
                        next_frame_.parent_entity[reference_component.first] = other_entity;
                    }
                } else {
                    AddRefComponent(other_entity, reference_component.first);               
                }
            }
        }
    }

    bool Entity::AddChildReference(std::string component_label, std::weak_ptr<Entity> child_reference) {
        if (HasComponent(component_label)) {
            if (WillHaveComponent(component_label)) {
                next_frame_.child_entities[component_label].emplace(child_reference.lock()->entity_id(), child_reference);
                has_changed_ = true;
                return true;
            }
        }
        return false;
    }

    bool Entity::DeleteChildReference(std::string component_label, std::weak_ptr<Entity> reference) {
        if (current_frame_.child_entities.find(component_label) != current_frame_.child_entities.end()) {
            if (next_frame_.child_entities.find(component_label) != next_frame_.child_entities.end()) {
                if (next_frame_.child_entities[component_label].find(reference.lock()->entity_id()) != next_frame_.child_entities[component_label].end()) {
                next_frame_.child_entities[component_label].erase(reference.lock()->entity_id());
                has_changed_ = true;
                }
            }
            return true;
        }
        return false;
    }

    bool Entity::UndoChildReferenceDeletion(std::string component_label, std::weak_ptr<Entity> reference) {
        if (!reference.expired()) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoChildReferenceDeletion(std::size_t, std::shared_ptr<Entity>)", 
                "Invalid Reference",
                "Trying to undo a reference deletion to a reference that doesn't exist"
            );
            return false;
        }
        if (!current_frame_.child_entities.contains(component_label)) {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoChildReferenceDeletion(std::size_t, std::shared_ptr<Entity>)", 
                "Invalid Component Label ID",
                fmt::format("Trying to undo a child reference deletion for component label that doesn't exist with ID {}", component_label)
            );
            return false;
        }
        auto ref = reference.lock();
        if (current_frame_.child_entities[component_label].contains(ref->entity_id())) {
            if (!next_frame_.child_entities[component_label].contains(ref->entity_id())) {
                next_frame_.child_entities[component_label].emplace(ref->entity_id(), reference);
                has_changed_ = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoChildReferenceDeletion(std::size_t, std::shared_ptr<Entity>)", 
                    "Invalid Reference",
                    fmt::format("Trying to undo a child reference deletion which wasn't deleted for Component Label ID {}, and Reference ID {}", component_label, reference.lock()->entity_id())
                );
                return false;
            }
        } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoChildReferenceDeletion(std::size_t, std::shared_ptr<Entity>)", 
                    "Invalid Reference",
                    fmt::format("Trying to undo a child reference for Component Label ID {}, and Reference ID {}", component_label, reference.lock()->entity_id())
                );
            return false;
        }
    }

    bool Entity::UndoChildReference(std::string component_label, std::weak_ptr<Entity> reference) {
        if (!HasChildren(component_label) && !current_frame_.child_entities[component_label].contains(reference.lock()->entity_id())) {
            if (WillHaveChildren(component_label)) {
                next_frame_.child_entities[component_label].erase(reference.lock()->entity_id());
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoChildReference(std::size_t, Entity*)", 
                    "Invalid Reference",
                    fmt::format("Reference will not exist for Component Label ID {}, and Reference ID {}", component_label, reference.lock()->entity_id())
                );
                return false;                
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoChildReference(std::size_t, Entity*)", 
                "Invalid Reference",
                fmt::format("Reference already existst for Component Label ID {}, and Reference ID {}", component_label, reference.lock()->entity_id())
            );
            return false;
        }
    }   

    bool Entity::AddComponent(std::string component_label, std::size_t component_id, bool update_tree /* = true*/) {
        if (!WillBeLoaded()) { return false; }
        if (HasComponent(component_label) || WillHaveComponent(component_label)) {
            // Component label already exists
            global_context_.main_console().IssueWarning(
                "entities::Entity::AddComponent(std::size_t, std::size_t, bool)", 
                "Invalid Component Label ID",
                fmt::format("Component with Label ID {} already/will exist in this component", component_label)
            );
            return false;
        } else {
            next_frame_.component_data[component_label] = std::make_pair(component_id, ComponentAccess::kRW);
            has_changed_ = true;
            return true;
        }
    }

    bool Entity::AddComponent(std::initializer_list<std::pair<std::string, std::size_t>> components) {
        if (!WillBeLoaded()) { return false; }
        std::map<std::string, std::pair<std::size_t, ComponentAccess>> new_components;
        for (auto component : components) {
            if (HasComponent(component.first) || WillHaveComponent(component.first) || new_components.contains(component.first)){
                return false;
            } else {
                new_components[component.first] = std::make_pair(component.second, ComponentAccess::kRW);
            }
        }
        next_frame_.component_data.insert(new_components.begin(), new_components.end());
        has_changed_ = true;
        return true;
    }

    bool Entity::AddRefComponent(std::weak_ptr<Entity> reference_entity, std::string component_label) {
        if (!WillBeLoaded()) { return false; }
        if (HasComponent(component_label) || WillHaveComponent(component_label)) {
            // Component label already exists
            global_context_.main_console().IssueWarning(
                "entities::Entity::AddRefComponent(Entity*, std::size_t)", 
                "Invalid Component Label ID",
                fmt::format("Component with Label ID {} already/will exist in this component", component_label)
            );
            return false;
        } else {
            auto ref = reference_entity.lock();
            if (ref->WillHaveComponent(component_label)) {
                next_frame_.component_data[component_label] = std::make_pair(ref->next_frame_.component_data[component_label].first, ComponentAccess::kR);
                ref->next_frame_.child_entities[component_label].emplace(entity_id_, parent_registrar_.GetWeakPointer(entity_id_));
                next_frame_.parent_entity[component_label] = reference_entity;
                has_changed_ = true;
                ref->has_changed_ = true;
                return true;
            } else {
                // Component label doesn't exist in reference
                global_context_.main_console().IssueWarning(
                    "entities::Entity::AddRefComponent(Entity*, std::size_t)", 
                    "Invalid Component Label ID",
                    fmt::format("Component with Label ID {} doesn't exist in the reference", component_label)
                );
                return false;
            }
        }
    }

    bool Entity::UndoComponentAddition(std::string component_label) {
        if (!HasComponent(component_label)) {
            if (WillHaveComponent(component_label)) {
                if (next_frame_.component_data[component_label].second == entities::kRW) {
                    if (!parent_registrar_.related_component_registrar().UndoCreateNewComponent(component_label, next_frame_.component_data[component_label].first).second) { return false; }
                } else if (next_frame_.component_data[component_label].second == entities::kR) {
                    next_frame_.component_data.erase(component_label);
                }
                if (WillHaveChildren(component_label)) {
                    for (auto child : next_frame_.child_entities[component_label]) {
                        child.second.lock()->UndoComponentAddition(component_label);
                    }
                    // No need to delete the reference to a child as the following will handle it
                }
                if (next_frame_.parent_entity.contains(component_label)) {
                    next_frame_.parent_entity[component_label].lock()->UndoChildReference(component_label, parent_registrar_.GetWeakPointer(entity_id_));
                    next_frame_.parent_entity.erase(component_label);
                }
                has_changed_ = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoAddComponent(std::size_t, bool)", 
                    "Invalid Component Label ID",
                    fmt::format("Component with Label ID {} will not exist in the next frame for this component", component_label)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoAddComponent(std::size_t, bool)", 
                "Invalid Component Label ID",
                fmt::format("Component with Label ID {} already exists in the current frame", component_label)
            );
            return false;
        }
    }


    bool Entity::DeleteComponent(std::string component_label, bool update_tree /* = true*/) {
        if (!IsLoaded() && !WillBeLoaded()) { return true; }
        if (HasComponent(component_label) && WillHaveComponent(component_label)) {
            // Clean up at component registrar
            if (next_frame_.component_data[component_label].second == ComponentAccess::kRW) {
                parent_registrar_.related_component_registrar().DeleteComponent(component_label, current_frame_.component_data[component_label].first);
                if (next_frame_.component_data[component_label].first != current_frame_.component_data[component_label].first) {
                    if (next_frame_.component_data[component_label].second == ComponentAccess::kRW) {
                        // Must have created a new component so undo that
                        parent_registrar_.related_component_registrar().UndoCreateNewComponent(component_label, next_frame_.component_data[component_label].second);
                    }
                }
            }
            // Clean up children
            for (auto item : next_frame_.child_entities[component_label]) {
                item.second.lock()->DeleteComponent(component_label);
            }
            // Clean up parent references
            if (current_frame_.parent_entity.find(component_label) != current_frame_.parent_entity.end()) {
                current_frame_.parent_entity[component_label].lock()->DeleteChildReference(component_label, parent_registrar_.GetWeakPointer(entity_id_));
            }
            next_frame_.child_entities.erase(component_label);
            next_frame_.component_data.erase(component_label);
            has_changed_ = true;
            return !WillHaveComponent(component_label);
        }
        return false;
    }

    bool Entity::UndoComponentDeletion(std::string component_label, bool update_tree /*=true*/) {
        if (HasComponent(component_label)) {
            if (!WillHaveComponent(component_label)) {
                next_frame_.component_data[component_label] = current_frame_.component_data[component_label];
                if (current_frame_.component_data[component_label].second == entities::kRW) {
                    parent_registrar_.related_component_registrar().UndoDeleteComponent(component_label, current_frame_.component_data[component_label].first);
                }
                if (current_frame_.child_entities[component_label].size() != 0) {
                    for (auto& child : current_frame_.child_entities[component_label]) {
                        child.second.lock()->UndoComponentDeletion(component_label);
                    }
                }
                // This is where the next frame child references get updated (deletion undone)
                if (current_frame_.parent_entity.contains(component_label)) {
                    current_frame_.parent_entity[component_label].lock()->UndoChildReferenceDeletion(component_label, parent_registrar_.GetWeakPointer(entity_id_));
                    next_frame_.parent_entity[component_label] = current_frame_.parent_entity[component_label];
                }
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoComponentDeletion(std::size_t, bool)", 
                    "Invalid Component Label ID",
                    fmt::format("Trying to undo the deletion of a component that isn't deleted with ID {}, which belongs to Entity with ID {}", component_label, entity_id_)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoComponentDeletion(std::size_t, bool)", 
                "Invalid Component Label ID",
                fmt::format("Trying to undo the deletion of a component that doesn't exist with ID {}, which belongs to Entity with ID {}", component_label, entity_id_)
            );
            return false;
        }
    }

    bool Entity::ModifyComponent(std::string component_label, std::pair<std::size_t, entities::ComponentAccess> component_data, bool clean_up_component /*= true */) {
        if (HasComponent(component_label)) {
            if (WillHaveComponent(component_label)) {
                if (clean_up_component) {
                    parent_registrar_.related_component_registrar().DeleteComponent(component_label, current_frame_.component_data[component_label].first);
                    if (HasChildren(component_label)) {
                        for (auto& child : current_frame_.child_entities[component_label]) {
                            child.second.lock()->DeleteComponent(component_label, true);
                        }
                    }
                    if (current_frame_.parent_entity.contains(component_label)) {
                        current_frame_.parent_entity[component_label].lock()->DeleteChildReference(component_label, parent_registrar_.GetWeakPointer(entity_id_));
                        current_frame_.parent_entity.erase(component_label);
                    }
                }
                next_frame_.component_data[component_label] = component_data;
                has_changed_ = true;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    bool Entity::UndoCreation(const application::EntityData& entity) {
        bool return_bool = true;
        if (entity_id_ == (std::size_t) -1) {
            if (entity_id_ != (std::size_t) -1) {
                if (next_frame_.tree_location) {
                    next_frame_.tree_location->UndoKeyAddition(entity_id_);
                }
                for (auto& component : next_frame_.component_data) {
                    UndoComponentAddition(component.first);
                }
                next_frame_.parent_entity.clear();
                next_frame_.child_entities.clear();
                next_frame_.component_data.clear();
                next_frame_.has_loaded = false;
                next_frame_.tree_location = nullptr;    
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoCreation()", 
                    "Invalid Entity",
                    fmt::format("Trying to undo the creation of an Entity that is already undone with ID {}", entity_id_)
                );
                return false;    
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoCreation()", 
                "Invalid Entity",
                fmt::format("Trying to undo the creation of an Entity that already exists with ID {}", entity_id_)
            );
            return false;
        }
        return return_bool;
    }

    bool Entity::UndoDeletion(bool ignore_bad_references /*=false*/, bool show_warnings /*=true*/) {
        if (entity_id_ != (std::size_t) -1) {
            if (entity_id_ == (std::size_t) -1) {
                // -- Error Checking
                    std::string bad_references = "";
                    for (auto& child_entities : current_frame_.child_entities) {
                        for (auto& child : child_entities.second) {
                            if (!child.second.lock()->WillBeLoaded()) {
                                bad_references += "Child, ID: " + std::to_string(child.second.lock()->entity_id());
                            }
                        }
                    }
                    for (auto& parent : current_frame_.parent_entity) {
                        if (!parent.second.lock()->WillBeLoaded()) {
                            bad_references += "Parent, ID: " + std::to_string(parent.second.lock()->entity_id());
                        }
                    }
                    // Errors in the references
                    if (bad_references.size() != 0) {
                        if (show_warnings) {
                            global_context_.main_console().IssueWarning(
                                "entities::Entity::UndoCreation()", 
                                "Bad Reference(s)",
                                fmt::format("Trying to undo the deletion of an Entity with ID {}, and References {}", entity_id_, bad_references)
                            );
                        }
                        if (!ignore_bad_references) return false;
                    }
                // -- End of Error Checking
                for (auto& component_data : current_frame_.component_data) {
                    UndoComponentDeletion(component_data.first, false);
                }
                next_frame_.has_loaded = true;
                has_changed_ = true;
                return true;
            } else {
                global_context_.main_console().IssueWarning(
                    "entities::Entity::UndoCreation()", 
                    "Bad Reference(s)",
                    fmt::format("Trying to undo the deletion of an Entity that isn't deleted with ID {}", entity_id_)
                );
                return false;
            }
        } else {
            global_context_.main_console().IssueWarning(
                "entities::Entity::UndoCreation()", 
                "Bad Reference(s)",
                fmt::format("Trying to undo the deletion of an Entity that doesn't exist with ID {}", entity_id_)
            );
            return false;
        }
    }
    //-- End of Entity Class
}