#include <iostream>
#include "entities/entitysearchtree.h"
namespace entities {
    //-- EntitySearchTree Class

    void EntitySearchTree::AdvanceFrame() {
        if (has_changed_) { 
            entity_values_ = next_entity_values_; 
            has_changed_ = false;
        }
        for (auto item : child_nodes_) {
            if (item.second) { item.second->AdvanceFrame(); }
        }
    }

    EntitySearchTree* EntitySearchTree::InsertKey(std::size_t entity_key, const std::map<std::string, std::pair<std::size_t, ComponentAccess>>* entity_components) {
        EntitySearchTree* tree_pointer = this;
        // Exploiting the sorted part of a map
        for (auto component : *entity_components) {
            auto treeLocation = tree_pointer->child_nodes_.find(component.first);
            // Check if tree has a child with the required component label id
            if (treeLocation != tree_pointer->child_nodes_.end()) {
                tree_pointer = treeLocation->second;
            } else {
                tree_pointer->child_nodes_[component.first] = new EntitySearchTree(tree_pointer->depth_+1);
                tree_pointer = tree_pointer->child_nodes_[component.first];
            }
        }
        tree_pointer->next_entity_values_.emplace(entity_key);
        tree_pointer->has_changed_ = true;
        return tree_pointer;
    }

    bool EntitySearchTree::DeleteKey(std::size_t entity_key) {
        if (entity_values_.contains(entity_key)) {
            auto num_deleted = next_entity_values_.erase(entity_key);
            if (num_deleted) has_changed_ = true;
        }
        return next_entity_values_.find(entity_key) == next_entity_values_.end();
    }

    bool EntitySearchTree::UndoKeyAddition(std::size_t entity_key) {
        if (!entity_values_.contains(entity_key)) {
            auto num_deleted = next_entity_values_.erase(entity_key);
            if (num_deleted) has_changed_ = true;
        }
        return !next_entity_values_.contains(entity_key);
    }

    std::vector<std::size_t> EntitySearchTree::GatherAllEntities() {
        std::vector<std::size_t> return_list;
        return_list.insert(return_list.end(), entity_values_.begin(), entity_values_.end());
        for (auto& child_tree : child_nodes_) {     
            auto gathered_list = child_tree.second->GatherAllEntities();
            return_list.insert(return_list.end(), gathered_list.begin(), gathered_list.end());
        }
        return return_list;
    }
        
    std::vector<std::size_t> EntitySearchTree::GatherEntities(const std::set<std::string>& component_labels) {
        std::vector<std::vector<entities::EntitySearchTree*>> index_tracking(component_labels.size()+ 1); // Tracks how deep the child trees are within the component label list
        if (component_labels.size() == 0) { return GatherAllEntities(); }
        std::vector<std::size_t> return_list;
        index_tracking[0].emplace_back(this); // Prep the search
        auto component_labels_iterator = component_labels.begin();
        for (std::size_t i = 0; i < component_labels.size(); i++) {
            while (index_tracking[i].size() != 0) {
                auto& tree_pointer = index_tracking[i].back();
                index_tracking[i].pop_back();
                for (auto& child : tree_pointer->child_nodes_) {
                    if (child.first == *component_labels_iterator) {
                        index_tracking[i+1].emplace_back(child.second);//errpr
                    } else if (child.first <= *component_labels_iterator) {
                        index_tracking[i].emplace_back(child.second);
                    }
                    // Bigger so discount it
                }
            }
            component_labels_iterator++;
        }
        // Now gather all the entities which match the labels
        for (auto& gather_target : index_tracking[component_labels.size()]) {
            auto gathered_list = gather_target->GatherAllEntities();
            return_list.insert(return_list.end(), gathered_list.begin(), gathered_list.end());
        }
        return return_list;
    }

    void EntitySearchTree::PrintTree(std::string prefix) {
        std::cout << prefix << "} = ";
        for (auto item : entity_values_) {
            std::cout << std::to_string(item) + ", ";
        } 
        std::cout << std::endl;
        for (auto item : child_nodes_) {
            if (item.second) { item.second->PrintTree(prefix + item.first + ", "); }
        }
    }
    //-- End of EntitySearchTree Class
}
