
#include "graphics/graphicsdata.h"

namespace graphics {

// -- Pipeline Storage

    void PipelineStorage::CleanUp(VkDevice& device, const VkAllocationCallbacks* p_allocator) {
        for (auto& item : pipelines) {
            vkDestroyPipeline(device, item.second.first, p_allocator);
            vkDestroyPipelineLayout(device, item.second.second, p_allocator);
        }
        pipelines.clear();
    }
    bool PipelineStorage::CreateEntry(std::string name) {
        if (pipelines.find(name) == pipelines.end()) {
            pipelines[name] = {VkPipeline(), VkPipelineLayout()};
            return true;
        }
        return false;
    }

    VkPipelineLayout& PipelineStorage::GetPipelineLayout(const std::string& pipeline_name) {
        if (pipelines.find(pipeline_name) != pipelines.end()) {
            return pipelines[pipeline_name].second;
        }
        throw std::runtime_error("There was no pipeline named: " + pipeline_name);
    }

    VkPipeline& PipelineStorage::getPipeline(const std::string& pipeline_name) {
        if (pipelines.find(pipeline_name) != pipelines.end()) {
            return pipelines[pipeline_name].first;
        }
        throw std::runtime_error("There was no pipeline named: " + pipeline_name);
    }

// --

// -- Pipeline Creator
    void PipelineCreator::InitialiseValues() {
        // These are generally the default values of the pipeline for my system
        vert_shader_stage_info_.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vert_shader_stage_info_.stage = VK_SHADER_STAGE_VERTEX_BIT;

        frag_shader_stage_info_.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        frag_shader_stage_info_.stage = VK_SHADER_STAGE_FRAGMENT_BIT;

        input_assembly_.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        input_assembly_.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        input_assembly_.primitiveRestartEnable = VK_FALSE;

        viewport_.x = 0.0f;
        viewport_.y = 0.0f;

        viewport_.width = (float) swap_chain_extent_.width;
        viewport_.height = (float) swap_chain_extent_.height;
        viewport_.minDepth = 0.0f;
        viewport_.maxDepth = 1.0f;

        scissor_.offset = {0, 0};
        scissor_.extent = swap_chain_extent_;

        view_port_state_.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        view_port_state_.viewportCount = 1;
        view_port_state_.pViewports = &viewport_;
        view_port_state_.scissorCount = 1;
        view_port_state_.pScissors = &scissor_;

        rasterizer_.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer_.depthClampEnable = VK_FALSE;
        rasterizer_.rasterizerDiscardEnable = VK_FALSE;
        rasterizer_.polygonMode = VK_POLYGON_MODE_FILL;
        rasterizer_.lineWidth = 1.0f;
        rasterizer_.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer_.frontFace = VK_FRONT_FACE_CLOCKWISE;
        rasterizer_.depthBiasEnable = VK_FALSE;

        multisampling_.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisampling_.sampleShadingEnable = VK_FALSE;
        multisampling_.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

        color_blend_attachment_.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        color_blend_attachment_.blendEnable = VK_FALSE;

        color_blending_.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        color_blending_.logicOpEnable = VK_FALSE;
        color_blending_.attachmentCount = 1;
        color_blending_.pAttachments = &color_blend_attachment_;

        depth_stencil_.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depth_stencil_.depthTestEnable = VK_TRUE;
        depth_stencil_.depthWriteEnable = VK_TRUE;
        depth_stencil_.depthCompareOp = VK_COMPARE_OP_LESS;
        depth_stencil_.depthBoundsTestEnable = VK_FALSE;
        depth_stencil_.stencilTestEnable = VK_FALSE;

        pipeline_layout_info_.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        
    }

    void PipelineCreator::CreatePipeline(PipelineCreateInfo creation_info) {
        if (!storage_location_.CreateEntry(creation_info.name)) return;

        // -- Data that needs to be supplied / refreshed
            pipeline_layout_info_.setLayoutCount = 1;
            pipeline_layout_info_.pSetLayouts = creation_info.descriptor_set_layout;

            VkPipelineShaderStageCreateInfo shader_stages[2] = {vert_shader_stage_info_, frag_shader_stage_info_};
            pipeline_info_.stageCount = 2;
            pipeline_info_.pStages = shader_stages;

            vertex_input_info_.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            
            VkPushConstantRange vpcr[1];
            vpcr[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
            vpcr[0].offset = 0;
            vpcr[0].size = sizeof(PushConstant);

            pipeline_layout_info_.pushConstantRangeCount = 1;
            pipeline_layout_info_.pPushConstantRanges = vpcr;

            auto binding_description = Vertex::GetBindingDescription();
            auto attributeDescriptions = Vertex::GetAttributeDescriptions();

            vertex_input_info_.vertexBindingDescriptionCount = 1;
            vertex_input_info_.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
            vertex_input_info_.pVertexBindingDescriptions = &binding_description;
            vertex_input_info_.pVertexAttributeDescriptions = attributeDescriptions.data();
        // --
        
        if (vkCreatePipelineLayout(creation_info.device, &pipeline_layout_info_, nullptr, &storage_location_.GetPipelineLayout(creation_info.name)) != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline layoutof name: " + creation_info.name);
        }

        pipeline_info_.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipeline_info_.pVertexInputState = &vertex_input_info_;
        pipeline_info_.pInputAssemblyState = &input_assembly_;
        pipeline_info_.pViewportState = &view_port_state_;
        pipeline_info_.pRasterizationState = &rasterizer_;
        pipeline_info_.pMultisampleState = &multisampling_;
        pipeline_info_.pDepthStencilState = &depth_stencil_;
        pipeline_info_.pColorBlendState = &color_blending_;
        pipeline_info_.layout = storage_location_.GetPipelineLayout(creation_info.name);
        pipeline_info_.renderPass = creation_info.render_pass;
        pipeline_info_.subpass = subpass_number_;

        if (vkCreateGraphicsPipelines(creation_info.device, VK_NULL_HANDLE, 1, &pipeline_info_, nullptr, &storage_location_.getPipeline(creation_info.name)) != VK_SUCCESS) {
            throw std::runtime_error("failed to create graphics pipeline of name: " + creation_info.name);
        }
    }
// --
}