#include <iterator>

#include "graphics/graphicsregister.h"
#include "application/console.h"
namespace graphics {

// -- Graphics Manager

    bool GraphicsRegister::HandleDeletion() {
        for (auto& graphic_name : graphics_to_be_deleted_) {
            if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
                auto& graphic = graphics_data_.at(graphic_name);
                vertex_list_.erase(vertex_list_.begin() + graphic.vertex_offset_,vertex_list_.begin() + graphic.vertex_offset_ + graphic.vertex_count_);
                index_list_.erase(index_list_.begin() + graphic.index_offset_, index_list_.begin() + graphic.index_offset_ + graphic.index_count_);
                load_order_.erase(graphic_name);
            } else {
                return false;
            }
            graphics_data_.erase(graphic_name);
        }
        graphics_to_be_deleted_.clear();
        return true;
    }

    bool GraphicsRegister::RegisterGraphic(std::string new_graphic_name) {
        // Graphic doesn't exist
        if (graphics_data_.find(new_graphic_name) == graphics_data_.end()) {
            graphics_data_.emplace(new_graphic_name, Graphic(*this));
            load_order_.emplace(new_graphic_name);
            has_changed_ = true;
            return true;
        }
        // Graphic exists
        return false;
    }

    bool GraphicsRegister::DeleteGraphic(std::string graphic_name) {
        if (graphics_data_.find(graphic_name) == graphics_data_.end()) { return true; }
        graphics_to_be_deleted_.emplace_back(graphic_name);
        return true;
    }

    bool GraphicsRegister::EndOfFrame() {
        return true;
    }

    bool GraphicsRegister::AdvanceFrame(){ 
        if (this->has_changed_) {
            if (!HandleDeletion()) { return false;}
            std::size_t current_index_offset = default_index_count_;
            std::size_t current_vertex_offset = default_vertex_count_;
            for (const  auto& graphic_name : load_order_) {
                // An entry in load_order_ is equivalent to existence of graphic with name of entry
                auto& related_graphic = graphics_data_.at(graphic_name);
                if (related_graphic.has_changed_) {
                    std::size_t new_vertex_difference = current_vertex_offset - related_graphic.vertex_offset_;
                    // Set all the indices to the new values required due to vertex offset changing
                    // Will be aligned into index list due to current_index_offset being used
                    for (std::size_t relative_index_number = current_index_offset; relative_index_number < related_graphic.index_count_ + current_index_offset; relative_index_number++ ) {
                        index_list_[relative_index_number] += new_vertex_difference;
                    }
                    // Set the new indices to the right vertex offset
                    for (auto& index : related_graphic.new_indices_) {
                        index += current_vertex_offset;
                    }
                    // Move the new vertices and indices into the main array
                    vertex_list_.insert(std::prev(vertex_list_.end(), vertex_list_.size() - (current_vertex_offset + related_graphic.vertex_count_)),
                                        std::make_move_iterator(related_graphic.new_vertices_.begin()), 
                                        std::make_move_iterator(related_graphic.new_vertices_.end()));
                    index_list_.insert(std::prev(index_list_.end(), index_list_.size() - (current_index_offset + related_graphic.index_count_)),
                                       std::make_move_iterator(related_graphic.new_indices_.begin()), 
                                       std::make_move_iterator(related_graphic.new_indices_.end()));
                    // Still a valid call as move leaves unspecified but valid members in the previous vectors
                    related_graphic.vertex_count_ += related_graphic.new_vertices_.size();
                    related_graphic.index_count_ += related_graphic.new_indices_.size();
                    // Clean up the old vectors
                    related_graphic.AdvanceFrame();
                }
                related_graphic.index_offset_ = current_index_offset;
                related_graphic.vertex_offset_ = current_vertex_offset;
                current_index_offset += related_graphic.index_count_;
                current_vertex_offset += related_graphic.vertex_count_;
            }
            indices_changed_ = true;
            vertices_changed_ = true;
        }
        return true;
    }

    std::vector<Vertex>& GraphicsRegister::get_vertices() {
        vertices_changed_ = false;
        return vertex_list_;
    }


    std::vector<uint32_t>& GraphicsRegister::get_indices() {
        indices_changed_ = false;
        return index_list_;
    }

    std::vector<std::reference_wrapper<const Vertex>> GraphicsRegister::GetGraphicsVertices(std::string graphic_name) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            std::vector<std::reference_wrapper<const Vertex>> return_list;
            auto& graphic = graphics_data_.at(graphic_name); // Exists due to previous check
            for (auto i = graphic.vertex_offset_; i < graphic.vertex_offset_ + graphic.vertex_count_; i++) {
                return_list.emplace_back(vertex_list_[i]);
            }
            return return_list;
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::GetGraphicsVertices(std::string)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return {};
        }
    }

    std::vector<uint32_t> GraphicsRegister::GetGraphicsIndices(std::string graphic_name) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            std::vector<uint32_t> return_list;
            auto& graphic = graphics_data_.at(graphic_name); // Exists due to previous check
            for (auto i = graphic.index_offset_; i < graphic.index_offset_ + graphic.index_count_; i++) {
                return_list.emplace_back(index_list_[i]);
            }
            return return_list;
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::GetGraphicsIndices(std::string)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return {};
        }
    }

    bool GraphicsRegister::AddVertexToGraphic(std::string graphic_name, Vertex new_vertex, bool with_index) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
             auto& graphic = graphics_data_.at(graphic_name); // Exists due to previous check
            return graphic.AddNewVertex(new_vertex, with_index);
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::AddVertexToGraphic(std::string, Vertex, bool)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return false;
        }
    }

    bool GraphicsRegister::AddIndexToGraphic(std::string graphic_name, uint32_t new_index, bool bounds_checking) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            auto& graphic = graphics_data_.at(graphic_name); // Exists due to previous check
            return graphic.AddNewIndex(new_index, bounds_checking);
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::AddIndexToGraphic(std::string, uint32_t, bool)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return false;
        }
    }

    bool GraphicsRegister::HasVertex(std::string graphic_name, const Vertex& wanted_vertex) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            for (uint32_t i = graphics_data_.at(graphic_name).vertex_offset_; i < graphics_data_.at(graphic_name).vertex_offset_ + graphics_data_.at(graphic_name).vertex_count_; i++) {
                if (vertex_list_[i] == wanted_vertex) { return true; }
            }
            return false;
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::HasVertex(std::string, const Vertex&)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return false;
        }
    }

    bool GraphicsRegister::WillHaveVertex(std::string graphic_name, const Vertex& wanted_vertex) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            return (graphics_data_.at(graphic_name).WillHaveVertex(wanted_vertex));// Exists due to previous check
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::WillHaveVertex(std::string, const Vertex&)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return false;
        }
    }

    bool GraphicsRegister::HasIndex(std::string graphic_name, uint32_t wanted_index) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            for (uint32_t i = graphics_data_.at(graphic_name).index_offset_; i < graphics_data_.at(graphic_name).index_offset_ + graphics_data_.at(graphic_name).index_count_; i++) {
                if (index_list_[i] == wanted_index) { return true; }
            }
            return false;
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::HasIndex(std::string, uin32_t)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return false;
        }
    }

    bool GraphicsRegister::WillHaveIndex(std::string graphic_name, uint32_t wanted_index) {
        if (graphics_data_.find(graphic_name) != graphics_data_.end()) {
            return ( graphics_data_.at(graphic_name).WillHaveIndex(wanted_index)); // Exists due to previous check
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::WillHaveIndex(std::string, uin32_t)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return false;
        }
    }

    uint32_t GraphicsRegister::GraphicIndexOffset(std::string graphic_name) {
        if (HasGraphic(graphic_name)) {
            return graphics_data_.at(graphic_name).index_offset_;
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::GraphicsIndexOffset(std::string)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return -1;
        }
    }

    uint32_t GraphicsRegister::GraphicIndexCount(std::string graphic_name) {
        if (HasGraphic(graphic_name)) {
            return graphics_data_.at(graphic_name).index_count_;
        } else {
            related_application_.main_console().IssueWarning(
                "graphics::GraphicsRegistrar::GraphicsRegister::GraphicsIndexCount(std::string)", 
                "Invalid Name",
                fmt::format("Graphic doesn't exist with name {}", graphic_name)
            );
            return -1;
        }
    }

// -- End of Graphics Manager

// -- Graphic
    bool GraphicsRegister::Graphic::AdvanceFrame() {
        if (has_changed_) {
            new_vertices_.clear();
            new_indices_.clear();
            has_changed_ = false;
            return true;
        }
        return false;
    }

    bool GraphicsRegister::Graphic::AddNewVertex(Vertex new_vertex, bool with_index) {
        if (with_index) { new_indices_.emplace_back(new_vertices_.size() + vertex_count_); }
        new_vertices_.emplace_back(new_vertex);
        parent_register_.set_has_changed(true);
        has_changed_ = true;
        return true;
    }

    bool GraphicsRegister::Graphic::AddNewIndex(uint32_t new_index, bool bounds_checking) {
        if (bounds_checking) { 
            if (new_index >= vertex_count_) {
                parent_register_.related_application_.main_console().IssueWarning(
                    "graphics::GraphicsRegistrar::GraphicsRegister::Graphic::AddNewIndex(uint32_t, bool)", 
                    "Invalid Index",
                    fmt::format("Index {} is out of bounds.", new_index)
                );
                return false; 
            } 
        }
        new_indices_.emplace_back(new_index);
        parent_register_.set_has_changed(true);
        has_changed_ = true;
        return true;
    }

    bool GraphicsRegister::Graphic::WillHaveIndex(uint32_t wanted_index) {
        for (auto& new_index : new_indices_) {
            if (new_index == wanted_index) { return true; }
        }
        return false;
    }

    bool GraphicsRegister::Graphic::WillHaveVertex(const Vertex& wanted_vertex) {
        for (auto& new_vertex : new_vertices_) {
            if (new_vertex == wanted_vertex) { return true; } 
        }
        return false;
    }
// -- End of Graphic
}