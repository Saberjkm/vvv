
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include "graphics/vmadef.h"
#include "vma/vk_mem_alloc.h"
#pragma GCC diagnostic pop


#include "graphics/graphicsregistrar.h"

#include "application/file.h"


namespace graphics {

//-- Namespace level
    VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallBack(
        VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
        VkDebugUtilsMessageTypeFlagsEXT message_type,
        const VkDebugUtilsMessengerCallbackDataEXT* p_callback_data,
        void* p_user_data) {
            if (message_severity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
                // Message is important enough to show
                std::cerr << "validation layer: " << p_callback_data->pMessage << std::endl;
            }
            return VK_FALSE;
        }


    VkResult CreateDebugUtilsMessengerEXT(VkInstance instance,
                                        const VkDebugUtilsMessengerCreateInfoEXT* p_create_info,
                                        const VkAllocationCallbacks* p_allocator,
                                        VkDebugUtilsMessengerEXT* p_debug_messenger) 
    {
        auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
        if (func != nullptr) {
            return func(instance, p_create_info, p_allocator, p_debug_messenger);
        } else {
            return VK_ERROR_EXTENSION_NOT_PRESENT;
        }
    }

    void DestroyDebugUtilsMessengerEXT(
        VkInstance instance,
        VkDebugUtilsMessengerEXT debug_messenger, 
        const VkAllocationCallbacks* p_allocator) {
        auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
        if (func != nullptr) {
            func(instance, debug_messenger, p_allocator);
        }
    }

    void FramebufferResizeCallback(GLFWwindow* window, int width, int height) {
        auto app = reinterpret_cast<GraphicsRegistrar*>(glfwGetWindowUserPointer(window));
        app->framebuffer_resized_ = true;
    }

    VkBufferCreateInfo CreateBufferInfo(VkDeviceSize size,
                                        VkBufferUsageFlags usage,
                                        VkSharingMode sharing_mode = VK_SHARING_MODE_EXCLUSIVE,
                                        const void* p_next = nullptr, 
                                        VkBufferCreateFlags flags = 0, 
                                        uint32_t queue_family_index_count = 0,
                                        const uint32_t* p_queue_family_indices = nullptr)
    {
        VkBufferCreateInfo buffer_info{};
        buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.pNext = p_next;
        buffer_info.flags = flags;
        buffer_info.size = size;
        buffer_info.usage = usage;
        buffer_info.sharingMode = sharing_mode;
        buffer_info.queueFamilyIndexCount = queue_family_index_count;
        buffer_info.pQueueFamilyIndices  = p_queue_family_indices;
        return buffer_info;
    }

    VmaAllocationCreateInfo CreateAllocationInfo(VmaAllocationCreateFlags create_flags,
                                                VmaMemoryUsage usage,
                                                VkMemoryPropertyFlags required_flags,
                                                VkMemoryPropertyFlags preferred_flags = 0,
                                                uint32_t memory_type_bits = 0, 
                                                VmaPool pool = nullptr, 
                                                void* p_user_data = nullptr)
    {
        VmaAllocationCreateInfo alloc_info{};
        alloc_info.flags = create_flags;
        alloc_info.usage = usage;
        alloc_info.requiredFlags = required_flags;
        alloc_info.preferredFlags = preferred_flags;
        alloc_info.memoryTypeBits = memory_type_bits;
        alloc_info.pool = pool;
        alloc_info.pUserData = p_user_data;
        return alloc_info;
    }

    VkImageCreateInfo CreateImageInfo(VkImageType image_type,
                                    VkFormat format,
                                    VkExtent3D extent,
                                    VkImageUsageFlags usage,
                                    VkImageTiling tiling,
                                    VkImageLayout initial_layout = VK_IMAGE_LAYOUT_UNDEFINED,
                                    const void* p_next = nullptr,
                                    VkImageCreateFlags flags = 0,
                                    uint32_t mip_levels = 1,
                                    uint32_t array_layers = 1,
                                    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT,
                                    VkSharingMode sharing_mode = VK_SHARING_MODE_EXCLUSIVE,
                                    uint32_t queue_family_index_count = 0,
                                    const uint32_t* p_queue_family_indices = 0) 
    {
        return 
        {
            VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
            p_next, flags, image_type, format, extent, mip_levels, array_layers,
            samples, tiling, usage, sharing_mode, queue_family_index_count, p_queue_family_indices, initial_layout
        };
    }

    VkImageViewCreateInfo CreateImageViewInfo(VkImage image,
                                            VkFormat format,
                                            VkImageSubresourceRange subresource_range,
                                            VkImageViewType view_type = VK_IMAGE_VIEW_TYPE_2D,
                                            const void* p_next = nullptr,
                                            VkImageViewCreateFlags flags = 0,
                                            VkComponentMapping components = VkComponentMapping())
    {
        VkImageViewCreateInfo view_info{};
        view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        view_info.image = image;
        view_info.viewType = view_type;
        view_info.format = format;
        view_info.flags = flags;
        view_info.subresourceRange = subresource_range;
        view_info.components = components;
        return view_info;
    }

    void CreateBuffer(VmaAllocator allocator, 
                        VkBufferCreateInfo& buffer_info, 
                        VmaAllocationCreateInfo& allocation_info, 
                        VkBuffer& buffer_out,
                        VmaAllocation& allocation_out) 
    {
        VkResult result = vmaCreateBuffer(allocator, &buffer_info, &allocation_info, &buffer_out, &allocation_out, nullptr);
        if (result != VK_SUCCESS) {
            throw std::runtime_error("Failed to create buffer! Error: " + std::to_string(result));
        }
    }

    void CreateImage(VmaAllocator allocator, 
                     VkImageCreateInfo& image_info, 
                     VmaAllocationCreateInfo& allocation_info, 
                     VkImage& image_out,
                     VmaAllocation& allocation_out) 
    {
        if (vmaCreateImage(allocator, &image_info, &allocation_info, &image_out, &allocation_out, nullptr) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create image!");
        }
    }

    bool HasStencilComponent(VkFormat format) {
        return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
    }

//--

//-- Graphics Registrar

    bool GraphicsRegistrar::EndOfFrame() {
        graphics_register_.EndOfFrame();
        return true;
    }

    bool GraphicsRegistrar::AdvanceFrame() {
        if (graphics_register_.AdvanceFrame()) {
            // Either the vertices or indices have changed
            if (graphics_register_.needs_indices_loaded()) {
                vkWaitForFences(device_, in_flight_fences.size(), in_flight_fences.data(), VK_TRUE, UINT64_MAX);
                DestroyBuffer(vertex_buffer_, vertex_buffer_memory_);
                CreateVertexBuffer(vertex_buffer_, vertex_buffer_memory_, {graphics_register_.get_vertices().data(), 3});
            }
            if (graphics_register_.needs_vertices_loaded()) {
                vkWaitForFences(device_, in_flight_fences.size(), in_flight_fences.data(), VK_TRUE, UINT64_MAX);
                DestroyBuffer(index_buffer_, index_buffer_memory_);
                CreateIndexBuffer(index_buffer_, index_buffer_memory_, graphics_register_.get_indices());
            }
        }
        return true;
    }

    void GraphicsRegistrar::DrawFrame() {
        vkWaitForFences(device_, 1, &in_flight_fences[current_frame_], VK_TRUE, UINT64_MAX);

        uint32_t image_index;
        VkResult result = vkAcquireNextImageKHR(device_, swap_chain_, UINT64_MAX, image_avaliable_semaphores_[current_frame_], VK_NULL_HANDLE, &image_index);

        if (result == VK_ERROR_OUT_OF_DATE_KHR) {
            RecreateSwapChain();
            return;
        } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
            throw std::runtime_error("failed to acquire swap chain image!");
        }

        UpdateUniformBuffer(current_frame_);

        vkResetFences(device_, 1, &in_flight_fences[current_frame_]);

        if (!render_object_index_[current_frame_] == related_logic_registrar_.render_objects_index()) {
            vkResetCommandBuffer(command_buffers_[current_frame_], 0);
            RecordCommandBuffer(current_frame_);
        }

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

        VkSemaphore wait_semaphores[] = {image_avaliable_semaphores_[current_frame_]};
        VkPipelineStageFlags wait_stages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
        
        submit_info.waitSemaphoreCount = 1;
        submit_info.pWaitSemaphores = wait_semaphores;
        submit_info.pWaitDstStageMask = wait_stages;

        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffers_[current_frame_];

        VkSemaphore signalSemaphores[] = {render_finished_semaphores_[current_frame_]};
        submit_info.signalSemaphoreCount = 1;
        submit_info.pSignalSemaphores = signalSemaphores;

        if (vkQueueSubmit(graphics_queue_, 1, &submit_info, in_flight_fences[current_frame_]) != VK_SUCCESS) {
            throw std::runtime_error("failed to submit draw command buffer!");
        }

        VkPresentInfoKHR present_info{};
        present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores = signalSemaphores;

        VkSwapchainKHR swapChains[] = {swap_chain_};
        present_info.swapchainCount = 1;
        present_info.pSwapchains = swapChains;

        present_info.pImageIndices = &image_index;

        result = vkQueuePresentKHR(present_queue_, &present_info);

        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebuffer_resized_) {
            framebuffer_resized_ = false;
            RecreateSwapChain();
        } else if (result != VK_SUCCESS) {
            throw std::runtime_error("failed to present swap chain image!");
        }

        current_frame_ = (current_frame_ + 1) % kMaxFramesInFlight;
    }

    void GraphicsRegistrar::UpdateUniformBuffer(uint32_t current_image) {
        //static auto startTime = std::chrono::high_resolution_clock::now();

        //auto currentTime = std::chrono::high_resolution_clock::now();
        //float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

        UniformBufferObject ubo{};
        //ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        //ubo.model = glm::identity<glm::mat4>();
        //ubo.view = glm::lookAt(glm::vec3(15.0f, 25.0f, 15.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0, 0, 1.0f));
        ubo.view = glm::mat4(1.0f);
        //ubo.proj = glm::perspective(glm::radians(45.0f), (float) swapChainExtent.width / (float) swapChainExtent.height, 0.1f, 100.0f);
        ubo.proj = glm::mat4(1.0f);
        //ubo.proj[1][1] *= -1; // A reminder for OpenGl -> Vulkan 

        void* data;
        VmaAllocator typedAllocator = (VmaAllocator) allocator_;
        VmaAllocation typedUniformBufferMemory = (VmaAllocation) uniform_buffers_memory_[current_image];
        vmaMapMemory(typedAllocator, typedUniformBufferMemory, &data);
        memcpy(data, &ubo, sizeof(ubo));
        vmaUnmapMemory(typedAllocator, typedUniformBufferMemory);
        uniform_buffers_memory_[current_image] = typedUniformBufferMemory;
    }

    void GraphicsRegistrar::Initialise() { 
        InitialiseWindow();
        InitialiseVulkan();
    }

    void GraphicsRegistrar::FirstFrame() {}

    void GraphicsRegistrar::InitialiseVulkan() {
        CreateInstance();
        SetupDebugMessenger();
        CreateSurface();
        PickPhysicalDevice();
        CreateLogicalDevice(); 
        CreateMemoryAllocator();
        CreateSwapChain();
        CreateImageViews();
        CreateRenderPasses();
        CreateDescriptorSetLayout();
        CreateGraphicsPipelines();
        CreateDepthResources();
        CreateFramebuffers();
        CreateCommandPools();
        CreateVertexBuffer(vertex_buffer_, vertex_buffer_memory_, {graphics_register_.get_vertices().data(), 3});
        CreateIndexBuffer(index_buffer_, index_buffer_memory_, graphics_register_.get_indices());
        CreateUniformBuffers();
        CreateDescriptorPool();
        CreateDescriptorSets();
        main_renderer_->Initialise({"main"}, kMaxFramesInFlight);
        CreateCommandBuffers();
        CreateSyncObjects();
    }

    void GraphicsRegistrar::InitialiseWindow() {
        glfwInit();

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        window_ = glfwCreateWindow(kScreenWidth, kScreenHeight, "VVV", nullptr, nullptr);
        glfwSetWindowUserPointer(window_, this);
        glfwSetFramebufferSizeCallback(window_, FramebufferResizeCallback);
    }

    void GraphicsRegistrar::DestroyBuffer(VkBuffer buffer, void* buffer_memory) {
        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        VmaAllocation typed_buffer_memory = (VmaAllocation) buffer_memory;
        vmaDestroyBuffer(typed_allocator, buffer, typed_buffer_memory);
    }

    void GraphicsRegistrar::CleanUp(){
        CleanupSwapChain();
        
        vkDestroyDescriptorSetLayout(device_, descriptor_set_layout_, nullptr);

        DestroyBuffer(vertex_buffer_, vertex_buffer_memory_);
        DestroyBuffer(index_buffer_, index_buffer_memory_);

        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        vmaDestroyAllocator(typed_allocator);

        for (size_t i = 0; i < kMaxFramesInFlight; i++) {
            vkDestroySemaphore(device_, render_finished_semaphores_[i], nullptr);
            vkDestroySemaphore(device_, image_avaliable_semaphores_[i], nullptr);
            vkDestroyFence(device_, in_flight_fences[i], nullptr);
        }

        vkDestroyCommandPool(device_, command_pool_, nullptr);
        vkDestroyCommandPool(device_, transient_command_pool_, nullptr);

        main_renderer_->CleanUp();

        vkDestroyDevice(device_, nullptr);

        if (enableValidationLayers) {
            DestroyDebugUtilsMessengerEXT(instance_, debug_messenger_, nullptr);
        }

        vkDestroySurfaceKHR(instance_, surface_, nullptr);
        vkDestroyInstance(instance_, nullptr);

        glfwDestroyWindow(window_);

        glfwTerminate();
    }

    void GraphicsRegistrar::CleanupSwapChain() {
        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        VmaAllocation typed_depth_memory = (VmaAllocation) depth_image_memory_;
        vkDestroyImageView(device_, depth_image_view_, nullptr);
        vmaDestroyImage(typed_allocator, depth_image_, typed_depth_memory);

        for (auto frame_buffer : swap_chain_frame_buffers_) {
            vkDestroyFramebuffer(device_, frame_buffer, nullptr);
        }

        vkFreeCommandBuffers(device_, command_pool_, static_cast<uint32_t>(command_buffers_.size()), command_buffers_.data());

        pipeline_data_.CleanUp(device_, nullptr);
        for (auto render_pass : render_pass_data_.renderpass) {
            if (render_pass) vkDestroyRenderPass(device_, render_pass, nullptr);
        }

        for (auto image_view : swap_chain_image_views_) {
            vkDestroyImageView(device_, image_view, nullptr);
        }

        vkDestroySwapchainKHR(device_, swap_chain_, nullptr);

        //-- VMA Deleteion
        VmaAllocation typed_uniform_buffer_memory;
        for (size_t i = 0; i < uniform_buffers_.size(); i++) {
            typed_uniform_buffer_memory = (VmaAllocation) uniform_buffers_memory_[i];
            vmaDestroyBuffer(typed_allocator, uniform_buffers_[i], typed_uniform_buffer_memory);
        }
        //--
        vkDestroyDescriptorPool(device_, descriptor_pool_, nullptr);
    }

    bool GraphicsRegistrar::CheckValidationLayerSupport() {
        uint32_t layer_count;
        vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

        std::vector<VkLayerProperties> avaliable_layers(layer_count);
        vkEnumerateInstanceLayerProperties(&layer_count, avaliable_layers.data());
        // Check validation layers exist
        for (const char* layer_name : validation_layers_) {
            bool layer_found = false;
            for (const auto& layer_properties : avaliable_layers) {
                if (strcmp(layer_name, layer_properties.layerName) == 0) {
                    layer_found = true;
                    break;
                }
            }
            if (!layer_found) {
                return false;
            }
        }
        return true;
    }

    std::vector<const char*> GraphicsRegistrar::GetRequiredExtensions() {
        uint32_t glfw_extension_count = 0;
        const char** glfw_extensions;
        glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

        std::vector<const char*> extensions(glfw_extensions, glfw_extensions + glfw_extension_count);

        if (enableValidationLayers) {
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        return extensions;
    }

    void GraphicsRegistrar::CreateInstance() {
        if (enableValidationLayers && !CheckValidationLayerSupport()) {
            throw std::runtime_error("Validation layers requested, but not available!");
        }

        VkApplicationInfo app_info = {};
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pApplicationName = "VVV";
        app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.pEngineName = "No Engine";
        app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.apiVersion = VK_API_VERSION_1_3;

        VkInstanceCreateInfo create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        create_info.pApplicationInfo = &app_info;

        auto extensions = GetRequiredExtensions();
        create_info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        create_info.ppEnabledExtensionNames = extensions.data();

        // Creation info for the create/destroy instance debug messenger
        VkDebugUtilsMessengerCreateInfoEXT debug_create_info;
        VkValidationFeaturesEXT valFeats = {};
        VkValidationFeatureEnableEXT features_wanted[] = {VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT};

        if (enableValidationLayers) {
            PopulateDebugMessengerCreateInfo(debug_create_info);
            create_info.pNext = (VkDebugUtilsMessengerCreateInfoEXT*) &debug_create_info;

            create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers_.size());
            create_info.ppEnabledLayerNames = validation_layers_.data();
            
            valFeats.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
            valFeats.enabledValidationFeatureCount = 1;
            valFeats.pEnabledValidationFeatures = features_wanted;
            valFeats.disabledValidationFeatureCount = 0;
            valFeats.pDisabledValidationFeatures = nullptr;
            valFeats.pNext = create_info.pNext;

            //create_info.pNext = &valFeats;
        } else {
            create_info.enabledLayerCount = 0;
            create_info.pNext = 0;
        }

        if (vkCreateInstance(&create_info, nullptr, &instance_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create vulkan instance!");
        }
        
    }

    void GraphicsRegistrar::PickPhysicalDevice() {
        uint32_t device_count = 0;
        vkEnumeratePhysicalDevices(instance_, &device_count, nullptr);

        if (device_count == 0) {
            throw std::runtime_error("Failed to find GPUs with Vulkan support!");
        }

        std::vector<VkPhysicalDevice> devices(device_count);
        vkEnumeratePhysicalDevices(instance_, &device_count, devices.data());

        // Checks for a *single* device that is suitable and sets it as the member
        for (const auto& device : devices) {
            if (IsDeviceSuitable(device)) {
                physical_device_ = device;
                break;
            }
        }

        if (physical_device_ == VK_NULL_HANDLE) {
            throw std::runtime_error("Failed to find a suitable GPU!");
        }
    }

    bool GraphicsRegistrar::CheckDeviceExtensionSupport(VkPhysicalDevice device) {
        uint32_t extension_count;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, nullptr);

        std::vector<VkExtensionProperties> available_extensions(extension_count);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, available_extensions.data());

        std::set<std::string> required_extensions(device_extensions_.begin(), device_extensions_.end());

        for (const auto& extension : available_extensions) {
            required_extensions.erase(extension.extensionName);
        }

        return required_extensions.empty();
    }

    bool GraphicsRegistrar::IsDeviceSuitable(VkPhysicalDevice device) {
        QueueFamilyIndices indices = FindQueueFamilies(device);

        bool extensions_supported = CheckDeviceExtensionSupport(device);

        // Check if the swap chain is adequate (and supported by extension)
        bool swap_chain_adequate = false;
        if (extensions_supported) {
            SwapChainSupportDetails swap_chain_support = QuerySwapChainSupport(device);
            swap_chain_adequate = !swap_chain_support.formats.empty() && !swap_chain_support.present_modes.empty();
        }

        return indices.IsComplete() && extensions_supported && swap_chain_adequate;
    }

    SwapChainSupportDetails GraphicsRegistrar::QuerySwapChainSupport(VkPhysicalDevice device) {
        SwapChainSupportDetails details;

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface_, &details.capabilities); // Check for the surface capabilities

        // Check for surface formats
        uint32_t format_count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, nullptr);
        if (format_count != 0) {
            details.formats.resize(format_count);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, details.formats.data());
        }

        // Check for presentation modes
        uint32_t present_mode_count;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count, nullptr);
        if (present_mode_count != 0) {
            details.present_modes.resize(present_mode_count);
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count, details.present_modes.data());
        }

        return details;
    }

    VkSurfaceFormatKHR GraphicsRegistrar::ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& available_formats) {
        for (const auto& available_format : available_formats) {
            if (available_format.format == VK_FORMAT_B8G8R8A8_SRGB && available_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return available_format;
            }
        }
        return available_formats[0];
    }

    VkPresentModeKHR GraphicsRegistrar::ChooseSwapPresentMode(const std::vector<VkPresentModeKHR>& available_present_modes) {
        for (const auto& available_present_mode : available_present_modes) {
            if (available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return available_present_mode;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    VkExtent2D GraphicsRegistrar::ChooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
        if (capabilities.currentExtent.width != UINT32_MAX) {
            return capabilities.currentExtent;
        } else {
            int width, height;
            glfwGetFramebufferSize(window_, &width, &height);

            VkExtent2D actual_extent = {
                static_cast<uint32_t>(width),
                static_cast<uint32_t>(height)
            };

            actual_extent.width = std::clamp(actual_extent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
            actual_extent.height = std::clamp(actual_extent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

            return actual_extent;
        }
    }

    void GraphicsRegistrar::CreateSwapChain() {
        SwapChainSupportDetails swap_chain_support = QuerySwapChainSupport(physical_device_);

        VkSurfaceFormatKHR surface_format = ChooseSwapSurfaceFormat(swap_chain_support.formats);
        VkPresentModeKHR present_mode = ChooseSwapPresentMode(swap_chain_support.present_modes);
        VkExtent2D extent = ChooseSwapExtent(swap_chain_support.capabilities);

        uint32_t image_count = swap_chain_support.capabilities.minImageCount;
        if (swap_chain_support.capabilities.maxImageCount > 0 && image_count > swap_chain_support.capabilities.maxImageCount) {
            image_count = swap_chain_support.capabilities.maxImageCount;
        }

        VkSwapchainCreateInfoKHR create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        create_info.surface = surface_;

        create_info.minImageCount = image_count;
        create_info.imageFormat = surface_format.format;
        create_info.imageColorSpace = surface_format.colorSpace;
        create_info.imageExtent = extent;
        create_info.imageArrayLayers = 1;
        create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;

        QueueFamilyIndices indices = FindQueueFamilies(physical_device_);
        uint32_t queue_family_indices[] = {indices.graphics_family.value(), indices.present_family.value()};

        if (indices.graphics_family != indices.present_family) {
            create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            create_info.queueFamilyIndexCount = 2;
            create_info.pQueueFamilyIndices = queue_family_indices;
        } else {
            create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }

        create_info.preTransform = swap_chain_support.capabilities.currentTransform;
        create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        create_info.presentMode = present_mode;
        create_info.clipped = VK_TRUE;

        if (vkCreateSwapchainKHR(device_, &create_info, nullptr, &swap_chain_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create swap chain!");
        }

        vkGetSwapchainImagesKHR(device_, swap_chain_, &image_count, nullptr);
        swap_chain_images_.resize(image_count);
        vkGetSwapchainImagesKHR(device_, swap_chain_, &image_count, swap_chain_images_.data());

        swap_chain_image_format_ = surface_format.format;
        swap_chain_extent_ = extent;
    }

    void GraphicsRegistrar::RecreateSwapChain() {
        int width = 0, height = 0;
        glfwGetFramebufferSize(window_, &width, &height);
        while (width == 0 || height == 0) {
            glfwGetFramebufferSize(window_, &width, &height);
            glfwWaitEvents();
        }

        vkDeviceWaitIdle(device_);

        CleanupSwapChain();
        main_renderer_->CleanUp(); 

        CreateSwapChain();
        main_renderer_->Initialise({"main"}, kMaxFramesInFlight);

        CreateImageViews();
        CreateRenderPasses();
        CreateGraphicsPipelines();
        CreateDepthResources();
        CreateFramebuffers();
        CreateUniformBuffers();
        CreateDescriptorPool();
        CreateDescriptorSets();
        CreateCommandBuffers();
        for (int i = 0; i < kMaxFramesInFlight; ++i) {
            RecordCommandBuffer(i);
        }
    }

    void GraphicsRegistrar::CreateImageViews() {
        swap_chain_image_views_.resize(swap_chain_images_.size());
        for (size_t i = 0; i < swap_chain_images_.size(); i++) {
            VkImageViewCreateInfo create_info{};
            create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            create_info.image = swap_chain_images_[i];
            create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
            create_info.format = swap_chain_image_format_;
            create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            create_info.subresourceRange.baseMipLevel = 0;
            create_info.subresourceRange.levelCount = 1;
            create_info.subresourceRange.baseArrayLayer = 0;
            create_info.subresourceRange.layerCount = 1;
            if (vkCreateImageView(device_, &create_info, nullptr, &swap_chain_image_views_[i]) != VK_SUCCESS) {
                throw std::runtime_error("failed to create image views!");
            }
        }
    }

    QueueFamilyIndices GraphicsRegistrar::FindQueueFamilies(VkPhysicalDevice device) {
        QueueFamilyIndices indices;

        uint32_t queue_family_count = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);

        std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());

        // Assigns the queue families gotten from device to ordered lists essentially
        for (std::size_t i = 0; i < queue_families.size(); i++) {
            const auto& queue_family = queue_families[i];
            if (queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                indices.graphics_family = i;
            }

            // Check if the presentation family is what we need
            VkBool32 present_support = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface_, &present_support);
            if (present_support) {
                indices.present_family = i;
            }

            // Found the families we want
            if (indices.IsComplete()) {
                break;
            }
        }
        return indices;
    }

    void GraphicsRegistrar::CreateLogicalDevice() {
        QueueFamilyIndices indices = FindQueueFamilies(physical_device_);

        std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
        std::set<uint32_t> unique_queue_families = {indices.graphics_family.value(), indices.present_family.value()};

        float queue_priority = 1.0f;
        for (uint32_t queueFamily : unique_queue_families) {
            VkDeviceQueueCreateInfo queue_create_info{};
            queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queue_create_info.queueFamilyIndex = queueFamily;
            queue_create_info.queueCount = 1;
            queue_create_info.pQueuePriorities = &queue_priority;
            queue_create_infos.push_back(queue_create_info);
        }

        VkPhysicalDeviceFeatures device_features{}; // All defaulted

        VkDeviceCreateInfo create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

        create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());
        create_info.pQueueCreateInfos = queue_create_infos.data();

        create_info.pEnabledFeatures = &device_features;

        create_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions_.size());
        create_info.ppEnabledExtensionNames = device_extensions_.data();

        if (enableValidationLayers) {
            create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers_.size());
            create_info.ppEnabledLayerNames = validation_layers_.data();
        } else {
            create_info.enabledLayerCount = 0;
        }

        if (vkCreateDevice(physical_device_, &create_info, nullptr, &device_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create logical device!");
        }

        vkGetDeviceQueue(device_, indices.graphics_family.value(), 0, &graphics_queue_);
        vkGetDeviceQueue(device_, indices.present_family.value(), 0, &present_queue_);
    }

    void GraphicsRegistrar::CreateRenderPasses() {
        render_pass_data_.renderpass.resize(1);
        CreateMainRenderPass(0);
    }

    void GraphicsRegistrar::CreateMainRenderPass(uint32_t renderPassNumber) {
        VkRenderPassCreateInfo renderPassInfo{};

        VkAttachmentDescription color_attachment{};
        color_attachment.format = swap_chain_image_format_;
        color_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
        color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        color_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        color_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        color_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        color_attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        VkAttachmentDescription depth_attachment{};
        depth_attachment.format = FindDepthFormat();
        depth_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
        depth_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        depth_attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depth_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        depth_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        depth_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        depth_attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;


        VkAttachmentReference color_attachment_ref{};
        color_attachment_ref.attachment = 0;
        color_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference depth_attachment_ref{};
        depth_attachment_ref.attachment = 1;
        depth_attachment_ref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        std::array<VkSubpassDescription, 1> subpasses({});

        subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpasses[0].colorAttachmentCount = 1;
        subpasses[0].pColorAttachments = &color_attachment_ref;
        subpasses[0].pDepthStencilAttachment = &depth_attachment_ref;

        VkSubpassDependency dependency{};
        dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        dependency.dstSubpass = 0;
        dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dependency.srcAccessMask = 0;
        dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        
        std::array<VkAttachmentDescription, 2> attachments = {color_attachment, depth_attachment};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        renderPassInfo.pAttachments = attachments.data();
        renderPassInfo.subpassCount = subpasses.size();
        renderPassInfo.pSubpasses = subpasses.data();
        renderPassInfo.dependencyCount = 1;
        renderPassInfo.pDependencies = &dependency;

        if (vkCreateRenderPass(device_, &renderPassInfo, nullptr, &render_pass_data_.renderpass[renderPassNumber]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create render pass!");
        }
    }

    void GraphicsRegistrar::CreateGraphicsPipelines() {
        std::string vert_shader, frag_shader;
        vert_shader = "shaders/shadernormal.vert.spv";
        frag_shader = "shaders/shadernormal.frag.spv";

        PipelineCreator pipe_creator(pipeline_data_, swap_chain_extent_, descriptor_set_layout_);
        auto base_vert_shader_code = application::ReadFile(vert_shader);
        auto base_frag_shader_code = application::ReadFile(frag_shader);
        VkShaderModule vert_shader_module = CreateShaderModule(base_vert_shader_code);
        VkShaderModule frag_shader_module = CreateShaderModule(base_frag_shader_code);

        pipe_creator.InitialiseValues();

        // -- Create Pipeline
            pipe_creator.subpass_number_ = 0;
            pipe_creator.vert_shader_stage_info_.module = vert_shader_module;
            pipe_creator.vert_shader_stage_info_.pName = "main";
            pipe_creator.frag_shader_stage_info_.module = frag_shader_module;
            pipe_creator.frag_shader_stage_info_.pName = "main";

            pipe_creator.CreatePipeline({device_, render_pass_data_.renderpass[0], "main", &descriptor_set_layout_});
        // --

        vkDestroyShaderModule(device_, vert_shader_module, nullptr);
        vkDestroyShaderModule(device_, frag_shader_module, nullptr);
    }

    VkShaderModule GraphicsRegistrar::CreateShaderModule(const std::vector<char>& code) {
        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        VkShaderModule shaderModule;
        if (vkCreateShaderModule(device_, &createInfo, nullptr, &shaderModule) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create shader module!");
        }
        return shaderModule;
    }

    void GraphicsRegistrar::CreateDescriptorSetLayout() {
        VkDescriptorSetLayoutBinding ubo_layout_binding{};
        ubo_layout_binding.binding = 0;
        ubo_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        ubo_layout_binding.descriptorCount = 1;
        ubo_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        ubo_layout_binding.pImmutableSamplers = nullptr; // Optional

        VkDescriptorSetLayoutCreateInfo layout_info{};
        layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        layout_info.bindingCount = 1;
        layout_info.pBindings = &ubo_layout_binding;

        if (vkCreateDescriptorSetLayout(device_, &layout_info, nullptr, &descriptor_set_layout_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create descriptor set layout!");
        }
    }

    void GraphicsRegistrar::CreateFramebuffers() {
        swap_chain_frame_buffers_.resize(swap_chain_image_views_.size());
        for (size_t i = 0; i < swap_chain_image_views_.size(); i++) {
            std::array<VkImageView, 2> attachments = {
                swap_chain_image_views_[i],
                depth_image_view_
            };

            VkFramebufferCreateInfo frame_buffer_info{};
            frame_buffer_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            frame_buffer_info.renderPass = render_pass_data_.renderpass[0];
            frame_buffer_info.attachmentCount = static_cast<uint32_t>(attachments.size());
            frame_buffer_info.pAttachments = attachments.data();
            frame_buffer_info.width = swap_chain_extent_.width;
            frame_buffer_info.height = swap_chain_extent_.height;
            frame_buffer_info.layers = 1;


            if (vkCreateFramebuffer(device_, &frame_buffer_info, nullptr, &swap_chain_frame_buffers_[i]) != VK_SUCCESS) {
                throw std::runtime_error("Failed to create framebuffer!");
            }
        }
    }

    void GraphicsRegistrar::CreateDescriptorPool() {
        VkDescriptorPoolSize pool_size{};
        pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        pool_size.descriptorCount = static_cast<uint32_t>(kMaxFramesInFlight);

        VkDescriptorPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.poolSizeCount = 1;
        pool_info.pPoolSizes = &pool_size;

        pool_info.maxSets = static_cast<uint32_t>(kMaxFramesInFlight);

        if (vkCreateDescriptorPool(device_, &pool_info, nullptr, &descriptor_pool_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create descriptor pool!");
        }
    }

    void GraphicsRegistrar::CreateDescriptorSets() {
        std::vector<VkDescriptorSetLayout> layouts(kMaxFramesInFlight, descriptor_set_layout_);
        VkDescriptorSetAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        alloc_info.descriptorPool = descriptor_pool_;
        alloc_info.descriptorSetCount = static_cast<uint32_t>(kMaxFramesInFlight);
        alloc_info.pSetLayouts = layouts.data();

        descriptor_sets_.resize(kMaxFramesInFlight);
        if (vkAllocateDescriptorSets(device_, &alloc_info, descriptor_sets_.data()) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate descriptor sets!");
        }
        for (size_t i = 0; i < kMaxFramesInFlight; i++) {
            VkDescriptorBufferInfo buffer_info{};
            buffer_info.buffer = uniform_buffers_[i];
            buffer_info.offset = 0;
            buffer_info.range = sizeof(UniformBufferObject);

            VkWriteDescriptorSet descriptorWrite{};
            descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            descriptorWrite.dstSet = descriptor_sets_[i];
            descriptorWrite.dstBinding = 0;
            descriptorWrite.dstArrayElement = 0;
            descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
            descriptorWrite.descriptorCount = 1;
            descriptorWrite.pBufferInfo = &buffer_info;

            vkUpdateDescriptorSets(device_, 1, &descriptorWrite, 0, nullptr);
        }
    }

    void GraphicsRegistrar::CreateCommandPools() {
        queue_family_indices_ = FindQueueFamilies(physical_device_);

        VkCommandPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        pool_info.queueFamilyIndex = queue_family_indices_.graphics_family.value();
        pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

        if (vkCreateCommandPool(device_, &pool_info, nullptr, &command_pool_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create base command pool!");
        }

        pool_info.flags = pool_info.flags | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

        if (vkCreateCommandPool(device_, &pool_info, nullptr, &transient_command_pool_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create transient command pool!");
        }
    }

    void GraphicsRegistrar::CopyBuffer(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size) {
        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandPool = transient_command_pool_;
        alloc_info.commandBufferCount = 1;

        VkCommandBuffer command_buffer;
        vkAllocateCommandBuffers(device_, &alloc_info, &command_buffer);

        VkCommandBufferBeginInfo begin_info{};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(command_buffer, &begin_info);
            VkBufferCopy copy_region{};
            copy_region.srcOffset = 0; // Optional
            copy_region.dstOffset = 0; // Optional
            copy_region.size = size;
            vkCmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region);
        vkEndCommandBuffer(command_buffer);

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffer;

        vkQueueSubmit(graphics_queue_, 1, &submit_info, VK_NULL_HANDLE);
        vkQueueWaitIdle(graphics_queue_);

        vkFreeCommandBuffers(device_, transient_command_pool_, 1, &command_buffer);
    }

    void GraphicsRegistrar::CreateMemoryAllocator() {
        VmaAllocatorCreateInfo allocator_info = {};
        allocator_info.physicalDevice = physical_device_;
        allocator_info.device = device_;
        allocator_info.instance = instance_;

        VmaAllocator typed_allocator;
        if (vmaCreateAllocator(&allocator_info, &typed_allocator) != VK_SUCCESS) {
            throw std::runtime_error("Failed to create memory allocator");
        }
        
        // Update member variables
        allocator_ = typed_allocator;
    }

    void GraphicsRegistrar::CreateVertexBuffer(VkBuffer& buffer_location, void*& memory_location, std::pair<const Vertex*, std::size_t> vertices) {
        if (vertices.second == 0) return; 
        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        VmaAllocation typed_vertex_buffer_memory = (VmaAllocation) memory_location;

        VkDeviceSize buffer_size = sizeof(Vertex) * vertices.second;

        //--Create Staging Buffer
        VkBuffer staging_buffer;
        VmaAllocation staging_buffer_memory;

        VkBufferCreateInfo staging_buffer_info(CreateBufferInfo(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT));
        VmaAllocationCreateInfo staging_alloc_info (
            CreateAllocationInfo(VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, 
                                VMA_MEMORY_USAGE_CPU_ONLY,   
                                (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
        );
        CreateBuffer(typed_allocator, staging_buffer_info, staging_alloc_info, staging_buffer, staging_buffer_memory);

        void* data;
        vmaMapMemory(typed_allocator, staging_buffer_memory, &data);
        memcpy(data, vertices.first, buffer_size);
        vmaUnmapMemory(typed_allocator, staging_buffer_memory);
        //--

        //-- Create Vertex Buffer
        VkBufferCreateInfo vertex_buffer_info(CreateBufferInfo(buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT));
        VmaAllocationCreateInfo vertex_alloc_info (
            CreateAllocationInfo(VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, 
                                VMA_MEMORY_USAGE_GPU_ONLY,   
                                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        );
        CreateBuffer(typed_allocator, vertex_buffer_info, vertex_alloc_info, vertex_buffer_, typed_vertex_buffer_memory);
        CopyBuffer(staging_buffer, vertex_buffer_, buffer_size);
        vmaDestroyBuffer(typed_allocator, staging_buffer, staging_buffer_memory);
        memory_location = typed_vertex_buffer_memory;
        //--
    }

    void GraphicsRegistrar::CreateIndexBuffer(VkBuffer& buffer_location, void*& memory_location, const std::vector<uint32_t>& indices) {
        if (indices.size() == 0) return;
        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        VmaAllocation typed_index_buffer_memory = (VmaAllocation) memory_location;

        VkDeviceSize buffer_size = sizeof(indices[0]) * indices.size();

        //--Create Staging Buffer
        VkBuffer staging_buffer;
        VmaAllocation staging_buffer_memory;

        VkBufferCreateInfo staging_buffer_info(CreateBufferInfo(buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT));
        VmaAllocationCreateInfo staging_alloc_info (
            CreateAllocationInfo(VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, 
                                VMA_MEMORY_USAGE_CPU_ONLY,   
                                (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT))
        );
        CreateBuffer(typed_allocator, staging_buffer_info, staging_alloc_info, staging_buffer, staging_buffer_memory);

        void* data;
        vmaMapMemory(typed_allocator, staging_buffer_memory, &data);
        memcpy(data, indices.data(), buffer_size);
        vmaUnmapMemory(typed_allocator, staging_buffer_memory);
        //--

        //-- Create Index
        VkBufferCreateInfo index_buffer_info(CreateBufferInfo(buffer_size, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT));
        VmaAllocationCreateInfo index_alloc_info (
            CreateAllocationInfo(VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, 
                                VMA_MEMORY_USAGE_GPU_ONLY,   
                                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        );
        CreateBuffer(typed_allocator, index_buffer_info, index_alloc_info, buffer_location, typed_index_buffer_memory);
        CopyBuffer(staging_buffer, buffer_location, buffer_size);
        vmaDestroyBuffer(typed_allocator, staging_buffer, staging_buffer_memory);
        memory_location = typed_index_buffer_memory;
        //--
    }

    void GraphicsRegistrar::CreateUniformBuffer(VkBuffer& buffer_location, void*& memory_location){
        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        VmaAllocation typed_uniform_buffer_memory = (VmaAllocation) memory_location;

        VkDeviceSize buffer_size = sizeof(UniformBufferObject);

        VkBufferCreateInfo uniform_buffer_info(CreateBufferInfo(buffer_size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT));
        VmaAllocationCreateInfo uniform_alloc_info(
            CreateAllocationInfo(VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, 
                                VMA_MEMORY_USAGE_GPU_TO_CPU, 
                                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
        );
        CreateBuffer(typed_allocator, uniform_buffer_info, uniform_alloc_info, buffer_location, typed_uniform_buffer_memory);
        memory_location = typed_uniform_buffer_memory;
    }

    void GraphicsRegistrar::CreateUniformBuffers() {
        uniform_buffers_.resize(kMaxFramesInFlight);
        uniform_buffers_memory_.resize(kMaxFramesInFlight);
        for (size_t i = 0; i < kMaxFramesInFlight; i++) {
            CreateUniformBuffer(uniform_buffers_[i], uniform_buffers_memory_[i]);
        }
    }

    VkFormat GraphicsRegistrar::FindSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features) {
        for (VkFormat format : candidates) {
            VkFormatProperties props;
            vkGetPhysicalDeviceFormatProperties(physical_device_, format, &props);

            if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
                return format;
            } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
                return format;
            }
        }
        throw std::runtime_error("failed to find supported format!");
    }

    VkFormat GraphicsRegistrar::FindDepthFormat() {
        return FindSupportedFormat(
            {VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
            VK_IMAGE_TILING_OPTIMAL,
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
        );

    }

    void GraphicsRegistrar::CreateDepthResources() {
        VmaAllocator typed_allocator = (VmaAllocator) allocator_;
        VmaAllocation typed_image_memory = (VmaAllocation) depth_image_memory_;
        VkFormat depth_format = FindDepthFormat();
        VkExtent3D needed_extent = {swap_chain_extent_.width, swap_chain_extent_.height, 1};
        VkImageCreateInfo image_create_info(CreateImageInfo(VK_IMAGE_TYPE_2D, depth_format, needed_extent, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT, VK_IMAGE_TILING_OPTIMAL));
        VmaAllocationCreateInfo image_memory_create_info(        
            CreateAllocationInfo(VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, 
                                VMA_MEMORY_USAGE_GPU_ONLY,
                                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        );
        CreateImage(typed_allocator,image_create_info,image_memory_create_info, depth_image_, typed_image_memory);
        depth_image_memory_ = typed_image_memory;
        VkImageSubresourceRange needed_subresource_range = {VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT, 0, 1, 0, 1};
        VkImageViewCreateInfo view_create_info(CreateImageViewInfo(depth_image_, depth_format, needed_subresource_range));
        if (vkCreateImageView(device_, &view_create_info, nullptr, &depth_image_view_) != VK_SUCCESS) {
                throw std::runtime_error("failed to create image view!");
        }
    }

    void GraphicsRegistrar::CreateCommandBuffers() {
        command_buffers_.resize(kMaxFramesInFlight);

        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.commandPool = command_pool_;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandBufferCount = (uint32_t) command_buffers_.size();

        if (vkAllocateCommandBuffers(device_, &alloc_info, command_buffers_.data()) != VK_SUCCESS) {
            throw std::runtime_error("Failed to allocate command buffers!");
        } 

    }

    void GraphicsRegistrar::RecordCommandBuffer(std::size_t index) {
        VkCommandBufferBeginInfo begin_info{};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags = 0; // Optional
        begin_info.pInheritanceInfo = nullptr; // Optional

        if (vkBeginCommandBuffer(command_buffers_[index], &begin_info) != VK_SUCCESS) {
            throw std::runtime_error("Failed to begin recording command buffer!");
        }

        std::vector<VkRenderPassBeginInfo> render_pass_infos;
        render_pass_infos.resize(render_pass_data_.renderpass.size());

        std::array<VkClearValue, 2> clear_values{};
        clear_values[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
        clear_values[1].depthStencil = {1.0f, 0};

        for (std::size_t j = 0; j < render_pass_infos.size(); j++) {
            render_pass_infos[j].sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            render_pass_infos[j].renderPass = render_pass_data_.renderpass[j];
            render_pass_infos[j].framebuffer = swap_chain_frame_buffers_[index];
            render_pass_infos[j].renderArea.offset = {0, 0};
            render_pass_infos[j].renderArea.extent = swap_chain_extent_;
            render_pass_infos[j].clearValueCount = static_cast<uint32_t>(clear_values.size());
            render_pass_infos[j].pClearValues = clear_values.data();
        }
        
        vkCmdBeginRenderPass(command_buffers_[index], &render_pass_infos[0], VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
            VkCommandBufferInheritanceInfo inheritance_info = {};
            inheritance_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
            inheritance_info.renderPass = render_pass_data_.renderpass[0];
            inheritance_info.subpass = 0;
            inheritance_info.framebuffer = swap_chain_frame_buffers_[index];
            main_renderer_->Render({GatherRenderObjects(render_object_index_[current_frame_]), vertex_buffer_, index_buffer_, inheritance_info, 0, index,  &descriptor_sets_[0]});
            main_renderer_->Wait();
            auto buffer_info = main_renderer_->command_buffers(0, index);
            vkCmdExecuteCommands(command_buffers_[index], 
                                    buffer_info.second, 
                                    buffer_info.first);
        vkCmdEndRenderPass(command_buffers_[index]);

        if (vkEndCommandBuffer(command_buffers_[index]) != VK_SUCCESS) {
            throw std::runtime_error("Failed to record command buffer!");
        }
    }

    void GraphicsRegistrar::CreateSyncObjects() {
        image_avaliable_semaphores_.resize(kMaxFramesInFlight);
        render_finished_semaphores_.resize(kMaxFramesInFlight);
        in_flight_fences.resize(kMaxFramesInFlight);
        images_in_flight_.resize(kMaxFramesInFlight, VK_NULL_HANDLE);

        VkSemaphoreCreateInfo semaphore_info{};
        semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

        VkFenceCreateInfo fence_info{};
        fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

        for (size_t i = 0; i < kMaxFramesInFlight; i++) {
            if (vkCreateSemaphore(device_, &semaphore_info, nullptr, &image_avaliable_semaphores_[i]) != VK_SUCCESS ||
                vkCreateSemaphore(device_, &semaphore_info, nullptr, &render_finished_semaphores_[i]) != VK_SUCCESS ||
                vkCreateFence(device_, &fence_info, nullptr, &in_flight_fences[i]) != VK_SUCCESS) {
                throw std::runtime_error("failed to create synchronization objects for a frame!");
            }
        }
    }

    void GraphicsRegistrar::PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& create_info) {
        create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT 
                                    | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT 
                                    | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
                                    | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT;
        create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT 
                                | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT 
                                | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        create_info.pfnUserCallback = DebugCallBack;
    }

    void GraphicsRegistrar::SetupDebugMessenger() {
        if (!enableValidationLayers) return;
        
        VkDebugUtilsMessengerCreateInfoEXT create_info = {};
        PopulateDebugMessengerCreateInfo(create_info);
        if (CreateDebugUtilsMessengerEXT(instance_, &create_info, nullptr, &debug_messenger_) != VK_SUCCESS) {
            throw std::runtime_error("Failed to set up debug messenger!");
        }
    }

    void GraphicsRegistrar::CreateSurface() {
        if (glfwCreateWindowSurface(instance_, window_, nullptr, &surface_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create window surface!");
        }
    }

    std::vector<RenderObject> GraphicsRegistrar::GatherRenderObjects(unsigned long long& index_store) {
        std::vector<RenderObject> return_list;
        for (const auto& render_object : related_logic_registrar_.get_render_objects(index_store)) {
            if (graphics_register_.HasGraphic(render_object.second)) {
                return_list.emplace_back(render_object.first, 
                                         graphics_register_.GraphicIndexOffset(render_object.second), 
                                         graphics_register_.GraphicIndexCount(render_object.second)
                );
            } else {
                related_application_.main_console().IssueWarning(
                    "graphics::GraphicsRegistrar::GatherRenderObjects()",
                    "Invalid Graphic Name",
                    fmt::format("Graphic with name {} could not be drawn as it hasn't been loaded.", render_object.second)
                );
            }
        }
        if (!return_list.size()) return_list.emplace_back(&kIdentity, 0, graphics_register_.default_index_count()); // No object to render so add one
        return return_list;
    }

//--

}
