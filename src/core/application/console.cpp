#include <string>

#include "application/console.h"

namespace console {
    void SystemConsole::IssueWarning(std::string source, std::string reason, std::string description) {}
    void SystemConsole::ThrowError(std::string source, std::string reason, std::string description) {}
}