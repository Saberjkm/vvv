
#include "application/xml.h"
#include "tinyxml2/tinyxml2.h"
#include "fmt/format.h"

#include "application/versionnumber.h"
#include "application/application.h"
#include "application/console.h"
#include "logic/logicregistrar.h"
#include "scenes/scenes.h"
#include "application/xml.h"

namespace application::xml {

FileType ConvertToFileType(std::string type) {
    if (type == "scene") {
        return FileType::kScene;
    } else {
        return FileType::kGeneric;
    }
}

std::size_t XMLFile::GetAttributeMask(tinyxml2::XMLElement* node) {
    bool unload = node->BoolAttribute("unload", true);
    bool lock = node->BoolAttribute("lock", "");
    if (lock && !unload) {
        global_context_.main_console().IssueWarning(
            "application::xml::XMLFile::LoadComponentTypes()", 
            "Error Loading Component Type",
            fmt::format("Cannot have Unload as false whilst Lock is present (Unload now set to true) in {}", file_header_.file_location.string())
        );
        unload = true;
    }
    std::size_t attribute_mask = 0;
    if (unload) attribute_mask |= 1;
    if (lock)  attribute_mask |= 2;
    return attribute_mask;
}

bool XMLFile::LoadComponentTypes(logic::LogicRegistrar& related_logic_registrar, 
                                const std::vector<tinyxml2::XMLElement*>& component_type_nodes,  
                                std::vector<application::ComponentTypeData>& component_types_storage) 
{
    bool good_load = true;
    return good_load; 
}

bool XMLFile::LoadSystems(logic::LogicRegistrar& related_logic_registrar, 
                          const std::vector<tinyxml2::XMLElement*>& system_nodes,  
                          std::vector<application::SystemData>& systems_storage) 
{
    bool good_load = true;
    return good_load;
}

bool XMLFile::LoadEntities(logic::LogicRegistrar& related_logic_registrar, 
                           const std::vector<std::pair<tinyxml2::XMLElement*, std::vector<XMLElementList>>>& entity_nodes,  
                           std::map<std::string, application::EntityData>& entities_storage) 
{
    bool good_load = true;
    return good_load;
}

bool XMLFile::LoadHeader() {
    return true;
}

scenes::SceneData XMLFile::LoadScene(logic::LogicRegistrar& related_logic_registrar) {
    return {};
}

}