#include <string>
#include <iostream>

#include "components/componentregistrar.h"
#include "application/console.h"
#include "components/componentregister.h"
#include "components/vvvcomponentregister.h"

namespace components {

ComponentRegistrar::ComponentRegistrar(application::BaseApplication& global_context, logic::LogicRegistrar& related_logic_registrar)
    : global_context_(global_context),
      related_logic_registrar_(related_logic_registrar),
      core_component_register_(std::make_shared<VVVComponentRegister>(*this, kVVVComponentRegisterName)) 
{
    component_registers_[kVVVComponentRegisterName] = core_component_register_;
}

bool ComponentRegistrar::AddRegister(std::string register_name, std::shared_ptr<ComponentRegister> register_pointer) {
    if (component_registers_.contains(register_name)) { return true; }
    component_registers_[register_name] = register_pointer;
    return true;
}

// Component Registrar Class

bool ComponentRegistrar::Initialise(const application::ComponentRegistrarSettings& component_registrar_settings) {
    for (auto& component_register : component_registrar_settings.component_registers) {
        if (component_register.second != nullptr ) {
            if (!component_registers_.contains(component_register.first)) {
                component_registers_.emplace(component_register.first, component_register.second);
            } else {
                global_context_.main_console().IssueWarning(
                    "components::ComponentRegistrar::Initialise(const application::ComponentRegistrarSettings&)", 
                    "Invalid Component Register",
                    fmt::format("Cant add Register {} to the registrar as a register with the same name already existst.", component_register.first)
                ); 
            }
        } else {
            global_context_.main_console().IssueWarning(
                "components::ComponentRegistrar::Initialise(const application::ComponentRegistrarSettings&)", 
                "Invalid Component Register",
                fmt::format("Cant add Register {} to the registrar as a register with the same name already existst.", component_register.first)
            ); 
        }
    }
    for (auto& component_register : component_registers_) {
        component_register.second->LoadAllComponentTypes();
    }
    return true;
}

bool ComponentRegistrar::FirstFrame() {
    return true;
}

std::size_t ComponentRegistrar::CountAllComponents() const {
    std::size_t count = 0;
    for (auto& item : component_lists_) {
        count += item.second.count();
    }
    return count;
}

bool ComponentRegistrar::AdvanceFrame() { 
    bool success = true;
    for (auto& item : component_lists_) {
        if (!item.second.AdvanceFrame()) success = false;
    }
    return success;
}

bool ComponentRegistrar::EndOfFrame() {
    bool success = true;
    for (auto& item : component_lists_) {
        if (!item.second.EndOfFrame()) success = false;
    }
    return success;
}

bool ComponentRegistrar::HasSameType(const std::string& component_label, const std::any& data) {
    if (!HasComponentList(component_label)) return false;
    if (component_lists_.at(component_label).IsWellFormed()) {
        return component_lists_.at(component_label).HasSameType(data); 
    }
    return false;
}

bool ComponentRegistrar::RegisterComponentList(std::string component_label, const std::any& component_type) {
    if (HasComponentList(component_label)) { return true; }
    if (!component_type.has_value()) {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::GetList(std::string)", 
            "Invalid Component Type",
            fmt::format("Cant register a Component Type {} that has no value", component_label)
        ); 
        return false; 
    }
    ComponentList new_list(global_context_);
    new_list.InitialiseList(component_type);
    component_lists_.emplace(component_label, std::move(new_list));
    return true;
}

std::size_t ComponentRegistrar::CreateNewComponent(std::string component_label) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).CreateNewComponent();
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::CreateNewComponent(std::string)", 
            "Invalid ID",
            fmt::format("Trying to create component with Label ID {}", component_label)
        );
        return -1;
    }
}

std::size_t ComponentRegistrar::CreateNewComponent(std::string component_label, std::any component_data) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).CreateNewComponent(component_data);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::CreateNewComponent(std::string, std::any)", 
            "Invalid ID",
            fmt::format("Trying to create component with Label ID {}", component_label)
        );
        return -1;
    }
}

std::size_t ComponentRegistrar::CreateNewComponent(std::string component_label, const std::vector<std::pair<std::string, std::string>>& data) {
    if (HasComponentList(component_label)) {
        if (HasDataConstructor(component_label)) {
            return component_lists_.at(component_label).CreateNewComponent(component_constructors[component_label](data));
        } else {
            global_context_.main_console().IssueWarning(
                "components::ComponentRegistrar::CreateNewComponent(std::string, const std::vector<std::pair<std::string, std::string>>&)", 
                "Invalid Constructor",
                fmt::format("Trying to use data to create a component with no method to do so with Label ID {}", component_label)
            );
        }
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::CreateNewComponent(std::string, const std::vector<std::pair<std::string, std::string>>&)", 
            "Invalid ID",
            fmt::format("Trying to create component with Label ID {}", component_label)
        );
    }
    return -1;
}

std::pair<bool, bool> ComponentRegistrar::UndoCreateNewComponent(std::string component_label, std::size_t component_id) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).UndoCreation(component_id);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UndoCreateNewComponent(std::string)", 
            "Invalid Component Label",
            fmt::format("Trying to create component for list with Label {} that doesn't exist.", component_label)
        );
        return {false, false};
    }
}

bool ComponentRegistrar::DeleteComponent(std::string component_label, std::size_t comp_id) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).DeleteComponent(comp_id);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::DeleteComponent(std::string, std::size_t)", 
            "Invalid ID",
            fmt::format("Trying to delete component with Label ID {}, and ID {}", component_label, comp_id)
        );
        return false;
    }
}

std::pair<bool, bool> ComponentRegistrar::UndoDeleteComponent(std::string component_label, std::size_t component_id) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).UndoDeletion(component_id);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UndoDeletion(std::string, std::size_t)", 
            "Invalid ID",
            fmt::format("Trying to undo deletion of component with Label ID {}, and ID {}", component_label, component_id)
        );
        return {false, false};
    }
}

application::ComponentTypeData ComponentRegistrar::LoadComponentType(const application::ComponentTypeRepresentation& component_type) {
    application::ComponentTypeData return_component_data;
    if (HasComponentList(component_type.component_label)) {
        if ((return_component_data.is_valid = component_lists_.at(component_type.component_label).Load(component_type.flag_data))) {
            return_component_data.label = component_type.component_label;
            return_component_data.flag_data = component_type.flag_data;
            return_component_data.is_valid = true;
        } else {
            global_context_.main_console().IssueWarning(
                "components::ComponentRegistrar::LoadComponentType(const application::ComponentTypeRepresentation&)", 
                "Error while Loading",
                fmt::format("Trying to load component type with List ID {}", component_type.component_label)
            );
        }
        return return_component_data;
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::LoadComponentType(sconst application::ComponentTypeRepresentation&)", 
            "Invalid ID",
            fmt::format("Trying to load component type with List ID {}", component_type.component_label)
        );
        return return_component_data;
    }
}

std::pair<bool, bool> ComponentRegistrar::UndoLoadComponentType(const application::ComponentTypeData& component_type) {
    if (HasComponentList(component_type.label)) {
        return component_lists_.at(component_type.label).UndoLoad(component_type.flag_data);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UndoLoadComponentType(std::string)", 
            "Invalid ID",
            fmt::format("Trying to undo the load of a component type with List ID {}", component_type.label)
        );
        return {false, false};
    }
}

bool ComponentRegistrar::UnloadComponentType(std::string component_label) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).Unload();
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UnloadComponentType(std::string)", 
            "Invalid ID",
            fmt::format("Trying to unload component type with List ID {}", component_label)
        );
        return false;
    }
}

std::pair<bool, bool> ComponentRegistrar::UndoUnloadComponentType(std::string component_label) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).UndoUnload();
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UndoUnloadComponentType(std::string)", 
            "Invalid ID",
            fmt::format("Trying to undo an unload of component type with List ID {}", component_label)
        );
        return {false, false};
    }
}

bool ComponentRegistrar::LockComponentType(std::string component_label, std::string lock_name) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).AddLock(lock_name);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::LockComponentType(std::size_t, std::string)", 
            "Invalid ID",
            fmt::format("Trying to lock a component type with List ID {}, and Lock Name {}", component_label, lock_name)
        );
        return false;
    }
}

std::pair<bool, bool> ComponentRegistrar::UndoLockComponentType(std::string component_label, std::string lock_name) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).UndoAddLock(lock_name);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UndoLockComponentType(std::size_t, std::string)", 
            "Invalid ID",
            fmt::format("Trying to undo a lock for component type with List ID {}, and Lock Name {}", component_label, lock_name)
        );
        return {false, false};
    }
}

bool ComponentRegistrar::UnlockComponentType(std::string component_label, std::string lock_name) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).Unlock(lock_name);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UnockComponentType(std::size_t, std::string)", 
            "Invalid ID",
            fmt::format("Trying to unlock a component type with List ID {}, and Lock Name {}", component_label, lock_name)
        );
        return false;
    }
}

std::pair<bool, bool> ComponentRegistrar::UndoUnlockComponentType(std::string component_label, std::string lock_name) {
    if (HasComponentList(component_label)) {
        return component_lists_.at(component_label).UndoUnlock(lock_name);
    } else {
        global_context_.main_console().IssueWarning(
            "components::ComponentRegistrar::UnockComponentType(std::size_t, std::string)", 
            "Invalid ID",
            fmt::format("Trying to undo an unlock of a component type with List ID {}, and Lock Name {}", component_label, lock_name)
        );
        return {false, false};
    }
}

}
// End of Component Registrar Class