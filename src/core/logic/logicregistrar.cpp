#include "logic/logic.h"
#include "logic/logicregistrar.h"
#include "application/application.h"
#include "logic/logic.h"
#include "input/inputregistrar.h"
#include "application/console.h"
#include "application/settings.h"

namespace logic {

LogicRegistrar::LogicRegistrar(application::BaseApplication& global_context) 
    : related_entity_registrar_(global_context, *this),
        related_component_registrar_(global_context, *this),
        related_system_registrar_(global_context, *this),
        related_input_registrar_(global_context, *this),
        related_scene_registrar_(global_context, *this),
        global_context_(global_context)
{}

bool LogicRegistrar::Initialise(const application::LogicRegistrarSettings& logic_registrar_settings) {
    return related_entity_registrar_.Initialise(logic_registrar_settings.entity_registrar_settings)
    && related_component_registrar_.Initialise(logic_registrar_settings.component_registrar_settings)
    && related_system_registrar_.Initialise(logic_registrar_settings.system_registrar_settings)
    && related_input_registrar_.Initialise(logic_registrar_settings.input_registrar_settings)
    && related_scene_registrar_.Initialise(logic_registrar_settings.scene_registrar_settings);
}

bool LogicRegistrar::FirstFrame() {
    return related_entity_registrar_.FirstFrame()
    && related_component_registrar_.FirstFrame()
    && related_system_registrar_.FirstFrame()
    && related_input_registrar_.FirstFrame()
    && related_scene_registrar_.FirstFrame();
}

bool LogicRegistrar::AdvanceFrame() {
    for (auto system_label : render_objects_systems) {
        if (related_system_registrar_.SystemDataWillChange(system_label)) {
            render_objects_index_++;
            break;
        }
    }
    logic_step_ += 1;
    return related_input_registrar_.AdvanceFrame()     &&
           related_component_registrar_.AdvanceFrame() &&
           related_component_registrar_.AdvanceFrame() &&
           related_system_registrar_.AdvanceFrame()    &&
           related_scene_registrar_.AdvanceFrame();
}

bool LogicRegistrar::EndOfFrame() {
    return related_input_registrar_.EndOfFrame()     &&
           related_entity_registrar_.EndOfFrame()    &&
           related_component_registrar_.EndOfFrame() &&
           related_system_registrar_.EndOfFrame()    &&
           related_scene_registrar_.EndOfFrame();
}

std::vector<std::pair<const glm::mat3*, std::string>> LogicRegistrar::get_render_objects(unsigned long long& index_store) {
    index_store = render_objects_index_;
    std::vector<std::pair<const glm::mat3*, std::string>> return_list;
    index_store = render_objects_index_;
    return return_list;
}

bool LogicRegistrar::HandleEvent(Event& event) {
    auto gathered_evaluate_result = std::visit(GatheredDataHandler(event, *this), event.gathered_data);
    auto inherent_evaluate_result = std::visit(InherentDataHandler(event, *this), event.inherent_data);

    // Error gathering data for the event
    if (!gathered_evaluate_result) {
        global_context_.main_console().IssueWarning(
            "logic::LogicRegistrar::HandleEvent(Event&)",
            "Error Gathering Data",
            fmt::format("There was an error gathering required data for Event {}", event.event_label)
        );
        return false;
    }

    bool handled_correctly = std::visit(TargetHandler(event, *this), event.target);
    
    // Check if needed to be re-added
    if (inherent_evaluate_result.first && event.can_repeat) {
        event_queue_[logic_step_ + inherent_evaluate_result.second].emplace_back(event);
    } else {
        registered_events_.erase(event.event_label);
    }

    // Foward the event to the right target
    return handled_correctly;
}

bool LogicRegistrar::HandleEvents() {
    bool all_success;
    auto& event_list = event_queue_[logic_step_];
    for (auto& event : event_queue_[logic_step_]) {
        if (!HandleEvent(event)) all_success = false;
    }
    // The handle event function removes the id from registered events
    event_queue_.erase(logic_step_);
    return all_success;
}

void LogicRegistrar::HandleLogicStep() {
    HandleEvents();
    related_input_registrar_.MapInputs();
    logic_step_++;
}

bool LogicRegistrar::DispatchEvent(Event&& event) {
    return HandleEvent(event);
}

bool LogicRegistrar::QueueEvent(Event&& event) {
    if (!registered_events_.contains(event.event_label)) {
        registered_events_.emplace(event.event_label);
        event_queue_[logic_step_ + 1].emplace_back(event);
        return true;
    } else {
        global_context_.main_console().IssueWarning(
            "logic::LogicRegistrar::QueueEvent(Event&&)",
            "Duplicate Event Name",
            fmt::format("Event was trying to be queued that already exists in queue with name {}", event.event_label)
        );
        return false;
    }
}

std::vector<std::pair<glm::mat4, std::string>> GatherModelMatrices() {
    return {};
}

}