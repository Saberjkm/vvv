#include "logic/logic.h"
#include "logic/logicregistrar.h"
#include "input/input.h"

namespace logic {
    
bool EntityGatherDataHandler::operator()(EntityRelatedGatherAction& related_gather_action) {
    parent_gather_data.entity_data = logic_registrar.entity_registrar().GatherEntityData(related_gather_action.components);
    return true;
}

bool EntityGatherDataHandler::operator()(EntityAllGatherAction& related_gather_action) {
    parent_gather_data.entity_data = logic_registrar.entity_registrar().GatherEntityData(related_gather_action.components);
    return true;
}

bool EntityGatherDataHandler::operator()(EntityConditionalGatherAction& related_gather_action) {
    auto entity_list =  logic_registrar.entity_registrar().GatherEntityPointers(related_gather_action.components);
    for (auto& entity : entity_list) {
        if (related_gather_action.condition(entity)) {
            auto entity_data = entity->GetDataRepresentation(related_gather_action.components);
            parent_gather_data.entity_data[entity_data.first] = entity_data.second;
        }
    }
    return true;
}

bool EntityGatherDataHandler::operator()(EntitySelectedGatherAction& related_gather_action) {
    decltype(parent_gather_data.entity_data) new_entity_data;
    for (auto id : related_gather_action.entity_id_list) {
        if (logic_registrar.entity_registrar().HasEntity(id)) {
            new_entity_data[id] = logic_registrar.entity_registrar()[id]->GetDataRepresentation().second;
        } else {
            return false;
        }
    }
    return true;
}

bool TargetHandler::operator()(EntityTarget& target) { return true; }
bool TargetHandler::operator()(ComponentTarget& target) { return true; }

bool TargetHandler::operator()(SystemTarget& target) {
    return logic_registrar.system_registrar().HandleSystemEvent(parent_event, target.system_label);
}

bool TargetHandler::operator()(EntityRegistrarTarget& target) { return true; }
bool TargetHandler::operator()(ComponentRegistrarTarget& target) { return true; }
bool TargetHandler::operator()(SystemRegistrarTarget& target) { return true; }
bool TargetHandler::operator()(GraphicsRegistrarTarget& target) { return true; }
bool TargetHandler::operator()(LogicRegistrarTarget& target) { return true; }
bool TargetHandler::operator()(InputRegistrarTarget& target) { return true; }
}