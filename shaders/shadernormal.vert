#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_debug_printf : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 view;
    mat4 proj;
} ubo;

layout( push_constant ) uniform matrix
{
    mat3 model;
} Matrix;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec4 inTexCoord;
layout(location = 3) in float inInsideOrOutside;
layout(location = 4) in uint inFillType;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec4 fragTexCoord;
layout(location = 2) out float insideOrOutside;
layout(location = 3) out uint fillType;


void main() {
    gl_Position = ubo.proj * ubo.view * vec4(inPosition, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
    insideOrOutside = inInsideOrOutside;
    fillType = inFillType;
}